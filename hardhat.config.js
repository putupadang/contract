require('dotenv').config()

require('@nomiclabs/hardhat-etherscan')
require('@nomiclabs/hardhat-waffle')
require('hardhat-gas-reporter')
require('solidity-coverage')

// init all tasks in folder ./tasks
const fs = require('fs')
const taskPath = './tasks/'
fs.readdirSync(taskPath).forEach((file) => {
  require(taskPath + file)
})

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  solidity: {
    compilers: [
      {
        version: '0.8.13',
        settings: {
          optimizer: {
            enabled: true,
            runs: 300
          }
        }
      },
      { version: '0.8.4', settings: {} },
      { version: '0.7.6', settings: {} },
      { version: '0.6.12', settings: {} },
      { version: '0.6.6', settings: {} },
      { version: '0.5.12', settings: {} }
    ]
  },
  networks: {
    goerli: {
      url: process.env.GOERLI_INFURA_HTTPS_URL || '',
      accounts:
        process.env.WALLET_PRIVATE_KEY !== undefined
          ? [process.env.WALLET_PRIVATE_KEY]
          : [],
      timeout: 5 * 60 * 1000
    },
    hardhat: {
      forking: {
        enabled: true,
        url: `https://eth-mainnet.alchemyapi.io/v2/${process.env.ALCHEMY_API_KEY}`,
        blockNumber: 14741120
      }
    }
  },
  gasReporter: {
    enabled: process.env.REPORT_GAS !== undefined,
    currency: 'USD'
  }
}
