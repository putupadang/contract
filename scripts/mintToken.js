const { ethers: rawEthers } = require('ethers')
const dummy = require('../artifacts/contracts/test/DummyToken.sol/DummyToken.json')

async function main () {
  const wallet = new rawEthers.Wallet(process.env.WALLET_PRIVATE_KEY)
  const provider = new rawEthers.providers.JsonRpcProvider(
    process.env.GOERLI_INFURA_HTTPS_URL
  )
  const signer = await wallet.connect(provider)

  const Dummy = new rawEthers.ContractFactory(
    dummy.abi,
    dummy.bytecode,
    signer
  )
  const token = await Dummy.deploy(
    ethers.utils.parseUnits('10000', 18),
    'Dummy',
    18
  )
  await token.deployed()

  console.log(token)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
