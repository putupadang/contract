const jsonAbi = require('../artifacts/contracts/PairVault.sol/PairVault.json')
const iface = new ethers.utils.Interface(jsonAbi.abi)
console.log(iface)
console.dir(
  {
    interface: iface.format(ethers.utils.FormatTypes.full)
  },
  { depth: null }
)
