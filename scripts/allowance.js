const { ethers: rawEthers } = require('ethers')

async function main () {
  const PairInfo = {
    v2factory: '0xCf7Ed3AccA5a467e9e704C703E8D87F634fB0Fc9',
    v2router: '0xDc64a140Aa3E981100a9becA4E685f962f0cF6C9',
    pair: '0x5FC8d32690cc91D4c39d9d3abcBD16989F875707',
    WETH: {
      address: '0x5FbDB2315678afecb367f032d93F642f64180aa3',
      decimals: 18
    },
    USDC: {
      address: '0x9fE46736679d2D9a65F0992F2272dE9f3c7fa6e0',
      decimals: 6
    }
  }

  const [deployer] = await ethers.getSigners()
  const wallet = new ethers.Wallet(process.env.WALLET_PRIVATE_KEY)
  const signer = wallet.connect(ethers.provider)
  for (const target of [
    { signer: deployer, spender: PairInfo.v2router },
    { signer, spender: PairInfo.pair }
  ]) {
    for (const sym of ['WETH', 'USDC']) {
      const token = PairInfo[sym]

      const contract = new rawEthers.Contract(
        token.address,
        [
          'function allowance(address owner, address spender) public view returns (uint256)'
        ],
        target.signer
      )
      const allowance = await contract.allowance(
        target.signer.address,
        target.spender
      )
      console.log(
        `${target.signer.address} ${sym} allowance:`,
        rawEthers.utils.formatUnits(allowance, token.decimals)
      )
    }
  }
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
