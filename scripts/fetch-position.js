const { ethers: rawEthers, Contract } = require('ethers')
const { Addresses, Tokens, PairList } = require('../../data.json')
const ownerAddr = ''

async function fetchPositionValue (
  tokenA,
  tokenB,
  ownerAddr,
  pairVaultAddr,
  provider
) {
  const pairVaultContract = new Contract(
    pairVaultAddr,
    [
      'function positions(address, address) view returns (uint112 amount, uint112 debt, uint32 blocktime, uint128 available, bytes16 info)',
      'function getReserves() public view returns (uint112 reserve0, uint112 reserve1, uint256 supply)'
    ],
    provider
  )

  const positions = await pairVaultContract.positions(ownerAddr, pairVaultAddr)
  const [reserve0, reserve1, totalSupply] =
    await pairVaultContract.getReserves()
  const [reserveA, reserveB] =
    tokenA.address < tokenB.address
      ? [reserve0, reserve1]
      : [reserve1, reserve0]

  console.log(tokenA.symbol, tokenB.symbol)
  console.log('positions', format(positions.amount, 18))
  console.log('reserveA', format(reserveA, tokenA.decimals))
  console.log('reserveB', format(reserveB, tokenB.decimals))
  console.log('totalSupply', format(totalSupply, 18))

  return +ethers.utils.formatUnits(
    reserveB.mul(2).mul(positions.amount).div(totalSupply),
    tokenB.decimals
  )
}

async function main () {
  for (const pairInfo of PairList) {
    const tokenA = Tokens[pairInfo.addressA]
    const tokenB = Tokens[pairInfo.addressB]
    const key = pairInfo.id
    const pairVaultAddr = Addresses.pairs[key].vault

    const positionValue = await fetchPositionValue(
      tokenA,
      tokenB,
      ownerAddr,
      pairVaultAddr,
      ethers.provider
    )

    console.log('positionValue', positionValue, '\n')
  }
}

function format (value, decimals) {
  return +rawEthers.utils.formatUnits(value, decimals)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
