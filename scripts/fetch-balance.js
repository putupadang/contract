const { ethers: rawEthers } = require('ethers')
const IERC20 = require('@openzeppelin/contracts/build/contracts/IERC20.json')
const Tokens = {
  DAI: {
    address: '0x6B175474E89094C44Da98b954EedeAC495271d0F',
    decimals: 18,
    name: 'Dai Stablecoin',
    symbol: 'DAI'
  },
  USDC: {
    address: '0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48',
    decimals: 6,
    name: 'USD Coin',
    symbol: 'USDC'
  },
  WETH: {
    address: '0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2',
    decimals: 18,
    name: 'Wrapped Ether',
    symbol: 'WETH'
  },
  WBTC: {
    address: '0x2260FAC5E5542a773Aa44fBCfeDf7C193bc2C599',
    decimals: 8,
    name: 'Wrapped BTC',
    symbol: 'WBTC'
  },
  'UNI-V2-USDC-WETH': {
    address: '0xB4e16d0168e52d35CaCD2c6185b44281Ec28C9Dc',
    decimals: 18,
    name: 'UNI-V2-USDC-WETH',
    symbol: 'UNI-V2-USDC-WETH'
  }
}

async function getTokenContracts () {
  const tokenContracts = {}
  for (const symbol in Tokens) {
    const token = Tokens[symbol]
    tokenContracts[symbol] = await ethers.getContractAt(
      IERC20.abi,
      token.address
    )
  }
  return tokenContracts
}

async function fetchBalance (target, tokenContracts) {
  const balance = {}
  balance.ETH = await ethers.provider.getBalance(target.address)
  for (const name in tokenContracts) {
    balance[name] = await tokenContracts[name].balanceOf(target.address)
  }
  return balance
}

async function main () {
  const [deployer] = await ethers.getSigners()
  const tokenContracts = await getTokenContracts()

  const targets = [
    { name: 'deployer', address: deployer.address },
    {
      name: 'MakerStrategy',
      address: '0x38A70c040CA5F5439ad52d0e821063b0EC0B52b6'
    },
    {
      name: 'MCD_JOIN_ETH_A',
      address: '0x2F0b23f53734252Bda2277357e97e1517d6B042A'
    }
  ]

  for (const target of targets) {
    console.log(`[${target.name}]:`, target.address)
    const deployerBalance = await fetchBalance(target, tokenContracts)
    for (const sym in deployerBalance) {
      if (sym === 'ETH') {
        console.log(
          `[${target.name}] ${sym}:`,
          +rawEthers.utils.formatUnits(deployerBalance[sym], 'ether')
        )
      } else {
        console.log(
          `[${target.name}] ${sym}:`,
          +rawEthers.utils.formatUnits(
            deployerBalance[sym],
            Tokens[sym].decimals
          )
        )
      }
    }
  }
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
