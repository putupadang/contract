const { ethers: rawEthers } = require('ethers')
const IERC20 = require('@openzeppelin/contracts/build/contracts/IERC20.json')
const UniswapV2Router02 = require('@uniswap/v2-periphery/build/UniswapV2Router02.json')
const Tokens = {
  '0x6B175474E89094C44Da98b954EedeAC495271d0F': {
    address: '0x6B175474E89094C44Da98b954EedeAC495271d0F',
    decimals: 18,
    name: 'Dai Stablecoin',
    symbol: 'DAI'
  },
  '0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48': {
    address: '0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48',
    decimals: 6,
    name: 'USD Coin',
    symbol: 'USDC'
  },
  '0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2': {
    address: '0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2',
    decimals: 18,
    name: 'Wrapped Ether',
    symbol: 'WETH'
  },
  '0x2260FAC5E5542a773Aa44fBCfeDf7C193bc2C599': {
    address: '0x2260FAC5E5542a773Aa44fBCfeDf7C193bc2C599',
    decimals: 8,
    name: 'Wrapped BTC',
    symbol: 'WBTC'
  }
}
const V2router = '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D'
const V2Pools = {
  '0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48-0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2':
    {
      address: '0xB4e16d0168e52d35CaCD2c6185b44281Ec28C9Dc',
      decimals: 18,
      name: 'UNI-V2-USDC-WETH'
    }
}

function getTokenContract (tokenAddr) {
  return ethers.getContractAt(IERC20.abi, tokenAddr)
}

async function approve (tokenAddr) {
  const tokenContract = await getTokenContract(tokenAddr)
  const approve = await tokenContract.approve(V2router, BigInt(2 ** 255))
  return approve.wait()
}

async function fetchReserves (poolAddr, token0, token1) {
  const poolContract = new rawEthers.Contract(
    poolAddr,
    [
      'function getReserves() public view returns (uint112 _reserve0, uint112 _reserve1, uint32 _blockTimestampLast)'
    ],
    ethers.provider
  )
  const reserves = await poolContract.getReserves()
  return [
    formatUnits(reserves[0], token0.decimals),
    formatUnits(reserves[1], token1.decimals)
  ]
}

function formatUnits (value, decimals) {
  return +rawEthers.utils.formatUnits(value, decimals)
}

function parseUnits (value, decimals) {
  return rawEthers.utils.parseUnits(value, decimals)
}

function truncate (number, precision = 8) {
  const decimals = number.toString().indexOf('.')
  if (decimals < 0) {
    return number
  }
  return +number.toString().slice(0, decimals + precision + 1)
}

async function addLiquidity (key, ethAmount, deployer) {
  const [addr0, addr1] = key.split('-')
  const token0 = Tokens[addr0]
  const token1 = Tokens[addr1]
  const pool = V2Pools[key]
  const reserves = await fetchReserves(pool.address, token0, token1)
  let amount0Desired, amount1Desired
  if (token0.symbol === 'WETH') {
    amount0Desired = ethAmount
    amount1Desired = truncate(
      (ethAmount * reserves[1]) / reserves[0],
      token1.decimals
    )
  } else {
    amount0Desired = truncate(
      (ethAmount * reserves[0]) / reserves[1],
      token0.decimals
    )
    amount1Desired = ethAmount
  }
  await approve(addr0)
  await approve(addr1)

  const deadline = Math.trunc(Date.now() / 1000) + 5 * 60
  const v2router = await ethers.getContractAt(UniswapV2Router02.abi, V2router)
  const tx = await v2router.addLiquidity(
    token0.address,
    token1.address,
    parseUnits(amount0Desired.toString(), token0.decimals),
    parseUnits(amount1Desired.toString(), token1.decimals),
    0,
    0,
    deployer.address,
    deadline
  )
  await tx.wait()
  console.log(
    '[addLiquidity]',
    `${token0.symbol}/${token1.symbol}`,
    `${amount0Desired}/${amount1Desired}`
  )
}

async function main () {
  const [deployer] = await ethers.getSigners()

  for (const key in V2Pools) {
    const ethAmount = 20
    await addLiquidity(key, ethAmount, deployer)
  }
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
