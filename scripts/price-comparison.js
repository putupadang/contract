const { ethers: rawEthers } = require('ethers')
const { formatUnits } = rawEthers.utils
const IERC20Metadata = require('@openzeppelin/contracts/build/contracts/IERC20Metadata.json')
const Addresses = require('../static/addresses.json').goerli
let user = new rawEthers.Wallet(process.env.WALLET_PRIVATE_KEY)
user = user.connect(ethers.provider)

// goerli contracts
const addrList = {
  deployer: '0x05C7f50A3704cf84bCCB81AB7Eaf566d7fF8d77D',
  user: '0x05C7f50A3704cf84bCCB81AB7Eaf566d7fF8d77D',
  v2factory: '0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f',
  v2router: '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D',
  strategy: '0xF915Aba7A8B54b7E85d6Dcfcc6641B1317C3Dc3a',
  pairVaults: {
    'WETH/USDC': '0x09098D7C3f4A5f4FEdb92E074189e8a256C36964',
    'WBTC/DAI': '0x13265eE6AEc31411FABbd3694B4C9c95c7E7571f',
    'USDC/DAI': '0xde4d6e069B09f644c521a269797618c68A3B5D48'
  },
  leverage: '0x13d32df737D45587f0fB7455cEb6cBE655a6a335',
  staking: '0xbf769438F7cE8A559F9674eB1dFf4200EC6AAC9E'
}

async function main () {
  const tokens = await setupTokens(
    [
      { symbol: 'wstETH', decimals: 18 },
      { symbol: 'WETH', decimals: 18 },
      { symbol: 'WBTC', decimals: 8 },
      { symbol: 'USDC', decimals: 6 },
      { symbol: 'DAI', decimals: 18 }
    ],
    Addresses
  )
  console.log('user:', user.address)

  const pairVaults = {
    'WETH/USDC': await getPairContract('WETH/USDC'),
    'WBTC/DAI': await getPairContract('WBTC/DAI'),
    'USDC/DAI': await getPairContract('USDC/DAI')
  }
  const leverage = await getLeverageContract()

  await comparePrice(pairVaults, leverage, tokens)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })

async function getPairContract (target) {
  const PairVault = await ethers.getContractFactory('PairVault')
  return ethers.getContractAt(PairVault.interface, addrList.pairVaults[target])
}

async function getLeverageContract () {
  const LeverageFactory = await ethers.getContractFactory('Leverage')
  return ethers.getContractAt(LeverageFactory.interface, addrList.leverage)
}

async function setupTokens (targetList, Addresses) {
  const tokenContracts = {}
  for (const target of targetList) {
    const symbol = target.symbol
    const token = Addresses.Tokens[target.symbol]
    tokenContracts[symbol] = await ethers.getContractAt(
      IERC20Metadata.abi,
      token.address
    )
    if (!tokenContracts[symbol].decimals) {
      tokenContracts[symbol].decimals = async () => {
        return target.decimals
      }
    }
  }
  return tokenContracts
}

async function comparePrice (pairVaults, leverage, tokens) {
  for (const key in pairVaults) {
    const pair = pairVaults[key]
    const token0Addr = await pair.token0()
    const token1Addr = await pair.token1()
    const tokenQuoteAddr = await pair.tokenQuote()
    const token0 = Object.values(tokens).find((t) => t.address === token0Addr)
    const token1 = Object.values(tokens).find((t) => t.address === token1Addr)
    const tokenQuote = Object.values(tokens).find(
      (t) => t.address === tokenQuoteAddr
    )

    const [tokenA, tokenB] =
      token1Addr === tokenQuoteAddr ? [token0, token1] : [token1, token0]

    console.log(`${await tokenA.symbol()}-${await tokenB.symbol()}`)
    console.log({ token0Addr, token1Addr, tokenQuoteAddr })
    const reserves = await pair.getReserves()
    console.log({ reserves })
    const vaultId = await pair.data(leverage.address, 'vault')
    const price = await leverage.getPrice(+vaultId)

    const reserveQuote =
      tokenA.address < tokenB.address ? reserves.reserve1 : reserves.reserve0
    console.log({ reserveQuote })

    const pricing = {
      maker: Math.trunc(formatUnits(price, 27)),
      uniV2: Math.trunc(
        ((+formatUnits(reserveQuote, await tokenQuote.decimals()) * 2) /
          +reserves.totalSupply) *
          10 ** 18
      )
    }
    console.log({ pricing })
    console.log(`[pricing] ${await tokenA.symbol()}-${await tokenB.symbol()}`)
    console.log(
      `[pricing] maker price in ${await tokenB.symbol()}: 1 LP:`,
      pricing.maker
    )
    console.log(
      `[pricing] uni price in ${await tokenB.symbol()}: 1 LP:`,
      pricing.uniV2
    )
    console.log(
      '[pricing] difference %:',
      (Math.abs(pricing.uniV2 - pricing.maker) /
        Math.max(pricing.uniV2, pricing.maker)) *
        100
    )
    console.log('\n')
  }
}
