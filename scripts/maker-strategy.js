const { ethers: rawEthers } = require('ethers')
const IERC20 = require('@openzeppelin/contracts/build/contracts/IERC20.json')
const IMakerStrategy = require('../artifacts/contracts/MakerStrategy.sol/MakerStrategy.json')
const CDP_MANAGER = '0x5ef30b9986345249bc32d8928B7ee64DE9435E39'
const MCD_VAT = '0x35D1b3F3D7966A1DFe207aa4514C12a259A0492B'
const MCD_JOIN_ETH_A = '0x2F0b23f53734252Bda2277357e97e1517d6B042A'
const MCD_JOIN_DAI = '0x9759A6Ac90977b93B58547b4A71c78317f391A28'
const MCD_JOIN_UNIV2USDCETH_A = '0x03Ae53B33FeeAc1222C3f372f32D37Ba95f0F099'
const MCD_JUG = '0x19c0976f590D67707E62397C87829d896Dc0f1F1'
// const MCD_JOIN_WBTC_A = '0xBF72Da2Bd84c5170618Fbe5914B0ECA9638d5eb5'

const Tokens = {
  '0x6B175474E89094C44Da98b954EedeAC495271d0F': {
    address: '0x6B175474E89094C44Da98b954EedeAC495271d0F',
    decimals: 18,
    name: 'Dai Stablecoin',
    symbol: 'DAI'
  },
  '0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48': {
    address: '0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48',
    decimals: 6,
    name: 'USD Coin',
    symbol: 'USDC'
  },
  '0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2': {
    address: '0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2',
    decimals: 18,
    name: 'Wrapped Ether',
    symbol: 'WETH'
  },
  '0x2260FAC5E5542a773Aa44fBCfeDf7C193bc2C599': {
    address: '0x2260FAC5E5542a773Aa44fBCfeDf7C193bc2C599',
    decimals: 8,
    name: 'Wrapped BTC',
    symbol: 'WBTC'
  },
  '0xB4e16d0168e52d35CaCD2c6185b44281Ec28C9Dc': {
    address: '0xB4e16d0168e52d35CaCD2c6185b44281Ec28C9Dc',
    decimals: 18,
    name: 'UNI-V2-USDC-WETH',
    symbol: 'UNI-V2-USDC-WETH'
  }
}

async function deployContract () {
  const MakerStrategy = await ethers.getContractFactory('MakerStrategy')
  const makerStrategy = await MakerStrategy.deploy('MakerStrategy')
  await makerStrategy.deployed()
  console.log('MakerStrategy deployed:', makerStrategy.address)
  return makerStrategy
}

async function setupMakerStrategy (makerStrategyAddr) {
  let makerStrategy
  if (makerStrategyAddr) {
    makerStrategy = await ethers.getContractAt(
      IMakerStrategy.abi,
      makerStrategyAddr
    )
  } else {
    makerStrategy = await deployContract()
  }
  return makerStrategy
}

async function openMakerVault (makerStrategy, colType) {
  const tx = await makerStrategy.open(colType, CDP_MANAGER)
  await tx.wait()
  console.log('[openMakerVault] success')
}

function getTokenContract (tokenAddr) {
  return ethers.getContractAt(IERC20.abi, tokenAddr)
}

async function approve (tokenAddr, spenderAddr, amount) {
  const tokenContract = await getTokenContract(tokenAddr)
  const approve = await tokenContract.approve(spenderAddr, amount)
  return approve.wait()
}

async function deposit (makerStrategy, vaultId, amount, target) {
  console.log(
    '[deposit] token:',
    target.type,
    'amount:',
    formatUnits(amount, target.decimals)
  )
  let tx
  if (target.symbol === 'ETH') {
    tx = await makerStrategy.deposit(
      vaultId,
      amount,
      CDP_MANAGER,
      target.mcdJoinAddr,
      {
        value: amount
      }
    )
  } else {
    await approve(target.tokenInAddr, makerStrategy.address, amount)
    tx = await makerStrategy.deposit(
      vaultId,
      amount,
      CDP_MANAGER,
      target.mcdJoinAddr
    )
  }
  await tx.wait()
}

async function borrowDAI (makerStrategy, vaultId, amtBorrow) {
  console.log('[borrowDAI] borrow:', formatUnits(amtBorrow, 18), 'DAI')
  const tx = await makerStrategy.borrow(
    vaultId,
    amtBorrow,
    CDP_MANAGER,
    MCD_JUG,
    MCD_JOIN_DAI
  )
  await tx.wait()
}

async function depositAndBorrow (
  makerStrategy,
  vaultId,
  amtDeposit,
  amtBorrow,
  target
) {
  const tx = await makerStrategy.depositAndBorrow(
    vaultId,
    amtDeposit,
    amtBorrow,
    CDP_MANAGER,
    MCD_JUG,
    target.mcdJoinAddr,
    MCD_JOIN_DAI,
    {
      value: amtDeposit
    }
  )
  await tx.wait()
}

async function payback (makerStrategy, vaultId, amount) {
  const tx = await makerStrategy.payback(
    vaultId,
    amount,
    CDP_MANAGER,
    MCD_JOIN_DAI
  )
  await tx.wait()
}

async function withdraw (makerStrategy, vaultId, amount, target) {
  console.log('[withdraw] amount:', +rawEthers.utils.formatUnits(amount, 18))
  const tx = await makerStrategy.withdraw(
    vaultId,
    amount,
    CDP_MANAGER,
    target.mcdJoinAddr,
    MCD_JOIN_DAI
  )
  await tx.wait()
}

async function getVaultDebt (makerStrategy, vatAddr, ilk, urn) {
  const debt = await makerStrategy.getVaultDebt(vatAddr, ilk, urn)
  return debt
}

async function getVaultData (makerStrategy, deployer) {
  const cdpManager = new rawEthers.Contract(
    CDP_MANAGER,
    [
      'function cdpAllow(uint256 cdp, address usr, uint256 ok)',
      'function cdpCan(address, uint256, address) view returns (uint256)',
      'function cdpi() view returns (uint256)',
      'function count(address) view returns (uint256)',
      'function enter(address src, uint256 cdp)',
      'function first(address) view returns (uint256)',
      'function flux(bytes32 ilk, uint256 cdp, address dst, uint256 wad)',
      'function flux(uint256 cdp, address dst, uint256 wad)',
      'function frob(uint256 cdp, int256 dink, int256 dart)',
      'function give(uint256 cdp, address dst)',
      'function ilks(uint256) view returns (bytes32)',
      'function last(address) view returns (uint256)',
      'function list(uint256) view returns (uint256 prev, uint256 next)',
      'function move(uint256 cdp, address dst, uint256 rad)',
      'function open(bytes32 ilk, address usr) returns (uint256)',
      'function owns(uint256) view returns (address)',
      'function quit(uint256 cdp, address dst)',
      'function shift(uint256 cdpSrc, uint256 cdpDst)',
      'function urnAllow(address usr, uint256 ok)',
      'function urnCan(address, address) view returns (uint256)',
      'function urns(uint256) view returns (address)',
      'function vat() view returns (address)'
    ],
    deployer
  )

  const vaultId = await cdpManager.last(makerStrategy.address)
  const ilk = await cdpManager.ilks(vaultId)
  const urn = await cdpManager.urns(vaultId)
  const count = await cdpManager.count(makerStrategy.address)
  const last = await cdpManager.last(makerStrategy.address)
  const vat = await cdpManager.vat()

  return { vaultId, ilk, urn, count, last, vat }
}

async function getVatData (ilk, urn, deployer, amtDeposit) {
  const vatContract = new rawEthers.Contract(
    MCD_VAT,
    [
      'function Line() view returns (uint256)',
      'function cage()',
      'function can(address, address) view returns (uint256)',
      'function dai(address) view returns (uint256)',
      'function debt() view returns (uint256)',
      'function deny(address usr)',
      'function file(bytes32 ilk, bytes32 what, uint256 data)',
      'function file(bytes32 what, uint256 data)',
      'function flux(bytes32 ilk, address src, address dst, uint256 wad)',
      'function fold(bytes32 i, address u, int256 rate)',
      'function fork(bytes32 ilk, address src, address dst, int256 dink, int256 dart)',
      'function frob(bytes32 i, address u, address v, address w, int256 dink, int256 dart)',
      'function gem(bytes32, address) view returns (uint256)',
      'function grab(bytes32 i, address u, address v, address w, int256 dink, int256 dart)',
      'function heal(uint256 rad)',
      'function hope(address usr)',
      'function ilks(bytes32) view returns (uint256 Art, uint256 rate, uint256 spot, uint256 line, uint256 dust)',
      'function init(bytes32 ilk)',
      'function live() view returns (uint256)',
      'function move(address src, address dst, uint256 rad)',
      'function nope(address usr)',
      'function rely(address usr)',
      'function sin(address) view returns (uint256)',
      'function slip(bytes32 ilk, address usr, int256 wad)',
      'function suck(address u, address v, uint256 rad)',
      'function urns(bytes32, address) view returns (uint256 ink, uint256 art)',
      'function vice() view returns (uint256)',
      'function wards(address) view returns (uint256)'
    ],
    deployer
  )
  const gem = await vatContract.gem(ilk, urn)
  const dai = await vatContract.dai(urn)

  const vatUrns = await vatContract.urns(ilk, urn)
  const vatIlks = await vatContract.ilks(ilk)
  const vatUrn = {
    ink: BigInt(vatUrns.ink),
    art: BigInt(vatUrns.art)
  }
  const vatIlk = {
    Art: BigInt(vatIlks.Art),
    rate: BigInt(vatIlks.rate),
    spot: BigInt(vatIlks.spot),
    line: BigInt(vatIlks.line),
    dust: BigInt(vatIlks.dust)
  }

  const amtBorrowMax = rawEthers.utils.parseUnits(
    ((vatIlk.spot * BigInt(amtDeposit)) / BigInt(10 ** (27 + 18))).toString(),
    18
  )

  const dink = BigInt(amtDeposit)
  const dart = BigInt(amtBorrowMax)

  vatUrn.ink = BigInt(vatUrn.ink) + dink
  vatUrn.art = BigInt(vatUrn.art) + dart
  vatIlk.Art = BigInt(vatIlk.Art) + dart

  let debt = await vatContract.debt()
  const dtab = BigInt(vatIlk.rate) * dart
  const tab = BigInt(vatIlk.rate) * vatUrn.art
  debt = BigInt(debt) + dtab

  return { gem, dai, vatUrn, vatIlk, dtab, tab, debt, dink, dart, amtBorrowMax }
}

function formatUnits (value, decimals) {
  return +rawEthers.utils.formatUnits(value, decimals)
}

function parseBytes32String (target) {
  return rawEthers.utils.parseBytes32String(target)
}

async function main () {
  const [deployer] = await ethers.getSigners()

  // Step1: Empty string for makerstrategy
  const makerStrategyAddr = '0x38A70c040CA5F5439ad52d0e821063b0EC0B52b6'
  const makerStrategy = await setupMakerStrategy(makerStrategyAddr)
  const MCD_LIST = {
    'ETH-A': {
      type: 'ETH-A',
      mcdJoinAddr: MCD_JOIN_ETH_A,
      decimals: 18
    },
    'UNIV2USDCETH-A': {
      type: 'UNIV2USDCETH-A',
      mcdJoinAddr: MCD_JOIN_UNIV2USDCETH_A,
      decimals: 18,
      tokenInAddr: '0xB4e16d0168e52d35CaCD2c6185b44281Ec28C9Dc'
    }
  }
  // const colType = 'ETH-A'
  const colType = 'UNIV2USDCETH-A'

  // step2: uncomment await openMakerVault and return (so it stops here)
  // NOTE: Open Vault
  if (makerStrategyAddr === '') {
    await openMakerVault(makerStrategy, colType)
    return
  }

  const vaultData = await getVaultData(makerStrategy, deployer)
  console.log({ vaultData })

  let amtIn
  let tokenIn
  if (colType === 'UNIV2USDCETH-A') {
    amtIn = 0.0005
    tokenIn = Tokens[MCD_LIST[colType].tokenInAddr]
  } else if (colType === 'ETH-A') {
    amtIn = 20
    tokenIn = {
      name: 'ETH',
      symbol: 'ETH',
      decimals: 18
    }
  }
  const amtDeposit = rawEthers.utils.parseUnits(
    amtIn.toString(),
    tokenIn.decimals
  )
  const vatData = await getVatData(
    vaultData.ilk,
    vaultData.urn,
    deployer,
    amtDeposit
  )
  console.log({ vatData })

  const minDebt = formatUnits(vatData.vatIlk.dust, 45)
  const borrowAmt = formatUnits(vatData.amtBorrowMax, 18)
  console.log(
    parseBytes32String(vaultData.ilk),
    'minimum vault debt:',
    minDebt,
    'DAI'
  )
  console.log(`${amtIn} ${tokenIn.name} can get max of`, borrowAmt, 'DAI')
  if (borrowAmt < minDebt) {
    console.log('need more deposits')
    return
  }
  //
  // // step 3: Deposit Liquidity into Maker Vault
  // // NOTE: Deposit
  // await deposit(makerStrategy, vaultData.vaultId, amtDeposit, MCD_LIST[colType])
  //
  // // step 4: Borrow DAI from MakerVault
  // // NOTE: Borrow DAI
  // await borrowDAI(makerStrategy, vaultData.vaultId, vatData.amtBorrowMax)

  // NOTE: Deposit ETH-A and Withdraw DAI in a single tx
  // await depositAndBorrow(
  //   makerStrategy,
  //   vaultData.vaultId,
  //   amtDeposit,
  //   vatData.amtBorrowMax,
  //   MCD_LIST[colType]
  // )

  const amtPayback = await getVaultDebt(
    makerStrategy,
    vaultData.vat,
    vaultData.ilk,
    vaultData.urn
  )
  console.log('Payback amount:', formatUnits(amtPayback, 18))

  // Step 5: Paypal DAI to Maker Vault
  // NOTE: Payback DAI
  // await payback(makerStrategy, vaultData.vaultId, amtPayback)

  // step 6: Final step uncomment to withdraw from Maker Vault
  // NOTE: Withdraw collateral to MakerStrategy
  await withdraw(
    makerStrategy,
    vaultData.vaultId,
    amtDeposit,
    MCD_LIST[colType]
  )
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })

// wad: token unit amount
// gem: collateral token adapter
// ilk: Vault type
// urn: Vault record – keeps track of a Vault
// ink: rate * wad represented in collateral
// dink: delta ink – a signed difference value to the current value
// art: rate * wad represented in DAI
// dart: delta art – a signed difference value to the current value
// lad: Vault owner
// rat: collateralization ratio.

// dai: 45 decimals [rad]
// rate: 27 decimals [ray]
// dink: 18 decimals [wad]
// dart: 18 decimals [wad]
