const { ethers: rawEthers } = require('ethers')
const json = require('../artifacts/contracts/UniswapV2.sol/UniswapV2.json')

const getFunctionSignatures = (abi) => {
  const i = new rawEthers.utils.Interface(abi)
  return Object.keys(i.functions).forEach((f) => {
    console.log(f, i.getSighash(i.functions[f]))
  })
}
getFunctionSignatures(json.abi)
