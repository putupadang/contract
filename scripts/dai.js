const Maker = require('@makerdao/dai').default
const { McdPlugin } = require('@makerdao/dai-plugin-mcd')

async function getAllVaults (manager, deployer) {
  // const proxyAddress = await maker.service('proxy').currentProxy()
  const data = await manager.getCdpIds(deployer.address)
  console.log(data)
}

function getAllCdpTypes (service) {
  service.cdpTypes.forEach((type) => console.log(type.ilk))
}

async function getCdpByVaultId (manager, vaultId) {
  const vault = await manager.getCdp(vaultId)
  return vault
}

async function main () {
  const infuraKey = process.env.INFURA_API_KEY
  const maker = await Maker.create('http', {
    plugins: [McdPlugin],
    url: `https://mainnet.infura.io/v3/${infuraKey}`
  })

  // const [deployer] = await ethers.getSigners()
  // const manager = maker.service('mcd:cdpManager')
  const service = maker.service('mcd:cdpType')

  // await getAllVaults(manager, deployer)
  // const vaultId = 28208
  // const cdp = await getCdpByVaultId(manager, vaultId)
  // console.log('cdp:', cdp)

  getAllCdpTypes(service)

  // const ethA = service.getCdpType(null, 'ETH-A')
  // console.log(ethA)
  // console.log(ethA.totalCollateral)
  // console.log(ethA.liquidationRatio)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
