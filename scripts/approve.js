const { ethers: rawEthers } = require('ethers')

async function main () {
  const PairInfo = {
    factory: '0x9fE46736679d2D9a65F0992F2272dE9f3c7fa6e0',
    router: '0xCf7Ed3AccA5a467e9e704C703E8D87F634fB0Fc9',
    pair: '0xDc64a140Aa3E981100a9becA4E685f962f0cF6C9',
    WETH: {
      address: '0x5FbDB2315678afecb367f032d93F642f64180aa3',
      decimals: 18
    },
    USDC: {
      address: '0xe7f1725E7734CE288F8367e1Bb143E90bb3F0512',
      decimals: 6
    }
  }

  const wallet = new ethers.Wallet(process.env.WALLET_PRIVATE_KEY)
  const signer = wallet.connect(ethers.provider)

  for (const sym of ['WETH', 'USDC']) {
    const token = PairInfo[sym]

    const contract = new rawEthers.Contract(
      token.address,
      [
        'function decimals() public view returns (uint8)',
        'function symbol() public view returns (string memory)',
        'function allowance(address owner, address spender) public view returns (uint256)',
        'function approve(address spender, uint256 amount) public returns (bool)'
      ],
      signer
    )
    const allowanceBefore = await contract.allowance(
      wallet.address,
      PairInfo.pair
    )
    console.log(
      `${sym} allowanceBefore:`,
      rawEthers.utils.formatUnits(allowanceBefore, token.decimals)
    )

    const amount = '1000000'
    const approve = await contract.approve(PairInfo.pair, amount)
    await approve.wait()

    const allowanceAfter = await contract.allowance(
      wallet.address,
      PairInfo.pair
    )
    console.log(
      `${sym} allowanceAfter:`,
      rawEthers.utils.formatUnits(allowanceAfter, token.decimals)
    )
  }
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
