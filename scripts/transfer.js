const { ethers: rawEthers } = require('ethers')
const IERC20 = require('@openzeppelin/contracts/build/contracts/IERC20.json')
const Tokens = {
  DAI: {
    address: '0x6B175474E89094C44Da98b954EedeAC495271d0F',
    decimals: 18,
    name: 'Dai Stablecoin',
    symbol: 'DAI'
  },
  USDC: {
    address: '0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48',
    decimals: 6,
    name: 'USD Coin',
    symbol: 'USDC'
  },
  WETH: {
    address: '0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2',
    decimals: 18,
    name: 'Wrapped Ether',
    symbol: 'WETH'
  },
  WBTC: {
    address: '0x2260FAC5E5542a773Aa44fBCfeDf7C193bc2C599',
    decimals: 8,
    name: 'Wrapped BTC',
    symbol: 'WBTC'
  }
}

async function getTokenContracts () {
  const tokenContracts = {}
  for (const symbol in Tokens) {
    const token = Tokens[symbol]
    tokenContracts[symbol] = await ethers.getContractAt(
      IERC20.abi,
      token.address
    )
  }
  return tokenContracts
}

async function main () {
  const tokenContracts = await getTokenContracts()
  const target = {
    name: 'MakerStrategy',
    address: '0x63fea6E447F120B8Faf85B53cdaD8348e645D80E',
    amount: '100'
  }

  await tokenContracts.DAI.transfer(
    target.address,
    rawEthers.utils.parseUnits(target.amount.toString(), Tokens.DAI.decimals)
  )
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
