const { expect } = require('chai')
const { ethers } = require('hardhat')
const { ethers: rawEthers } = require('ethers')
const { parseUnits, formatUnits } = ethers.utils
const UniswapV2JSON = require('../artifacts/contracts/UniswapV2.sol/UniswapV2.json')

describe('PairVault', () => {
  let factory, router, strategy, pair, deployer, balances, user, user2, user3
  let WETH, WBTC, USDC, DAI

  before(async () => {
    if (factory && router) {
      return
    }

    [deployer, user, user2, user3] = await ethers.getSigners()

    // deploy dummy token, set global variable, get balance.
    const tokenInfo = await setupTokens(user, {
      WETH: { amount: 10, symbol: 'WETH', decimals: 18 },
      WBTC: { amount: 0.1, symbol: 'WBTC', decimals: 8 },
      USDC: { amount: 10000, symbol: 'USDC', decimals: 6 },
      DAI: { amount: 10000, symbol: 'DAI', decimals: 18 }
    })
    balances = tokenInfo.balances
    WETH = tokenInfo.tokens.WETH
    WBTC = tokenInfo.tokens.WBTC
    USDC = tokenInfo.tokens.USDC
    DAI = tokenInfo.tokens.DAI

    // Wait until WETH been setup in global variable
    ;[factory, router] = await setupUniswap(deployer)

    const PairVault = await ethers.getContractFactory('PairVault')

    // ensure token0 < token1
    expect(!!WETH.address < USDC.address).to.equal(true)

    const UniStrategyFactory = await ethers.getContractFactory('UniswapV2')
    strategy = await UniStrategyFactory.deploy(router.address)

    pair = await PairVault.deploy(WETH.address, USDC.address, strategy.address)
    await pair.deployed()

    console.log('deployer:', deployer.address)
    console.log('user:', user.address)
    console.log('factory:', factory.address)
    console.log('router:', router.address)
    console.log('pair:', pair.address)

    console.log(
      'WETH:',
      WETH.address,
      formatUnits(balances.WETH),
      balances.WETH
    )
    console.log(
      'USDC:',
      USDC.address,
      formatUnits(balances.USDC, 6),
      balances.USDC
    )
    console.log('DAI:', DAI.address, formatUnits(balances.DAI))
  })

  describe('deployment', () => {
    it('should initialise with token0, token1', async function () {
      expect(await pair.token0()).to.equal(WETH.address)
      expect(await pair.token1()).to.equal(USDC.address)
    })

    it('should intialise uniswap v2 default strategy', async function () {
      expect(await pair.strategy()).to.equal(strategy.address)
    })

    it('sets allowance to make balance spendable by Uniswap Router')
  })

  describe('deposit', () => {
    let pool, pairDeposit

    const wethAmount = parseUnits('0.5')
    const usdcAmount = parseUnits('1500', 6)

    before(async () => {
      pool = await setupUniswapPool(
        WETH,
        USDC,
        parseUnits('0.01'),
        parseUnits('30', 6)
      )

      WETH = WETH.connect(user)
      USDC = USDC.connect(user)
      pair = pair.connect(user)

      await approve(wethAmount, usdcAmount)
      pairDeposit = pair.deposit(wethAmount, usdcAmount)
    })

    it('deposit 0.5ETH, 1500 USDC (ETH $3000)', async () => {
      await pairDeposit

      expect(await WETH.balanceOf(pair.address)).to.equal(0)
      expect(await USDC.balanceOf(pair.address)).to.equal(0)

      // owner wallet balances decrease
      expect(await WETH.balanceOf(user.address)).to.equal(
        balances.WETH.sub(wethAmount)
      )
      expect(await USDC.balanceOf(user.address)).to.equal(
        balances.USDC.sub(usdcAmount)
      )
    })

    it('emits Deposit(owner,amount0,amount1) event', async () => {
      await expect(pairDeposit)
        .to.emit(pair, 'Deposit')
        .withArgs(user.address, wethAmount, usdcAmount)
    })

    it('records user position at positions[owner][address(Contract)]', async () => {
      await pairDeposit

      const block = await ethers.provider.getBlock('latest')

      const position = await pair.positions(user.address, pair.address)
      const [amount, debt, blocktime, available, info] = position

      // amount (or liquidity) ? how is this calculated?

      const [totalSupply, minLiquidity] = [
        await pool.totalSupply(),
        await pool.MINIMUM_LIQUIDITY()
      ]

      const [reserve0, reserve1] = await pool.getReserves()

      console.log('pool total supply, min_liquidity', totalSupply, minLiquidity)

      console.log('Pool Reserves:', [reserve0, reserve1])

      let liquidity
      if (totalSupply === 0) {
        liquidity = BigSqrt(
          wethAmount.mul(usdcAmount).sub(minLiquidity).toBigInt()
        )
      } else {
        liquidity = Math.min(
          wethAmount.mul(totalSupply).div(reserve0),
          usdcAmount.mul(totalSupply).div(reserve1)
        )
      }
      liquidity = ethers.BigNumber.from(liquidity)

      // First LP liquidity would substract  Pool's MINIMUM_LIQUIDITY
      expect(amount).to.equal(liquidity)
      expect(debt.toString()).to.equal('0')
      expect(blocktime).to.equal(block.timestamp)
      expect(available.toString()).to.equal('0')

      // info (16bytes of extra info or pointer)
      expect(info).to.equal('0x00000000000000000000000000000000')
    })

    it('allocate increased position on new deposit', async () => {
      // previous deposit 0.5ETH $1500
      const wethAmount2 = parseUnits('0.4')
      const usdcAmount2 = parseUnits('1200', 6)
      await approve(wethAmount2, usdcAmount2)

      const [liquidity0] = await pair.positions(user.address, pair.address)

      await pair.deposit(wethAmount2, usdcAmount2)

      const [liquidity1] = await pair.positions(user.address, pair.address)

      expect(await WETH.balanceOf(user.address)).to.equal(
        // user initial balance
        balances.WETH.sub(wethAmount).sub(wethAmount2)
      )
      // liquidity increased after second deposit
      expect(liquidity1 > liquidity0).to.equal(true)
    })

    it('runs default strategy#allocate', async () => {
      expect(await pool.getReserves()).to.exist
      expect(await pair.pool()).to.equal(pool.address)

      await expect(pairDeposit)
        .to.emit(strategy, 'Allocate')
        .withArgs(pool.address, wethAmount, usdcAmount)
    })

    it('runs addons strategy#allocate for each addon strategy', async () => {
      // setup uni v2 LP pool for usdc eth
      const wethAmount2 = parseUnits('0.4')
      const usdcAmount2 = parseUnits('1200', 6)
      await approve(wethAmount2, usdcAmount2)

      const DummyAddon = await ethers.getContractFactory('DummyStrategy')
      const leverage = await DummyAddon.deploy('Leverage')
      const staking = await DummyAddon.deploy('Staking')
      const theta = await DummyAddon.deploy('Theta')

      await pair.register(leverage.address)
      await pair.register(staking.address)
      await pair.register(theta.address)

      expect(await pair.addons(0)).to.equal(leverage.address)
      expect(await pair.addons(1)).to.equal(staking.address)
      expect(await pair.addons(2)).to.equal(theta.address)

      // addon
      const pairDeposit = pair.deposit(wethAmount2, usdcAmount2)

      await expect(await pairDeposit)
        .to.emit(leverage, 'Allocate')
        .withArgs(user.address, wethAmount2, usdcAmount2)

      await expect(await pairDeposit)
        .to.emit(staking, 'Allocate')
        .withArgs(user.address, wethAmount2, usdcAmount2)

      await expect(await pairDeposit)
        .to.emit(theta, 'Allocate')
        .withArgs(user.address, wethAmount2, usdcAmount2)

      console.log(
        'USDC balance',
        formatUnits(await USDC.balanceOf(pair.address), 6)
      )
    })
  })

  describe('#allocate', async () => {
    const wethAmount = parseUnits('0.4')
    const usdcAmount = parseUnits('1200', 6)

    it('requires positive token0, token1 balance', async () => {
      await WETH.transfer(pair.address, wethAmount)
      await USDC.transfer(pair.address, usdcAmount)

      console.log(
        'USDC balance',
        formatUnits(await USDC.balanceOf(pair.address), 6)
      )

      // console.log("WETH after: ", await WETH.balanceOf(owner.address))
      const [liquidity0] = await pair.positions(user.address, pair.address)

      expect(await pair.allocate(wethAmount, usdcAmount)).to.exist

      const [liquidity1] = await pair.positions(user.address, pair.address)

      expect(liquidity1 > liquidity0).to.equal(true)
    })

    it('revert on empty balance', async () => {
      console.log(
        'USDC balance',
        formatUnits(await USDC.balanceOf(pair.address), 6)
      )

      expect(await WETH.balanceOf(pair.address)).to.equal(0)
      expect(await USDC.balanceOf(pair.address)).to.equal(0)

      await expect(pair.allocate(wethAmount, usdcAmount)).to.be.reverted
    })
  })

  describe('#free', async () => {
    const wethAmount = parseUnits('0.5')
    const usdcAmount = parseUnits('1500', 6)

    it('remove allocation from Uniswap V2 Pool and token0, token1 balance available in pair contract', async () => {
      // console.log("WETH after: ", await WETH.balanceOf(owner.address))
      expect(await WETH.balanceOf(pair.address)).to.equal(0)
      expect(await USDC.balanceOf(pair.address)).to.equal(0)

      const [liquidity0] = await pair.positions(user.address, pair.address)

      // eth_call get return value from current chainstate from rpc node
      const result = await pair.callStatic.free(wethAmount, usdcAmount)
      const [amount0, amount1, liquidity] = result

      // eth_call tx which will be mined on the next block
      await pair.free(wethAmount, usdcAmount)

      const [liquidity1] = await pair.positions(user.address, pair.address)

      expect(liquidity1 < liquidity0).to.equal(true)

      expect(amount0).to.equal(wethAmount)
      expect(amount1).to.equal(usdcAmount)
      //
      expect(await WETH.balanceOf(pair.address)).to.equal(amount0)
      expect(await USDC.balanceOf(pair.address)).to.equal(amount1)
    })

    it('revert when withdraw larger than position liquidity', async () => {
      const wethAmount = parseUnits('5000')
      const usdcAmount = parseUnits('1500000', 6)

      await expect(pair.free(wethAmount, usdcAmount)).to.be.reverted
    })
  })

  describe('#withdraw', async () => {
    const wethAmount = parseUnits('0.5')
    const usdcAmount = parseUnits('1500', 6)
    const balance = {}
    let pairWithdraw
    let liquidity0

    before(async () => {
      balance.WETH = await WETH.balanceOf(user.address)
      balance.USDC = await USDC.balanceOf(user.address)
      ;[liquidity0] = await pair.positions(user.address, pair.address)

      // eth_call tx which will be mined on the next block
      pairWithdraw = pair.withdraw(wethAmount, usdcAmount)
    })

    it('invokes #free', async () => {
      await pairWithdraw

      const [liquidity1] = await pair.positions(user.address, pair.address)

      expect(liquidity1 < liquidity0).to.equal(true)
    })

    it('transfer back to users', async () => {
      await pairWithdraw

      const afterBalance = {
        WETH: await WETH.balanceOf(user.address),
        USDC: await USDC.balanceOf(user.address)
      }

      expect(afterBalance.WETH).to.equal(balance.WETH.add(wethAmount))
      expect(afterBalance.USDC).to.equal(balance.USDC.add(usdcAmount))
    })

    it('emits Withdraw(owner,amount0,amount1) event', async () => {
      await expect(pairWithdraw)
        .to.emit(pair, 'Withdraw')
        .withArgs(user.address, wethAmount, usdcAmount)
    })
  })

  describe('Strategy registry: addons', async () => {
    let leverage, staking, theta

    before(async () => {
      const DummyAddon = await ethers.getContractFactory('DummyStrategy')
      leverage = await DummyAddon.deploy('Leverage')
      staking = await DummyAddon.deploy('Staking')
      theta = await DummyAddon.deploy('Theta')

      // manually assign strategy stacks.
      await pair.register(0, leverage.address)
      await pair.register(1, staking.address)
      await pair.register(2, theta.address)
    })

    it('registers strategy on a index', async () => {
      expect((await pair.addons(0)).strategy).to.equal(leverage.address)
      expect((await pair.addons(1)).strategy).to.equal(staking.address)
      expect((await pair.addons(2)).strategy).to.equal(theta.address)
    })

    it('remove a strategy from index', async () => {
      const address0 = ethers.constants.AddressZero

      // manually assign strategy stacks.
      await pair.remove(leverage.address)
      await pair.remove(staking.address)
      await pair.remove(theta.address)

      expect((await pair.addons(0)).strategy).to.equal(address0)
      expect((await pair.addons(1)).strategy).to.equal(address0)
      expect((await pair.addons(2)).strategy).to.equal(address0)
    })
  })

  describe('position stack', async () => {
    let leverage, staking, theta

    before(async () => {
      const DummyAddon = await ethers.getContractFactory('DummyStrategy')
      leverage = (await DummyAddon.deploy('Leverage')).connect(user)
      staking = (await DummyAddon.deploy('Staking')).connect(user)
      theta = (await DummyAddon.deploy('Theta')).connect(user)

      // manually assign strategy stacks.
      await pair.register(0, leverage.address)
      await pair.register(1, staking.address)
      await pair.register(2, theta.address)
    })

    it('sets user position stack', async () => {
      await pair.use(await leverage.selector())
      await pair.use(await staking.selector())
      await pair.use(await theta.selector())

      const stack = await pair.getStack()
      console.log('current user stack:', stack)

      expect(stack[0]).to.equal(await leverage.selector())
      expect(stack[1]).to.equal(await staking.selector())
      expect(stack[2]).to.equal(await theta.selector())

      //
      // await pair.invoke(leverage.address, "draw", 5000);
      //
      // await pair.use([leverage.address]);

      // addons[selector] = struct(
      //   index:0, address,
      // )
      // PairVault.use(StrategyClass)
    })

    it('should avoid duplicated position stack', async () => {
      const stack = await pair.getStack()
      await expect(pair.use(stack[stack.length - 1])).to.be.revertedWith(
        'duplicated-selector'
      )
    })

    it('should be able to pop the last stack', async () => {
      const prevStack = await pair.getStack()
      await pair.popStack()
      const currStack = await pair.getStack()
      expect(currStack.length).to.equal(prevStack.length - 1)
      expect(currStack[0]).to.equal(prevStack[0])
      expect(currStack[1]).to.equal(prevStack[1])
      await pair.popStack()
      await pair.popStack()
      const emptyStack = await pair.getStack()
      expect(emptyStack.length).to.equal(0)
      await expect(pair.popStack()).to.be.revertedWith('invalid-selectors')
    })
  })

  // features: User deposit 0.5 ETH, 1200 USDC, contract receives uni token

  describe('invoke', async () => {
    xit('should be able to call strategy methods', async () => {
      const strategyAddr = await pair.strategy()
      const iface = new rawEthers.utils.Interface(UniswapV2JSON.abi)
      const method = 'testInvoke(address,uint256,string[],bytes)'
      const params = [
        user.address,
        123,
        ['test', 'invoke'],
        ethers.utils.formatBytes32String('abc')
      ]
      const calldata = iface.encodeFunctionData(method, params)
      console.log('[spec:invoke]', { calldata })
      const methodSig = calldata.slice(0, 10)
      const data = '0x' + calldata.slice(10)
      console.log('[spec:invoke]', { methodSig, data })

      const result = await pair.callStatic.invoke(strategyAddr, methodSig, data)
      console.log('[spec:invoke]', { result })
      const decodedResult = iface.decodeFunctionResult(method, result)
      console.log('[spec:invoke]', { decodedResult })
      expect(decodedResult[0]).to.equal(user.address)
      expect(+decodedResult[1]).to.equal(123)
      expect(decodedResult[2][0]).to.equal('test')
      expect(decodedResult[2][1]).to.equal('invoke')
      expect(decodedResult[3]).to.equal(ethers.utils.formatBytes32String('abc'))
      expect(decodedResult[4]).to.equal(200)
    })
  })

  async function setupTokens (owner, tokenList) {
    const Dummy = await ethers.getContractFactory('DummyToken')
    const balances = {}
    const tokens = {}

    for (const name in tokenList) {
      const token = tokenList[name]
      const amount = parseUnits(token.amount.toString(), token.decimals)

      tokens[name] = await Dummy.deploy(amount, token.symbol, token.decimals)
      tokens[name].transfer(owner.address, amount.mul(9).div(10))

      balances[name] = await tokens[name].balanceOf(owner.address)
    }
    return { balances, tokens }
  }

  async function setupUniswap (owner) {
    const UniswapV2Factory = require('@uniswap/v2-core/build/UniswapV2Factory.json')
    const UniswapV2Router02 = require('@uniswap/v2-periphery/build/UniswapV2Router02.json')

    const uniswapV2Factory = new ethers.ContractFactory(
      UniswapV2Factory.abi,
      UniswapV2Factory.bytecode,
      owner
    )
    // Uniswap V2 doesn't take 0.05% fee by default, factory initalised to void address(0)
    // constructor(address _feeToSetter)
    const address0 = '0x0000000000000000000000000000000000000000'
    factory = await uniswapV2Factory.deploy(address0)

    router = new ethers.ContractFactory(
      UniswapV2Router02.abi,
      UniswapV2Router02.bytecode,
      owner
    )
    // constructor(address _factory, address _WETH)
    router = await router.deploy(factory.address, WETH.address)

    return [factory, router]
  }

  async function setupUniswapPool (token0, token1, amount0, amount1) {
    const [deployer] = await ethers.getSigners()
    const deadline = Math.floor(Date.now() / 1000) + 5 * 60

    // setup uni v2 LP pool for usdc eth
    const createPair = await factory.createPair(token0.address, token1.address)
    const pool = await factory.getPair(token0.address, token1.address)
    const contract = await ethers.getContractAt('IUniswapV2Pair', pool)

    await expect(createPair)
      .to.emit(factory, 'PairCreated')
      .withArgs(token0.address, token1.address, pool, 1)

    await token0.approve(router.address, amount0)
    await token1.approve(router.address, amount1)

    const tx = await router.addLiquidity(
      token0.address,
      token1.address,
      amount0,
      amount1,
      0,
      0,
      deployer.address,
      deadline
    )
    await tx.wait()

    // USDCWETH LP Pool
    return contract
  }

  function BigSqrt (value) {
    if (value < 0n) {
      throw 'square root of negative numbers is not supported'
    }

    if (value < 2n) {
      return value
    }

    function newtonIteration (n, x0) {
      const x1 = (n / x0 + x0) >> 1n
      if (x0 === x1 || x0 === x1 - 1n) {
        return x0
      }
      return newtonIteration(n, x1)
    }

    return newtonIteration(value, 1n)
  }

  async function approve (wethAmount, usdcAmount) {
    await WETH.approve(pair.address, wethAmount)
    await USDC.approve(pair.address, usdcAmount)
  }
})
