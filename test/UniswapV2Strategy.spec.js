const { expect } = require('chai')
const { ethers } = require('hardhat')
const { ethers: rawEthers } = require('ethers')
const {
  parseEther,
  parseUnits,
  formatUnits,
  formatBytes32String,
  parseBytes32String
} = rawEthers.utils
const IERC20 = require('@openzeppelin/contracts/build/contracts/IERC20.json')
const UniswapV2Factory = require('@uniswap/v2-core/build/UniswapV2Factory.json')
const UniswapV2Router02 = require('@uniswap/v2-periphery/build/UniswapV2Router02.json')

const Addresses = {
  Uniswap: {
    v2factory: '0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f',
    v2router: '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D'
  },
  Tokens: {
    DAI: {
      address: '0x6B175474E89094C44Da98b954EedeAC495271d0F',
      decimals: 18,
      name: 'Dai Stablecoin',
      symbol: 'DAI'
    },
    USDC: {
      address: '0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48',
      decimals: 6,
      name: 'USD Coin',
      symbol: 'USDC'
    },
    WETH: {
      address: '0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2',
      decimals: 18,
      name: 'Wrapped Ether',
      symbol: 'WETH'
    },
    WBTC: {
      address: '0x2260FAC5E5542a773Aa44fBCfeDf7C193bc2C599',
      decimals: 8,
      name: 'Wrapped BTC',
      symbol: 'WBTC'
    }
  }
}

describe('UniswapV2:Strategy', () => {
  let pair, deployer, user, v2factory, v2router, strategy
  let poolAddr, v2Pool
  let WETH, USDC, DAI

  before(async () => {
    [deployer, user] = await ethers.getSigners()
    const tokens = await setupTokens(deployer, [
      { amount: 40, symbol: 'WETH', decimals: 18 },
      { amount: 100_000, symbol: 'USDC', decimals: 6 },
      { amount: 100_000, symbol: 'DAI', decimals: 18 }
    ])
    await setupUserBalance(deployer, user, tokens, {
      WETH: 20,
      DAI: 10_000,
      USDC: 50_000
    })
    WETH = tokens.WETH
    USDC = tokens.USDC
    DAI = tokens.DAI
    ;[v2factory, v2router] = await setupUniswap()

    const PairVault = await ethers.getContractFactory('PairVault')
    const UniStrategyFactory = await ethers.getContractFactory('UniswapV2')
    strategy = await UniStrategyFactory.deploy(v2router.address)
    strategy = strategy.connect(user)

    pair = await PairVault.deploy(USDC.address, WETH.address, strategy.address)

    console.log('[spec:before] deployer', deployer.address)
    console.log('[spec:before] user', user.address)
    console.log('[spec:before] pair', pair.address)
    console.log('[spec:before] strategy', strategy.address)

    poolAddr = await pair.pool()
    v2Pool = await ethers.getContractAt('IUniswapV2ERC20', poolAddr)

    await printBalances({ WETH, USDC, DAI })
  })

  after(async () => {
    await printBalances({ WETH, USDC, DAI })
  })

  describe('1. deployment', () => {
    it('1.1 should initialize uniswap v2 as the default strategy', async () => {
      expect(await pair.strategy()).to.equal(strategy.address)
    })

    it('1.2 should initialize router & factory', async () => {
      expect(await strategy.factory()).to.equal(v2factory.address)
      expect(await strategy.router()).to.equal(v2router.address)
    })
  })

  describe('2. initialize', () => {
    it('2.1 should set infinite token allowances for uni router', async () => {
      const token0 = await pair.token0()
      const token1 = await pair.token1()
      const uint256Max = rawEthers.BigNumber.from(2n ** 256n - 1n)
      expect(token0).to.equal(USDC.address)
      expect(token1).to.equal(WETH.address)

      const allowances = {
        uniUSDC: await USDC.allowance(strategy.address, v2router.address),
        uniWETH: await WETH.allowance(strategy.address, v2router.address)
      }
      expect(allowances.uniUSDC).to.equal(uint256Max)
      expect(allowances.uniWETH).to.equal(uint256Max)
    })

    it('2.2 should return uni pool address', async () => {
      const token0 = await pair.token0()
      const token1 = await pair.token1()
      const bytes32PoolAddr = await strategy.callStatic.initialize(
        token0,
        token1
      )
      const poolAddr = await pair.pool()

      expect(rawEthers.utils.getAddress(bytes32PoolAddr.slice(0, 42))).to.equal(
        poolAddr
      )
    })
  })

  describe('3. swap', () => {
    it('3.1 should check and validate inputs', async () => {
      const AddressZero = rawEthers.constants.AddressZero
      const amountIn = 1
      const invalidAmountIn = strategy.swap(USDC.address, DAI.address, 0)
      const invalidToken0 = strategy.swap(AddressZero, DAI.address, amountIn)
      const invalidToken1 = strategy.swap(USDC.address, AddressZero, amountIn)

      await expect(invalidAmountIn).to.be.revertedWith('invalid-amountIn')
      await expect(invalidToken0).to.be.revertedWith('invalid-token0')
      await expect(invalidToken1).to.be.revertedWith('invalid-token1')
    })

    it("3.2 should return token1's amountOut as output", async () => {
      const token0 = WETH.address
      const token1 = DAI.address
      const amountIn = parseUnits('1', 18)

      WETH.connect(user).approve(strategy.address, amountIn, {})
      const result = await strategy.callStatic.swap(token0, token1, amountIn)
      expect(+result.amountOutMain).to.greaterThan(0)
    })

    it('3.3 should allow msg.sender to withdraw output after swap', async () => {
      const token0 = WETH.address
      const token1 = DAI.address
      const amountIn = parseUnits('1', 18)

      WETH.connect(user).approve(strategy.address, amountIn)
      const result = await strategy.callStatic.swap(token0, token1, amountIn)
      await strategy.swap(token0, token1, amountIn)

      const balanceBefore = await DAI.balanceOf(user.address)
      await DAI.connect(user).transferFrom(
        strategy.address,
        user.address,
        result.amountOutMain
      )
      const balanceAfter = await DAI.balanceOf(user.address)
      expect(+balanceAfter).to.greaterThan(+balanceBefore)
      expect(+balanceAfter.sub(balanceBefore)).to.equal(+result.amountOutMain)
    })

    it('3.4 should support swap DAI to Uni-V2 LP token', async () => {
      const UniV2UsdcWeth = {
        address: '0xB4e16d0168e52d35CaCD2c6185b44281Ec28C9Dc',
        decimals: 18,
        name: 'UNI-V2-USDC-WETH',
        symbol: 'UNI-V2-USDC-WETH'
      }
      const lpBalanceBefore = await v2Pool.balanceOf(user.address)
      const token0 = DAI.address
      const token1 = UniV2UsdcWeth.address
      const amountIn = parseUnits('1000', 18)
      DAI.connect(user).approve(strategy.address, amountIn)
      const result = await strategy.callStatic.swap(token0, token1, amountIn)
      await strategy.swap(token0, token1, amountIn)
      await v2Pool
        .connect(user)
        .transferFrom(strategy.address, user.address, result.amountOutMain)
      const lpBalanceAfter = await v2Pool.balanceOf(user.address)

      expect(+lpBalanceAfter).to.greaterThan(+lpBalanceBefore)
      expect(+lpBalanceAfter.sub(lpBalanceBefore)).to.equal(
        +result.amountOutMain
      )

      const allowanceUSDC = await USDC.allowance(strategy.address, user.address)
      const allowanceWETH = await WETH.allowance(strategy.address, user.address)
      if (allowanceUSDC.gt(0)) {
        await USDC.connect(user).transferFrom(
          strategy.address,
          user.address,
          allowanceUSDC
        )
      }
      if (allowanceWETH.gt(0)) {
        await WETH.connect(user).transferFrom(
          strategy.address,
          user.address,
          allowanceWETH
        )
      }
    })

    it('3.5 should support withdraw USDC & WETH after swap DAI to Uni-V2 LP token', async () => {
      const UniV2UsdcWeth = {
        address: '0xB4e16d0168e52d35CaCD2c6185b44281Ec28C9Dc',
        decimals: 18,
        name: 'UNI-V2-USDC-WETH',
        symbol: 'UNI-V2-USDC-WETH'
      }
      const token0 = DAI.address
      const token1 = UniV2UsdcWeth.address
      const amountIn = parseUnits('1000', 18)
      DAI.connect(user).approve(strategy.address, amountIn)
      const result = await strategy.callStatic.swap(token0, token1, amountIn)
      await strategy.swap(token0, token1, amountIn)
      await v2Pool
        .connect(user)
        .transferFrom(strategy.address, user.address, result.amountOutMain)
      const allowanceUSDC = await USDC.allowance(strategy.address, user.address)
      const allowanceWETH = await WETH.allowance(strategy.address, user.address)
      expect(allowanceUSDC.gt(0) || allowanceWETH.gt(0)).to.be.true

      if (allowanceUSDC.gt(0)) {
        const balanceBefore = await USDC.balanceOf(user.address)
        await USDC.connect(user).transferFrom(
          strategy.address,
          user.address,
          allowanceUSDC
        )
        const balanceAfter = await USDC.balanceOf(user.address)
        expect(balanceAfter.gt(balanceBefore)).to.be.true
      }
      if (allowanceWETH.gt(0)) {
        const balanceBefore = await WETH.balanceOf(user.address)
        await WETH.connect(user).transferFrom(
          strategy.address,
          user.address,
          allowanceWETH
        )
        const balanceAfter = await WETH.balanceOf(user.address)
        expect(balanceAfter.gt(balanceBefore)).to.be.true
      }
    })

    it('3.6 should support swap Uni-V2 LP token to DAI', async () => {
      const UniV2UsdcWeth = {
        address: '0xB4e16d0168e52d35CaCD2c6185b44281Ec28C9Dc',
        decimals: 18,
        name: 'UNI-V2-USDC-WETH',
        symbol: 'UNI-V2-USDC-WETH'
      }
      const daiBalanceBefore = await DAI.balanceOf(user.address)
      const lpBalance = await v2Pool.balanceOf(user.address)
      expect(lpBalance.gt(0)).to.be.true

      const token0 = UniV2UsdcWeth.address
      const token1 = DAI.address
      v2Pool.connect(user).approve(strategy.address, lpBalance)
      const result = await strategy.callStatic.swap(token0, token1, lpBalance)
      await strategy.swap(token0, token1, lpBalance)
      await DAI.connect(user).transferFrom(
        strategy.address,
        user.address,
        result.amountOutMain
      )
      const daiBalanceAfter = await DAI.balanceOf(user.address)
      expect(+daiBalanceAfter).to.greaterThan(+daiBalanceBefore)
      expect(+daiBalanceAfter.sub(daiBalanceBefore)).to.equal(
        +result.amountOutMain
      )
    })
  })

  async function setupTokens (deployer, targetList) {
    const tokenContracts = {}
    const Uniswap = Addresses.Uniswap
    const v2router = await ethers.getContractAt(
      UniswapV2Router02.abi,
      Uniswap.v2router
    )

    for (const target of targetList) {
      const symbol = target.symbol
      const token = Addresses.Tokens[target.symbol]
      tokenContracts[symbol] = await ethers.getContractAt(
        IERC20.abi,
        token.address
      )
      if (!tokenContracts[symbol].decimals) {
        tokenContracts[symbol].decimals = async () => {
          return target.decimals
        }
      }
      if (symbol === 'WETH') {
        const amount = parseUnits(target.amount.toString(), target.decimals)
        const tx = await deployer.sendTransaction({
          to: token.address,
          value: amount
        })
        await tx.wait()
      } else {
        const amountOut = parseUnits(target.amount.toString(), target.decimals)
        const path = [tokenContracts.WETH.address, token.address]
        const to = deployer.address
        const deadline = Math.trunc(Date.now() / 1000) + 5 * 60
        await uniSwapTokens(v2router, amountOut, path, to, deadline)
      }
    }
    return tokenContracts
  }

  async function setupUniswap () {
    const v2factory = await ethers.getContractAt(
      UniswapV2Factory.abi,
      Addresses.Uniswap.v2factory
    )
    const v2router = await ethers.getContractAt(
      UniswapV2Router02.abi,
      Addresses.Uniswap.v2router
    )

    return [v2factory, v2router]
  }

  async function uniSwapTokens (v2router, amountOut, path, to, deadline) {
    const amountIn = await v2router.getAmountsIn(amountOut, path)
    await v2router.swapETHForExactTokens(amountOut, path, to, deadline, {
      value: amountIn[0]
    })
  }

  async function setupUserBalance (sender, receiver, tokens, amount) {
    if (sender.address === receiver.address) {
      return
    }
    for (const sym in amount) {
      if (sym === 'ETH') {
        const tx = await sender.sendTransaction({
          to: receiver.address,
          value: parseEther(amount[sym].toString())
        })
        await tx.wait()
      } else {
        await tokens[sym].transfer(
          receiver.address,
          parseUnits(amount[sym].toString(), await tokens[sym].decimals())
        )
      }
    }
  }

  async function printBalances (tokens) {
    console.log('\n--- balances ---')
    const deployerBalance = await fetchBalance(deployer, tokens)
    const userBalance = await fetchBalance(user, tokens)
    const strategyBalance = await fetchBalance(strategy, tokens)
    const uniV2UsdcWeth = {
      address: '0xB4e16d0168e52d35CaCD2c6185b44281Ec28C9Dc',
      decimals: 18,
      name: 'UNI-V2-USDC-WETH',
      symbol: 'UNI-V2-USDC-WETH'
    }

    for (const sym in deployerBalance) {
      if (sym === 'ETH') {
        console.log(
          `[deployer balance] ${sym}:`,
          formatUnits(deployerBalance[sym], 'ether')
        )
      } else {
        console.log(
          `[deployer balance] ${sym}:`,
          formatUnits(deployerBalance[sym], await tokens[sym].decimals())
        )
      }
    }
    console.log(
      `[deployer balance] ${uniV2UsdcWeth.symbol}:`,
      formatUnits(
        await v2Pool.balanceOf(deployer.address),
        uniV2UsdcWeth.decimals
      )
    )

    for (const sym in userBalance) {
      if (sym === 'ETH') {
        console.log(
          `[user balance] ${sym}:`,
          formatUnits(userBalance[sym], 'ether')
        )
      } else {
        console.log(
          `[user balance] ${sym}:`,
          formatUnits(userBalance[sym], await tokens[sym].decimals())
        )
      }
    }
    console.log(
      `[user balance] ${uniV2UsdcWeth.symbol}:`,
      formatUnits(await v2Pool.balanceOf(user.address), uniV2UsdcWeth.decimals)
    )

    for (const sym in strategyBalance) {
      if (sym === 'ETH') {
        console.log(
          `[strategy balance] ${sym}:`,
          formatUnits(strategyBalance[sym], 'ether')
        )
      } else {
        console.log(
          `[strategy balance] ${sym}:`,
          formatUnits(strategyBalance[sym], await tokens[sym].decimals())
        )
      }
    }
    console.log(
      `[strategy balance] ${uniV2UsdcWeth.symbol}:`,
      formatUnits(
        await v2Pool.balanceOf(strategy.address),
        uniV2UsdcWeth.decimals
      )
    )
  }
  async function fetchBalance (target, tokens) {
    const balance = {}
    balance.ETH = await ethers.provider.getBalance(target.address)
    for (const name in tokens) {
      balance[name] = await tokens[name].balanceOf(target.address)
    }
    return balance
  }
})
