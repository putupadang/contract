const { expect } = require('chai')
const { ethers } = require('hardhat')
const { ethers: rawEthers } = require('ethers')
const { parseUnits, formatUnits } = rawEthers.utils
const IERC20 = require('@openzeppelin/contracts/build/contracts/IERC20.json')
const ContractSetup = require('../lib/contract-setup')

const Addresses = require('../static/addresses.json').mainnet

describe.only('User', () => {
  let pair, strategy, leverage, staking, pairVaults
  let deployer, user
  let v2factory, v2router, v2Pool
  let WETH, WBTC, USDC, DAI, wstETH, token0, token1
  let vatContract
  const ray = rawEthers.BigNumber.from(10).pow(27)
  const setup = new ContractSetup({
    addresses: Addresses,
    network
  })

  before(async () => {
    [deployer, user] = await ethers.getSigners()
    const tokens = await setup.initTokens([
      { symbol: 'wstETH', decimals: 18 },
      { symbol: 'WETH', decimals: 18 },
      { symbol: 'WBTC', decimals: 8 },
      { symbol: 'USDC', decimals: 6 },
      { symbol: 'DAI', decimals: 18 }
    ])
    await setup.initDeployerBalance(deployer, tokens, [
      { symbol: 'wstETH', amount: 0 },
      { symbol: 'WETH', amount: 25 },
      { symbol: 'WBTC', amount: 4 },
      { symbol: 'USDC', amount: 10_000 },
      { symbol: 'DAI', amount: 360_000 }
    ])
    await setup.initUserBalance(deployer, user, tokens, {
      WETH: 5,
      WBTC: 0.1,
      DAI: 10_000,
      USDC: 10_000
    })

    WETH = tokens.WETH
    WBTC = tokens.WBTC
    USDC = tokens.USDC
    DAI = tokens.DAI
    wstETH = tokens.wstETH
    ;[v2factory, v2router] = await setup.initUniswap()

    const pairSymList = [
      ['WETH', 'USDC'],
      ['WBTC', 'DAI'],
      ['USDC', 'DAI']
    ]

    vatContract = setup.initVatContract(deployer)
    strategy = await setup.initDefaultStrategy(v2router)
    pairVaults = await setup.initPairVaults(pairSymList, tokens, strategy)
    leverage = await setup.initLeverage(pairVaults)

    await setup.initLeverageDeposit(
      pairVaults,
      strategy,
      leverage,
      vatContract,
      tokens
    )

    staking = await setup.initStaking(pairVaults)

    // WETH-USDC
    pair = pairVaults[setup.sortedKey(WETH, USDC)]
    ;[token0, token1] =
      USDC.address < WETH.address ? [USDC, WETH] : [WETH, USDC]

    // WBTC-DAI
    // pair = pairVaults[setup.sortedKey(WBTC, DAI)]
    // ;[token0, token1] = WBTC.address < DAI.address ? [WBTC, DAI] : [DAI, WBTC]

    // USDC-DAI
    // pair = pairVaults[setup.sortedKey(USDC, DAI)]
    // ;[token0, token1] = USDC.address < DAI.address ? [USDC, DAI] : [DAI, USDC]

    v2Pool = await ethers.getContractAt(IERC20.abi, await pair.pool())

    const daiBalance = await DAI.balanceOf(deployer.address)
    await DAI.connect(deployer).transfer(pair.address, daiBalance)

    console.log('[spec:before] deployer', deployer.address)
    console.log('[spec:before] user', user.address)
    console.log('[spec:before] v2factory', v2factory.address)
    console.log('[spec:before] v2router', v2router.address)
    console.log('[spec:before] pair', pair.address)
    console.log('[spec:before] strategy', strategy.address)
    console.log('[spec:before] leverage', leverage.address)
    console.log('[spec:before] staking', staking.address)
    await printBalances({ WETH, WBTC, USDC, DAI, wstETH })
  })

  after(async () => {
    await printBalances({ WETH, WBTC, USDC, DAI, wstETH })
  })

  describe('1. prerequirements', async () => {
    it('1.1 should have PairVault set up', async () => {
      expect(pair.address).to.exist
      expect(await pair.token0()).to.equal(token0.address)
      expect(await pair.token1()).to.equal(token1.address)
      expect(await pair.pool()).to.exist
    })

    it('1.2 should set UniswapV2Strategy as the default PairVault strategy', async () => {
      expect(await pair.strategy()).to.equal(strategy.address)
      expect(strategy.address).to.exist
      expect(await strategy.router()).to.equal(v2router.address)
      expect(await strategy.factory()).to.equal(v2factory.address)
    })

    it('1.3 should have LeverageStrategy set up', async () => {
      expect(leverage.address).to.exist
      const vaultInBytes32 = await pair.data(leverage.address, 'vault')
      expect(+vaultInBytes32).to.greaterThan(0)
    })

    it('1.4 user should have some WETH & USDC balances', async () => {
      const balanceWETH = await WETH.balanceOf(user.address)
      const balanceUSDC = await USDC.balanceOf(user.address)
      expect(+balanceWETH).to.greaterThan(0)
      expect(+balanceUSDC).to.greaterThan(0)
    })

    it('1.5 should have StakingStrategy set up', async () => {
      expect(staking.address).to.exist
    })
  })

  describe('2. PairVault#DefaultStrategy', async () => {
    afterEach(async () => {
      console.log('\n--- balances ---')
      const pairPosition = await pair.positions(user.address, pair.address)
      await printPosition(pairPosition, 'user pair')
      console.log()
    })

    it('2.1 user can deposit into pair', async () => {
      const vaultPosBefore = await pair.position()
      const reserves = await pair.getReserves()
      const userBalanceBefore0 = await token0.balanceOf(user.address)
      const userBalanceBefore1 = await token1.balanceOf(user.address)
      const amountIn0 = userBalanceBefore0.div(2)
      const amountIn1 = amountIn0.mul(reserves.reserve1).div(reserves.reserve0)
      await token0.connect(user).approve(pair.address, amountIn0)
      await token1.connect(user).approve(pair.address, amountIn1)
      await pair.connect(user).deposit(amountIn0, amountIn1)

      const balanceLP = await v2Pool.balanceOf(pair.address)
      const position = await pair.positions(user.address, pair.address)
      expect(+position.amount).to.greaterThan(0)
      expect(+position.amount).to.equal(+balanceLP)
      expect(+position.debt).to.equal(0)
      expect(+position.blocktime).to.greaterThan(0)

      const info = await pair.decodeInfo(position.info)
      expect(+info.amount0).to.greaterThan(0)
      expect(+info.amount1).to.equal(0)

      const userBalanceAfter0 = await token0.balanceOf(user.address)
      const userBalanceAfter1 = await token1.balanceOf(user.address)
      expect(+userBalanceAfter0).to.equal(+userBalanceBefore0.sub(amountIn0))
      expect(+userBalanceAfter1).to.equal(+userBalanceBefore1.sub(amountIn1))

      const stats = await pair.check()
      console.log({ stats })
      expect(+stats[1]).to.equal(+position.amount)

      const vaultPosAfter = await pair.position()
      expect(+vaultPosAfter.amount).to.greaterThan(+vaultPosBefore.amount)
      expect(vaultPosAfter.blocktime).to.greaterThan(+vaultPosBefore.blocktime)
    })

    it('2.2 user position can be accumulated', async () => {
      const vaultPosBefore = await pair.position()
      const positionBefore = await pair.positions(user.address, pair.address)
      const infoBefore = await pair.decodeInfo(positionBefore.info)

      const reserves = await pair.getReserves()
      const userBalanceBefore0 = await token0.balanceOf(user.address)
      const amountIn0 = userBalanceBefore0.div(10)
      const amountIn1 = amountIn0.mul(reserves.reserve1).div(reserves.reserve0)
      await token0.connect(user).approve(pair.address, amountIn0)
      await token1.connect(user).approve(pair.address, amountIn1)
      await pair.connect(user).deposit(amountIn0, amountIn1)

      const positionAfter = await pair.positions(user.address, pair.address)
      expect(+positionAfter.amount).to.greaterThan(+positionBefore.amount)
      expect(positionAfter.blocktime).to.greaterThan(positionBefore.blocktime)

      const infoAfter = await pair.decodeInfo(positionAfter.info)
      expect(+infoAfter.amount0).to.greaterThan(+infoBefore.amount0)

      const stats = await pair.check()
      expect(+stats[1]).to.equal(+positionAfter.amount)

      const vaultPosAfter = await pair.position()
      expect(+vaultPosAfter.amount).to.greaterThan(+vaultPosBefore.amount)
      expect(vaultPosAfter.blocktime).to.greaterThan(+vaultPosBefore.blocktime)
    })

    it('2.3 user can withdraw all positions', async () => {
      const vaultPosBefore = await pair.position()
      const positionBefore = await pair.positions(user.address, pair.address)
      const infoBefore = await pair.decodeInfo(positionBefore.info)
      const { reserve0, reserve1, totalSupply } = await pair.getReserves()
      const amount0 = positionBefore.amount.mul(reserve0).div(totalSupply)
      const amount1 = positionBefore.amount.mul(reserve1).div(totalSupply)
      await pair.connect(user).withdraw(amount0, amount1)

      const positionAfter = await pair.positions(user.address, pair.address)
      expect(+positionAfter.amount).to.lessThan(1000)
      expect(positionAfter.blocktime).to.greaterThan(positionBefore.blocktime)

      const infoAfter = await pair.decodeInfo(positionAfter.info)
      expect(+infoAfter.amount0).to.equal(+infoBefore.amount0)
      expect(+infoAfter.amount1).to.greaterThan(+infoBefore.amount1)
      expect(+infoAfter.amount1).to.greaterThanOrEqual(+infoAfter.amount0)

      const stats = await pair.check()
      expect(+stats[1]).to.lessThan(+1000)

      const vaultPosAfter = await pair.position()
      expect(+vaultPosAfter.amount).to.lessThan(+vaultPosBefore.amount)
      expect(vaultPosAfter.blocktime).to.greaterThan(+vaultPosBefore.blocktime)
    })
  })

  describe('3. PairVault#Leverage', async () => {
    before(async () => {
      const reserves = await pair.getReserves()
      const userBalanceBefore0 = await token0.balanceOf(user.address)
      const amountIn0 = userBalanceBefore0.div(2)
      const amountIn1 = amountIn0.mul(reserves.reserve1).div(reserves.reserve0)
      await token0.connect(user).approve(pair.address, amountIn0)
      await token1.connect(user).approve(pair.address, amountIn1)
      await pair.connect(user).deposit(amountIn0, amountIn1)

      const pairPosition = await pair.positions(user.address, pair.address)
      console.log('[3.before] pair position:', pairPosition)

      const vaultInBytes32 = await pair.data(leverage.address, 'vault')
      const stats = await leverage.check(+vaultInBytes32)
      console.log('[3.before] leverage stats:', stats)
    })

    afterEach(async () => {
      console.log('\n--- balances ---')
      const pairPosition = await pair.positions(user.address, pair.address)
      await printPosition(pairPosition, 'user pair')

      const leveragePosition = await pair.positions(
        user.address,
        leverage.address
      )
      const col = await leverage.collaterals(await pair.pool())
      const [, , spot, ,] = await vatContract.ilks(col.ilk)
      // collateral * spot / ray - debt = available
      const available = leveragePosition.amount
        .mul(spot)
        .div(ray)
        .sub(leveragePosition.debt)
      await printPosition(leveragePosition, 'user leverage', { available })

      console.log()
    })

    after(async () => {
      const iface = new rawEthers.utils.Interface([
        'function fold(address _pool, uint256 _vault, address _owner)'
      ])
      const method = 'fold(address,uint256,address)'
      const vaultInBytes32 = await pair.data(leverage.address, 'vault')
      const params = [v2Pool.address, +vaultInBytes32, user.address]
      const encodedData = iface.encodeFunctionData(method, params)
      await pair.connect(user).invoke(leverage.address, encodedData)

      const stack = await pair.connect(user).getStack()
      expect(stack.length).to.equal(0)

      const pairPosition = await pair.positions(user.address, pair.address)
      const leveragePosition = await pair.positions(
        user.address,
        leverage.address
      )
      await printPosition(pairPosition, 'user pair')
      const col = await leverage.collaterals(await pair.pool())
      const [, , spot, ,] = await vatContract.ilks(col.ilk)
      const available = leveragePosition.amount
        .mul(spot)
        .div(ray)
        .sub(leveragePosition.debt)
      await printPosition(leveragePosition, 'user leverage', { available })
      expect(+leveragePosition.amount).to.equal(0)
      expect(+leveragePosition.debt).to.equal(0)

      console.log()
    })

    it('3.1 user can allocate position into Maker Vault', async () => {
      const pairLpBalanceBefore = await v2Pool.balanceOf(pair.address)
      const pairBefore = await pair.positions(user.address, pair.address)
      const iface = new rawEthers.utils.Interface([
        'function allocate(address _pool, uint256 _vault, uint256 _amount) returns (bytes32[])'
      ])
      const method = 'allocate(address,uint256,uint256)'
      const vaultInBytes32 = await pair.data(leverage.address, 'vault')
      const amountIn = pairBefore.amount
      const params = [v2Pool.address, +vaultInBytes32, amountIn]
      const encodedData = iface.encodeFunctionData(method, params)
      const request = pair.connect(user).invoke(leverage.address, encodedData)
      await expect(request)
        .to.emit(leverage, 'Allocate')
        .withArgs(v2Pool.address, +vaultInBytes32, amountIn)

      const pairLpBalanceAfter = await v2Pool.balanceOf(pair.address)
      expect(pairLpBalanceBefore.sub(pairLpBalanceAfter)).to.equal(amountIn)

      const leveragePosition = await pair.positions(
        user.address,
        leverage.address
      )

      const col = await leverage.collaterals(await pair.pool())
      const [, , spot, ,] = await vatContract.ilks(col.ilk)
      // collateral * spot / ray - debt = available
      const available = leveragePosition.amount
        .mul(spot)
        .div(ray)
        .sub(leveragePosition.debt)

      expect(+leveragePosition.amount).to.equal(+amountIn)
      expect(+available).to.greaterThan(0)
      expect(+leveragePosition.debt).to.equal(0)
      expect(leveragePosition.blocktime).to.greaterThan(pairBefore.blocktime)

      const pairAfter = await pair.positions(user.address, pair.address)
      expect(+pairAfter.available).to.lessThan(+pairBefore.available)
      expect(+pairAfter.available).to.equal(+pairBefore.available.sub(amountIn))
    })

    it('3.2 user should set stack after allocate', async () => {
      const stack = await pair.connect(user).getStack()
      expect(stack.length).to.equal(1)
      expect(stack[0]).to.equal(await leverage.selector())
    })

    it('3.3 user can borrow DAI', async () => {
      const daiBalanceBefore = await DAI.balanceOf(user.address)
      const pairBefore = await pair.positions(user.address, pair.address)
      const leverageBefore = await pair.positions(
        user.address,
        leverage.address
      )
      const infoBefore = await pair.decodeInfo(leverageBefore.info)

      // collateral * spot / ray - debt = available
      const col = await leverage.collaterals(await pair.pool())
      const [, , spot, ,] = await vatContract.ilks(col.ilk)
      const availableBefore = leverageBefore.amount
        .mul(spot)
        .div(ray)
        .sub(leverageBefore.debt)

      const iface = new rawEthers.utils.Interface([
        'function draw(address _pool, uint256 _vault, uint256 _amount)'
      ])
      const method = 'draw(address,uint256,uint256)'
      const vaultInBytes32 = await pair.data(leverage.address, 'vault')
      const borrowAmount = availableBefore
      const params = [v2Pool.address, +vaultInBytes32, borrowAmount]
      const encodedData = iface.encodeFunctionData(method, params)
      await pair.connect(user).invoke(leverage.address, encodedData)

      const leverageAfter = await pair.positions(user.address, leverage.address)
      expect(+leverageAfter.amount).to.equal(+leverageBefore.amount)
      expect(+leverageAfter.debt).to.greaterThan(+leverageBefore.debt)
      expect(leverageAfter.blocktime).to.greaterThan(leverageBefore.blocktime)

      // collateral * spot / ray - debt = available
      const availableAfter = leverageAfter.amount
        .mul(spot)
        .div(ray)
        .sub(leverageAfter.debt)
      expect(+availableAfter).to.lessThan(+availableBefore)

      const daiBalanceAfter = await DAI.balanceOf(user.address)
      expect(+daiBalanceAfter).to.greaterThan(+daiBalanceBefore)

      const pairAfter = await pair.positions(user.address, pair.address)
      expect(+pairAfter.amount).to.equal(+pairBefore.amount)

      const infoAfter = await pair.decodeInfo(leverageAfter.info)
      expect(+infoAfter.amount0).to.greaterThan(+infoBefore.amount0)
    })

    it('3.4 user can payback DAI', async () => {
      const leverageBefore = await pair.positions(
        user.address,
        leverage.address
      )
      const infoBefore = await pair.decodeInfo(leverageBefore.info)
      // collateral * spot / ray - debt = available
      const col = await leverage.collaterals(await pair.pool())
      const [, , spot, ,] = await vatContract.ilks(col.ilk)
      const availableBefore = leverageBefore.amount
        .mul(spot)
        .div(ray)
        .sub(leverageBefore.debt)

      const iface = new rawEthers.utils.Interface([
        'function payback(address _pool, uint256 _vault, uint256 _amount)'
      ])
      const method = 'payback(address,uint256,uint256)'
      const vaultInBytes32 = await pair.data(leverage.address, 'vault')
      // const paybackAmount = pairBefore.available
      const paybackAmount = leverageBefore.debt
      const params = [v2Pool.address, +vaultInBytes32, paybackAmount]
      const encodedData = iface.encodeFunctionData(method, params)

      await DAI.connect(user).approve(pair.address, paybackAmount)
      await pair.connect(user).invoke(leverage.address, encodedData)

      const leverageAfter = await pair.positions(user.address, leverage.address)
      // collateral * spot / ray - debt = available
      const availableAfter = leverageAfter.amount
        .mul(spot)
        .div(ray)
        .sub(leverageAfter.debt)

      expect(+leverageAfter.amount).to.equal(+leverageBefore.amount)
      expect(+leverageAfter.debt).to.lessThan(+leverageBefore.debt)
      expect(leverageAfter.blocktime).to.greaterThan(leverageBefore.blocktime)
      expect(+availableAfter).to.greaterThan(+availableBefore)

      const infoAfter = await pair.decodeInfo(leverageAfter.info)
      expect(+infoAfter.amount1).to.greaterThan(+infoBefore.amount1)
    })

    it('3.5 user can free collateral', async () => {
      const daiBalanceBefore = await v2Pool.balanceOf(pair.address)
      const leverageBefore = await pair.positions(
        user.address,
        leverage.address
      )
      const col = await leverage.collaterals(await pair.pool())
      const [, , spot, ,] = await vatContract.ilks(col.ilk)
      const availableBefore = leverageBefore.amount
        .mul(spot)
        .div(ray)
        .sub(leverageBefore.debt)

      const iface = new rawEthers.utils.Interface([
        'function free(address _pool, uint256 _vault, uint256 _amount) returns (uint256, uint256, uint256)',
        'function allocate(address _pool, uint256 _vault, uint256 _amount) returns (bytes32[])'
      ])
      const vaultInBytes32 = await pair.data(leverage.address, 'vault')
      const freeAmount = leverageBefore.amount.div(2)
      let method = 'free(address,uint256,uint256)'
      let params = [v2Pool.address, +vaultInBytes32, freeAmount]
      let encodedData = iface.encodeFunctionData(method, params)
      await pair.connect(user).invoke(leverage.address, encodedData)

      const leverageAfter = await pair.positions(user.address, leverage.address)
      const availableAfter = leverageAfter.amount
        .mul(spot)
        .div(ray)
        .sub(leverageAfter.debt)

      expect(+leverageAfter.amount).to.equal(
        +leverageBefore.amount.sub(freeAmount)
      )
      expect(+availableAfter).to.lessThan(+availableBefore)
      expect(+leverageAfter.debt).to.equal(+leverageBefore.debt)
      expect(leverageAfter.blocktime).to.greaterThan(leverageBefore.blocktime)

      const daiBalanceAfter = await v2Pool.balanceOf(pair.address)
      expect(+daiBalanceAfter.sub(daiBalanceBefore)).to.equal(+freeAmount)

      // NOTE: allocate freed amount back
      method = 'allocate(address,uint256,uint256)'
      params = [v2Pool.address, +vaultInBytes32, freeAmount]
      encodedData = iface.encodeFunctionData(method, params)
      await pair.connect(user).invoke(leverage.address, encodedData)
    })

    it('3.6 user can raise current allocation to 2x value', async () => {
      const leverageBefore = await pair.positions(
        user.address,
        leverage.address
      )
      const infoBefore = await pair.decodeInfo(leverageBefore.info)
      const col = await leverage.collaterals(await pair.pool())
      const [, , spot, ,] = await vatContract.ilks(col.ilk)
      const availableBefore = leverageBefore.amount
        .mul(spot)
        .div(ray)
        .sub(leverageBefore.debt)

      const iface = new rawEthers.utils.Interface([
        'function raise(address _pool, uint256 _vault, uint256 _target, address _owner)'
      ])
      const vaultInBytes32 = await pair.data(leverage.address, 'vault')
      const method = 'raise(address,uint256,uint256,address)'
      const price = await leverage.getPrice(+vaultInBytes32)
      const multiplier = 2
      const leverage2xColValue = leverageBefore.amount
        .mul(multiplier)
        .mul(price)
        .div(ray)
      const params = [
        v2Pool.address,
        +vaultInBytes32,
        leverage2xColValue,
        user.address
      ]
      const encodedData = iface.encodeFunctionData(method, params)
      const statsBefore = await leverage.check(+vaultInBytes32)
      await pair.connect(user).invoke(leverage.address, encodedData)

      const statsAfter = await leverage.check(+vaultInBytes32)
      expect(+statsAfter[0]).to.greaterThan(+statsBefore[0])

      const leverageAfter = await pair.positions(user.address, leverage.address)
      const availableAfter = leverageAfter.amount
        .mul(spot)
        .div(ray)
        .sub(leverageAfter.debt)
      expect(+leverageAfter.amount).to.greaterThan(+leverageBefore.amount)
      expect(+leverageAfter.debt).to.greaterThan(+leverageBefore.debt)
      expect(leverageAfter.blocktime).to.greaterThan(leverageBefore.blocktime)
      expect(+availableAfter).to.lessThan(+availableBefore)

      const infoAfter = await pair.decodeInfo(leverageAfter.info)
      expect(+infoAfter.amount0).to.greaterThan(+infoBefore.amount0)
    })

    it('3.7 user can reduce current allocation', async () => {
      const leverageBefore = await pair.positions(
        user.address,
        leverage.address
      )
      const infoBefore = await pair.decodeInfo(leverageBefore.info)
      const col = await leverage.collaterals(await pair.pool())
      const [, , spot, ,] = await vatContract.ilks(col.ilk)
      const availableBefore = leverageBefore.amount
        .mul(spot)
        .div(ray)
        .sub(leverageBefore.debt)
      const method = 'raise(address,uint256,uint256,address)'
      const vaultInBytes32 = await pair.data(leverage.address, 'vault')
      const price = await leverage.getPrice(+vaultInBytes32)
      const statsBefore = await leverage.check(+vaultInBytes32)
      const leverage1xColAmount = leverageBefore.amount.div(2)
      const leverage1xColValue = leverage1xColAmount.mul(price).div(ray)
      const params = [
        v2Pool.address,
        +vaultInBytes32,
        leverage1xColValue,
        user.address
      ]
      const iface = new rawEthers.utils.Interface([
        'function raise(address _pool, uint256 _vault, uint256 _target, address _owner)'
      ])
      const encodedData = iface.encodeFunctionData(method, params)
      await pair.connect(user).invoke(leverage.address, encodedData)

      const statsAfter = await leverage.check(+vaultInBytes32)
      expect(+statsAfter[0]).to.equal(
        +statsBefore[0]
          .sub(leverageBefore.amount)
          .add(leverage1xColValue.mul(ray).div(price))
      )

      const leverageAfter = await pair.positions(user.address, leverage.address)
      const availableAfter = leverageAfter.amount
        .mul(spot)
        .div(ray)
        .sub(leverageAfter.debt)
      expect(+leverageAfter.amount).to.equal(
        +leverage1xColValue.mul(ray).div(price)
      )
      expect(+leverageAfter.debt).to.lessThan(+leverageBefore.debt)
      expect(leverageAfter.blocktime).to.greaterThan(leverageBefore.blocktime)
      expect(+availableAfter).to.greaterThan(+availableBefore)

      const infoAfter = await pair.decodeInfo(leverageAfter.info)
      expect(+infoAfter.amount1).to.greaterThan(+infoBefore.amount1)
    })
  })

  describe('4. PairVault#Leverage extended', async () => {
    before(async () => {
      const pairPosition = await pair.positions(user.address, pair.address)
      console.log('[4.before] pair position:', pairPosition)

      if (+pairPosition.amount > 1000) {
        return
      }
      const reserves = await pair.getReserves()
      const userBalanceBefore0 = await token0.balanceOf(user.address)
      const amountIn0 = userBalanceBefore0.div(2)
      const amountIn1 = amountIn0.mul(reserves.reserve1).div(reserves.reserve0)
      await token0.connect(user).approve(pair.address, amountIn0)
      await token1.connect(user).approve(pair.address, amountIn1)
      await pair.connect(user).deposit(amountIn0, amountIn1)
    })

    afterEach(async () => {
      console.log('\n--- balances ---')
      const pairPosition = await pair.positions(user.address, pair.address)
      await printPosition(pairPosition, 'user pair')

      const leveragePosition = await pair.positions(
        user.address,
        leverage.address
      )
      const col = await leverage.collaterals(await pair.pool())
      const [, , spot, ,] = await vatContract.ilks(col.ilk)
      // collateral * spot / ray - debt = available
      const available = leveragePosition.amount
        .mul(spot)
        .div(ray)
        .sub(leveragePosition.debt)
      await printPosition(leveragePosition, 'user leverage', { available })

      console.log()
    })

    it('4.1 user can allocate and raise a position to 2x value', async () => {
      const leverageBefore = await pair.positions(
        user.address,
        leverage.address
      )
      const infoBefore = await pair.decodeInfo(leverageBefore.info)
      const pairPosition = await pair.positions(user.address, pair.address)
      const iface = new rawEthers.utils.Interface([
        'function raise(address _pool, uint256 _vault, uint256 _target, address _owner)'
      ])

      const vaultInBytes32 = await pair.data(leverage.address, 'vault')
      const statsBefore = await leverage.check(+vaultInBytes32)
      const price = await leverage.getPrice(+vaultInBytes32)
      const allocateAmount = pairPosition.amount
      const multiplier = 2
      const leverage2xColValue = allocateAmount
        .mul(multiplier)
        .mul(price)
        .div(ray)
      const method = 'raise(address,uint256,uint256,address)'
      const params = [
        v2Pool.address,
        +vaultInBytes32,
        leverage2xColValue,
        user.address
      ]
      const encodedData = iface.encodeFunctionData(method, params)
      await pair.connect(user).invoke(leverage.address, encodedData)

      const statsAfter = await leverage.check(+vaultInBytes32)
      expect(+statsAfter[0]).to.greaterThanOrEqual(
        +statsBefore[0].add(leverageBefore.amount)
      )

      const col = await leverage.collaterals(await pair.pool())
      const [, , spot, ,] = await vatContract.ilks(col.ilk)
      const leverageAfter = await pair.positions(user.address, leverage.address)
      const availableAfter = leverageAfter.amount
        .mul(spot)
        .div(ray)
        .sub(leverageAfter.debt)
      expect(+leverageAfter.amount).to.greaterThanOrEqual(
        +leverageBefore.amount
      )
      expect(+leverageAfter.amount).to.greaterThan(+leverageBefore.amount)
      expect(+leverageAfter.debt).to.greaterThan(+leverageBefore.debt)
      expect(leverageAfter.blocktime).to.greaterThan(leverageBefore.blocktime)
      expect(+availableAfter).to.greaterThan(0)

      const infoAfter = await pair.decodeInfo(leverageAfter.info)
      expect(+infoAfter.amount0).to.greaterThan(+infoBefore.amount0)
    })

    it('4.2 user can reduce current allocation to 1x value', async () => {
      const leverageBefore = await pair.positions(
        user.address,
        leverage.address
      )
      const infoBefore = await pair.decodeInfo(leverageBefore.info)
      const col = await leverage.collaterals(await pair.pool())
      const [, , spot, ,] = await vatContract.ilks(col.ilk)
      const availableBefore = leverageBefore.amount
        .mul(spot)
        .div(ray)
        .sub(leverageBefore.debt)
      const iface = new rawEthers.utils.Interface([
        'function raise(address _pool, uint256 _vault, uint256 _target, address _owner)'
      ])
      const vaultInBytes32 = await pair.data(leverage.address, 'vault')
      const statsBefore = await leverage.check(+vaultInBytes32)
      const price = await leverage.getPrice(+vaultInBytes32)
      const targetAmount = leverageBefore.amount.div(2)
      const targetValue = targetAmount.mul(price).div(ray)
      const method = 'raise(address,uint256,uint256,address)'
      const params = [
        v2Pool.address,
        +vaultInBytes32,
        targetValue,
        user.address
      ]
      const encodedData = iface.encodeFunctionData(method, params)
      await pair.connect(user).invoke(leverage.address, encodedData)

      const statsAfter = await leverage.check(+vaultInBytes32)
      expect(+statsAfter[0]).to.lessThanOrEqual(
        +statsBefore[0].sub(targetAmount)
      )

      const leverageAfter = await pair.positions(user.address, leverage.address)
      expect(+leverageAfter.amount).to.lessThan(+leverageBefore.amount)
      expect(+leverageAfter.debt).to.lessThan(+leverageBefore.debt)
      expect(leverageAfter.blocktime).to.greaterThan(leverageBefore.blocktime)
      const availableAfter = leverageAfter.amount
        .mul(spot)
        .div(ray)
        .sub(leverageAfter.debt)
      expect(+availableAfter).to.greaterThan(+availableBefore)

      const infoAfter = await pair.decodeInfo(leverageAfter.info)
      expect(+infoAfter.amount1).to.greaterThan(+infoBefore.amount1)
    })

    it('4.3 user can fold the position completely', async () => {
      const stackBefore = await pair.connect(user).getStack()
      expect(stackBefore.length).to.equal(1)

      const leverageBefore = await pair.positions(
        user.address,
        leverage.address
      )
      const iface = new rawEthers.utils.Interface([
        'function fold(address _pool, uint256 _vault, address _owner)'
      ])
      const method = 'fold(address,uint256,address)'
      const vaultInBytes32 = await pair.data(leverage.address, 'vault')
      const params = [v2Pool.address, +vaultInBytes32, user.address]
      const encodedData = iface.encodeFunctionData(method, params)
      await pair.connect(user).invoke(leverage.address, encodedData)

      const leverageAfter = await pair.positions(user.address, leverage.address)
      expect(+leverageAfter.amount).to.equal(0)
      expect(+leverageAfter.debt).to.equal(0)
      expect(leverageAfter.blocktime).to.greaterThan(leverageBefore.blocktime)

      const infoAfter = await pair.decodeInfo(leverageAfter.info)
      expect(+infoAfter.amount0).to.equal(0)
      expect(+infoAfter.amount1).to.equal(0)

      const stackAfter = await pair.connect(user).getStack()
      expect(stackAfter.length).to.equal(0)
    })
  })

  describe('5. PairVault#Staking', async () => {
    before(async () => {
      let pairPosition = await pair.positions(user.address, pair.address)
      if (+pairPosition.amount < 1000) {
        const reserves = await pair.getReserves()
        const userBalanceBefore0 = await token0.balanceOf(user.address)
        const amountIn0 = userBalanceBefore0.div(2)
        const amountIn1 = amountIn0
          .mul(reserves.reserve1)
          .div(reserves.reserve0)
        await token0.connect(user).approve(pair.address, amountIn0)
        await token1.connect(user).approve(pair.address, amountIn1)
        await pair.connect(user).deposit(amountIn0, amountIn1)
        pairPosition = await pair.positions(user.address, pair.address)
      }

      const vaultInBytes32 = await pair.data(leverage.address, 'vault')
      const stats = await leverage.check(+vaultInBytes32)

      const iface = new rawEthers.utils.Interface([
        'function allocate(address _pool, uint256 _vault, uint256 _amount) returns (bytes32[])'
      ])
      const method = 'allocate(address,uint256,uint256)'
      const amountIn = pairPosition.amount
      const params = [v2Pool.address, +vaultInBytes32, amountIn]
      const encodedData = iface.encodeFunctionData(method, params)
      await pair.connect(user).invoke(leverage.address, encodedData)
      const leveragePosition = await pair.positions(
        user.address,
        leverage.address
      )

      console.log('[5.before] leverage stats:', stats)
      console.log('\n--- balances ---')
      const col = await leverage.collaterals(await pair.pool())
      const [, , spot, ,] = await vatContract.ilks(col.ilk)
      // collateral * spot / ray - debt = available
      const available = leveragePosition.amount
        .mul(spot)
        .div(ray)
        .sub(leveragePosition.debt)
      await printPosition(pairPosition, 'user pair')
      await printPosition(leveragePosition, 'user leverage', { available })
      console.log()
    })

    afterEach(async () => {
      console.log('\n--- balances ---')
      const pairPosition = await pair.positions(user.address, pair.address)
      await printPosition(pairPosition, 'user pair')

      const leveragePosition = await pair.positions(
        user.address,
        leverage.address
      )
      const col = await leverage.collaterals(await pair.pool())
      const [, , spot, ,] = await vatContract.ilks(col.ilk)
      // collateral * spot / ray - debt = available
      const available = leveragePosition.amount
        .mul(spot)
        .div(ray)
        .sub(leveragePosition.debt)
      await printPosition(leveragePosition, 'user leverage', { available })

      const stakingPosition = await pair.positions(
        user.address,
        staking.address
      )
      await printPosition(stakingPosition, 'user staking')

      console.log()
    })

    after(async () => {
      const iface = new rawEthers.utils.Interface([
        'function fold(address, uint256, address)'
      ])
      const method = 'fold(address,uint256,address)'
      const params = [leverage.address, 0, user.address]
      const encodedData = iface.encodeFunctionData(method, params)
      await pair.connect(user).invoke(staking.address, encodedData)

      const stack = await pair.connect(user).getStack()
      expect(stack.length).to.equal(1)
      expect(stack[0]).to.equal(await leverage.selector())
    })

    it('5.1 user can allocate DAI from leverage to stake ETH2.0', async () => {
      const leverageBefore = await pair.positions(
        user.address,
        leverage.address
      )
      const stakingBefore = await pair.positions(user.address, staking.address)
      const infoBefore = await pair.decodeInfo(stakingBefore.info)
      const wstEthBalanceBefore = await wstETH.balanceOf(pair.address)
      const iface = new rawEthers.utils.Interface([
        'function allocate(address _leverage, uint256 _amount, uint256 _null)'
      ])
      const method = 'allocate(address,uint256,uint256)'
      const amountIn = parseUnits('5000', 18)
      const params = [leverage.address, amountIn, 0]
      const encodedData = iface.encodeFunctionData(method, params)
      await pair.connect(user).invoke(staking.address, encodedData)

      const stack = await pair.connect(user).getStack()
      expect(stack.length).to.equal(2)
      expect(stack[1]).to.equal(await staking.selector())

      const wstEthBalanceAfter = await wstETH.balanceOf(pair.address)
      expect(+wstEthBalanceAfter).to.greaterThan(+wstEthBalanceBefore)

      const leverageAfter = await pair.positions(user.address, leverage.address)
      const stakingAfter = await pair.positions(user.address, staking.address)
      expect(+leverageAfter.debt).to.equal(+leverageBefore.debt.add(amountIn))
      expect(+stakingAfter.amount).to.greaterThan(+stakingBefore.amount)
      expect(+stakingAfter.debt).to.equal(+amountIn)
      expect(stakingAfter.blocktime).to.greaterThan(stakingBefore.blocktime)

      const infoAfter = await pair.decodeInfo(stakingAfter.info)
      expect(+infoAfter.amount0).to.greaterThan(+infoBefore.amount0)
      expect(+infoAfter.amount0).to.greaterThan(0)
      expect(+infoAfter.amount1).to.equal(0)
    })

    it('5.2 user can free wstETH for DAI and payback Leverage debt', async () => {
      const leverageBefore = await pair.positions(
        user.address,
        leverage.address
      )
      const stakingBefore = await pair.positions(user.address, staking.address)
      const infoBefore = await pair.decodeInfo(stakingBefore.info)
      const wstEthBalanceBefore = await wstETH.balanceOf(pair.address)
      const iface = new rawEthers.utils.Interface([
        'function free(address _leverage, uint256 _amount, uint256 _null)'
      ])
      const method = 'free(address,uint256,uint256)'
      const params = [leverage.address, wstEthBalanceBefore, 0]
      const encodedData = iface.encodeFunctionData(method, params)
      await pair.connect(user).invoke(staking.address, encodedData)

      const leverageAfter = await pair.positions(user.address, leverage.address)
      const stakingAfter = await pair.positions(user.address, staking.address)
      expect(+leverageAfter.debt).to.lessThan(+leverageBefore.debt)
      expect(+stakingAfter.amount).to.lessThan(+stakingBefore.amount)
      expect(+stakingAfter.debt).to.lessThan(+stakingBefore.debt)
      expect(stakingAfter.blocktime).to.greaterThan(stakingBefore.blocktime)

      const infoAfter = await pair.decodeInfo(stakingAfter.info)
      expect(+infoAfter.amount1).to.greaterThan(+infoBefore.amount1)
    })

    it('5.3 user can fold current staking strategy', async () => {
      const iface = new rawEthers.utils.Interface([
        'function allocate(address _leverage, uint256 _amount, uint256 _null)',
        'function fold(address, uint256, address)'
      ])
      let method = 'allocate(address,uint256,uint256)'
      const amountIn = parseUnits('5000', 18)
      let params = [leverage.address, amountIn, 0]
      let encodedData = iface.encodeFunctionData(method, params)
      await pair.connect(user).invoke(staking.address, encodedData)

      method = 'fold(address,uint256,address)'
      params = [leverage.address, 0, user.address]
      encodedData = iface.encodeFunctionData(method, params)
      await pair.connect(user).invoke(staking.address, encodedData)

      const stack = await pair.connect(user).getStack()
      expect(stack.length).to.equal(1)
      expect(stack[0]).to.equal(await leverage.selector())

      const stakingAfter = await pair.positions(user.address, staking.address)
      expect(+stakingAfter.amount).to.equal(0)
      expect(+stakingAfter.debt).to.equal(0)

      const infoAfter = await pair.decodeInfo(stakingAfter.info)
      expect(+infoAfter.amount0).to.equal(0)
      expect(+infoAfter.amount1).to.equal(0)
    })

    it('5.4 user can raise up a position', async () => {
      const leverage0 = await pair.positions(user.address, leverage.address)
      const staking0 = await pair.positions(user.address, staking.address)
      const info0 = await pair.decodeInfo(staking0.info)
      const col = await leverage.collaterals(await pair.pool())
      const [, , spot, ,] = await vatContract.ilks(col.ilk)
      const available0 = leverage0.amount.mul(spot).div(ray).sub(leverage0.debt)
      const iface = new rawEthers.utils.Interface([
        'function allocate(address _leverage, uint256 _amount, uint256 _null)',
        'function raise(address, uint256, uint256, address)'
      ])
      let method = 'allocate(address,uint256,uint256)'
      let amountIn = parseUnits('5000', 18)
      let params = [leverage.address, amountIn, 0]
      let encodedData = iface.encodeFunctionData(method, params)
      await pair.connect(user).invoke(staking.address, encodedData)

      const leverage1 = await pair.positions(user.address, leverage.address)
      const staking1 = await pair.positions(user.address, staking.address)
      const info1 = await pair.decodeInfo(staking1.info)
      const available1 = leverage1.amount.mul(spot).div(ray).sub(leverage1.debt)
      expect(+leverage1.debt).to.equal(+leverage0.debt.add(amountIn))
      expect(+staking1.amount).to.greaterThan(+staking0.amount)
      expect(+staking1.debt).to.equal(+amountIn)
      expect(staking1.blocktime).to.greaterThan(staking0.blocktime)
      expect(+available1).to.equal(+available0.sub(amountIn))
      expect(+info1.amount0).to.greaterThan(+info0.amount0)

      const stack = await pair.connect(user).getStack()
      expect(stack.length).to.equal(2)
      expect(stack[1]).to.equal(await staking.selector())

      amountIn = parseUnits('8000', 18)
      method = 'raise(address,uint256,uint256,address)'
      params = [leverage.address, amountIn, 0, user.address]
      encodedData = iface.encodeFunctionData(method, params)
      await pair.connect(user).invoke(staking.address, encodedData)

      const leverage2 = await pair.positions(user.address, leverage.address)
      const staking2 = await pair.positions(user.address, staking.address)
      const info2 = await pair.decodeInfo(staking2.info)
      const available2 = leverage2.amount.mul(spot).div(ray).sub(leverage2.debt)
      expect(+available2).to.equal(+available0.sub(amountIn))
      expect(+info2.amount0).to.greaterThan(+info1.amount0)
    })

    it('5.5 user can raise down a position', async () => {
      const leverage0 = await pair.positions(user.address, leverage.address)
      const staking0 = await pair.positions(user.address, staking.address)
      const info0 = await pair.decodeInfo(staking0.info)
      const col = await leverage.collaterals(await pair.pool())
      const [, , spot, ,] = await vatContract.ilks(col.ilk)
      const available0 = leverage0.amount.mul(spot).div(ray).sub(leverage0.debt)
      const iface = new rawEthers.utils.Interface([
        'function raise(address, uint256, uint256, address)'
      ])
      const method = 'raise(address,uint256,uint256,address)'
      const amountIn = parseUnits('2000', 18)
      const params = [leverage.address, amountIn, 0, user.address]
      const encodedData = iface.encodeFunctionData(method, params)
      await pair.connect(user).invoke(staking.address, encodedData)

      const leverage1 = await pair.positions(user.address, leverage.address)
      const staking1 = await pair.positions(user.address, staking.address)
      const info1 = await pair.decodeInfo(staking1.info)
      const available1 = leverage1.amount.mul(spot).div(ray).sub(leverage1.debt)
      expect(+available1).to.greaterThan(+available0)
      expect(+staking1.amount).to.lessThan(+staking0.amount)
      expect(+staking1.debt).to.lessThan(+staking0.debt)
      expect(+leverage1.amount).to.equal(+leverage0.amount)
      expect(+leverage1.debt).to.lessThan(+leverage0.debt)
      expect(+info1.amount0).to.greaterThan(+info0.amount0)
      expect(+info1.amount1).to.greaterThan(+info0.amount1)
    })
  })

  describe('6. PairVault#Deployer', async () => {
    before(async () => {
      let pairPosition = await pair.positions(user.address, pair.address)
      if (+pairPosition.amount < 1000) {
        const reserves = await pair.getReserves()
        const userBalanceBefore0 = await token0.balanceOf(user.address)
        const amountIn0 = userBalanceBefore0.div(2)
        const amountIn1 = amountIn0
          .mul(reserves.reserve1)
          .div(reserves.reserve0)
        await token0.connect(user).approve(pair.address, amountIn0)
        await token1.connect(user).approve(pair.address, amountIn1)
        await pair.connect(user).deposit(amountIn0, amountIn1)
        pairPosition = await pair.positions(user.address, pair.address)
      }

      const vaultInBytes32 = await pair.data(leverage.address, 'vault')
      const stats = await leverage.check(+vaultInBytes32)

      if (+pairPosition.available > 0) {
        const iface = new rawEthers.utils.Interface([
          'function allocate(address _pool, uint256 _vault, uint256 _amount) returns (bytes32[])'
        ])
        const method = 'allocate(address,uint256,uint256)'
        const amountIn = pairPosition.available
        const params = [v2Pool.address, +vaultInBytes32, amountIn]
        const encodedData = iface.encodeFunctionData(method, params)
        await pair.connect(user).invoke(leverage.address, encodedData)
      }

      const stakingPosition = await pair.positions(
        user.address,
        staking.address
      )
      if (+stakingPosition.amount === 0) {
        const iface = new rawEthers.utils.Interface([
          'function allocate(address _leverage, uint256 _amount, uint256 _null)'
        ])
        const method = 'allocate(address,uint256,uint256)'
        const amountIn = parseUnits('1000', 18)
        const params = [leverage.address, amountIn, 0]
        const encodedData = iface.encodeFunctionData(method, params)
        await pair.connect(user).invoke(staking.address, encodedData)
      }

      console.log('[6.before] leverage stats:', stats)
      console.log('\n--- balances ---')
      const leveragePosition = await pair.positions(
        user.address,
        leverage.address
      )
      const col = await leverage.collaterals(await pair.pool())
      const [, , spot, ,] = await vatContract.ilks(col.ilk)
      // collateral * spot / ray - debt = available
      const available = leveragePosition.amount
        .mul(spot)
        .div(ray)
        .sub(leveragePosition.debt)
      await printPosition(pairPosition, 'user pair')
      await printPosition(leveragePosition, 'user leverage', { available })
      console.log()
    })

    afterEach(async () => {
      console.log('\n--- balances ---')
      const pairPosition = await pair.positions(user.address, pair.address)
      await printPosition(pairPosition, 'user pair')

      const leveragePosition = await pair.positions(
        user.address,
        leverage.address
      )
      const col = await leverage.collaterals(await pair.pool())
      const [, , spot, ,] = await vatContract.ilks(col.ilk)
      // collateral * spot / ray - debt = available
      const available = leveragePosition.amount
        .mul(spot)
        .div(ray)
        .sub(leveragePosition.debt)
      await printPosition(leveragePosition, 'user leverage', { available })

      const stakingPosition = await pair.positions(
        user.address,
        staking.address
      )
      await printPosition(stakingPosition, 'user staking')

      console.log()
    })

    it('6.1 deployer can fold all staking & leverage positions', async () => {
      const vaultInBytes32 = await pair.data(leverage.address, 'vault')
      const wethBalance = await WETH.balanceOf(deployer.address)
      await WETH.approve(strategy.address, wethBalance)
      await strategy.swap(WETH.address, DAI.address, wethBalance)
      const wethAllowance = await DAI.allowance(
        strategy.address,
        deployer.address
      )
      await DAI.transferFrom(strategy.address, deployer.address, wethAllowance)
      await DAI.transferFrom(
        deployer.address,
        pair.address,
        await DAI.balanceOf(deployer.address)
      )

      const deployerBalanceBefore = {
        DAI: await DAI.balanceOf(deployer.address),
        wstETH: await wstETH.balanceOf(deployer.address),
        lp: await v2Pool.balanceOf(deployer.address)
      }

      await pair
        .connect(deployer)
        .foldAll(leverage.address, v2Pool.address, +vaultInBytes32)

      const deployerBalanceAfter = {
        DAI: await DAI.balanceOf(deployer.address),
        wstETH: await wstETH.balanceOf(deployer.address),
        lp: await v2Pool.balanceOf(deployer.address)
      }
      expect(+deployerBalanceAfter.DAI).to.greaterThan(
        +deployerBalanceBefore.DAI
      )
      expect(+deployerBalanceAfter.wstETH).to.greaterThan(
        +deployerBalanceBefore.wstETH
      )
      expect(+deployerBalanceAfter.lp).to.greaterThan(+deployerBalanceBefore.lp)

      const pairBalance = {
        DAI: await DAI.balanceOf(pair.address),
        wstETH: await wstETH.balanceOf(pair.address),
        lp: await v2Pool.balanceOf(pair.address)
      }
      expect(+pairBalance.DAI).to.equal(0)
      expect(+pairBalance.wstETH).to.equal(0)
      expect(+pairBalance.lp).to.equal(0)
    })

    it('6.2 deployer can copy a position', async () => {
      const leverageBefore = await pair.positions(
        user.address,
        leverage.address
      )
      const fromPosition = await pair.positions(user.address, staking.address)
      expect(+leverageBefore.amount).to.not.equal(+fromPosition.amount)
      await pair.copyPosition(user.address, staking.address, leverage.address)

      const leverageAfter = await pair.positions(user.address, leverage.address)
      expect(+leverageAfter.amount).to.equal(+fromPosition.amount)
    })
  })

  async function printBalance (balance, name, tokens) {
    for (const sym in balance) {
      if (sym === 'ETH') {
        console.log(`[${name}] ${sym}:`, formatUnits(balance[sym], 'ether'))
      } else if (sym === 'UNI-V2-USDC-WETH') {
        console.log(`[${name}] ${sym}:`, formatUnits(balance[sym], 18))
      } else {
        const decimals = await tokens[sym].decimals()
        console.log(`[${name}] ${sym}:`, formatUnits(balance[sym], decimals))
      }
    }
  }

  async function printBalances (tokens) {
    console.log('\n--- balances ---')
    const targets = { deployer, user, pair, leverage, strategy, staking }
    for (const key in targets) {
      const balance = await fetchBalance(targets[key], tokens)
      await printBalance(balance, key, tokens)
    }
  }

  async function printPosition (position, name, options = {}) {
    console.log(
      `[spec] ${name} position: amount`,
      formatUnits(position.amount, 18)
    )
    console.log(
      `[spec] ${name} position: available`,
      formatUnits(options.available || position.available, 18)
    )
    console.log(`[spec] ${name} position: debt`, formatUnits(position.debt, 18))

    const info = await pair.decodeInfo(position.info)
    let infoTypes = ['Total Deposits', 'Total Withdraws']
    if (name.match(/leverage/i)) {
      infoTypes = ['Total Borrowed Debt', 'Total Repaid Debt']
    }
    console.log(
      `[spec] ${name} position: info - ${infoTypes[0]}`,
      formatUnits(info.amount0, 6)
    )
    console.log(
      `[spec] ${name} position: info - ${infoTypes[1]}`,
      formatUnits(info.amount1, 6)
    )
  }

  async function fetchBalance (target, tokens) {
    const balance = {}
    balance.ETH = await ethers.provider.getBalance(target.address)
    for (const name in tokens) {
      balance[name] = await tokens[name].balanceOf(target.address)
    }
    const uniV2UsdcWeth = {
      address: '0xB4e16d0168e52d35CaCD2c6185b44281Ec28C9Dc',
      decimals: 18,
      name: 'UNI-V2-USDC-WETH',
      symbol: 'UNI-V2'
    }
    balance[uniV2UsdcWeth.name] = await v2Pool.balanceOf(target.address)
    return balance
  }
})
