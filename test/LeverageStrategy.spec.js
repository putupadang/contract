const { expect } = require('chai')
const { ethers } = require('hardhat')
const { ethers: rawEthers } = require('ethers')
const {
  parseEther,
  parseUnits,
  formatUnits,
  formatBytes32String,
  parseBytes32String
} = rawEthers.utils
const IERC20 = require('@openzeppelin/contracts/build/contracts/IERC20.json')
const UniswapV2Factory = require('@uniswap/v2-core/build/UniswapV2Factory.json')
const UniswapV2Router02 = require('@uniswap/v2-periphery/build/UniswapV2Router02.json')

const Addresses = {
  Uniswap: {
    v2factory: '0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f',
    v2router: '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D'
  },
  Tokens: {
    DAI: {
      address: '0x6B175474E89094C44Da98b954EedeAC495271d0F',
      decimals: 18,
      name: 'Dai Stablecoin',
      symbol: 'DAI'
    },
    USDC: {
      address: '0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48',
      decimals: 6,
      name: 'USD Coin',
      symbol: 'USDC'
    },
    WETH: {
      address: '0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2',
      decimals: 18,
      name: 'Wrapped Ether',
      symbol: 'WETH'
    },
    WBTC: {
      address: '0x2260FAC5E5542a773Aa44fBCfeDf7C193bc2C599',
      decimals: 8,
      name: 'Wrapped BTC',
      symbol: 'WBTC'
    }
  }
}

describe('Leverage:Strategy', () => {
  let pair, leverage, deployer, user, factory, router, strategy
  let poolAddr, v2Pool, vaultId, vatContract, baseStats, baseDaiBalance
  let WETH, USDC, DAI

  before(async () => {
    [deployer, user] = await ethers.getSigners()
    const tokens = await setupTokens(deployer, [
      { amount: 40, symbol: 'WETH', decimals: 18 },
      { amount: 100_000, symbol: 'USDC', decimals: 6 },
      { amount: 100_000, symbol: 'DAI', decimals: 18 }
    ])
    await setupUserBalance(deployer, user, tokens, {
      WETH: 20,
      DAI: 100,
      USDC: 50_000
    })
    WETH = tokens.WETH
    USDC = tokens.USDC
    DAI = tokens.DAI
    ;[factory, router] = await setupUniswap()

    const PairVault = await ethers.getContractFactory('PairVault')
    const UniStrategyFactory = await ethers.getContractFactory('UniswapV2')
    strategy = await UniStrategyFactory.deploy(router.address)

    pair = await PairVault.deploy(USDC.address, WETH.address, strategy.address)
    await pair.deployed()
    pair = pair.connect(user)

    const MakerStrategyFactory = await ethers.getContractFactory('Leverage')
    const pool = await pair.pool()
    const pools = [pool]
    const types = ['UNIV2USDCETH-A']
    const joins = ['0x03Ae53B33FeeAc1222C3f372f32D37Ba95f0F099']
    const pairs = [pair.address]
    leverage = await MakerStrategyFactory.deploy(pools, types, joins, pairs)
    leverage = leverage.connect(user)

    console.log('[spec:before] deployer', deployer.address)
    console.log('[spec:before] factory', factory.address)
    console.log('[spec:before] router', router.address)
    console.log('[spec:before] pair', pair.address)
    console.log('[spec:before] strategy', strategy.address)
    console.log('[spec:before] leverage', leverage.address)

    poolAddr = await pair.pool()
    v2Pool = await ethers.getContractAt(IERC20.abi, poolAddr)
    v2Pool = v2Pool.connect(user)
    vaultId = parseInt(
      await leverage.callStatic.initialize(
        poolAddr,
        rawEthers.constants.AddressZero
      )
    )
    await leverage.initialize(poolAddr, ethers.constants.AddressZero)

    // Allocate LP in UniV2
    const usdcAmount = parseUnits('50000', 6)
    const wethAmount = parseUnits('20')
    await WETH.transfer(pair.address, wethAmount)
    await USDC.transfer(pair.address, usdcAmount)
    await pair.allocate(usdcAmount, wethAmount)

    const MCD_VAT = '0x35D1b3F3D7966A1DFe207aa4514C12a259A0492B'
    vatContract = new rawEthers.Contract(
      MCD_VAT,
      [
        'function urns(bytes32, address) view returns (uint256 ink, uint256 art)',
        'function ilks(bytes32) view returns (uint256 Art, uint256 rate, uint256 spot, uint256 line, uint256 dust)'
      ],
      user
    )
    const col = await leverage.collaterals(pool)
    const [, , spot, , dust] = await vatContract.ilks(col.ilk)
    const minLpAmount = dust.add(2n * 10n ** 45n).div(spot)
    await pair.withdrawLP(deployer.address, minLpAmount)
    await v2Pool.connect(deployer).approve(leverage.address, minLpAmount)
    await leverage
      .connect(deployer)
      .allocate(deployer.address, vaultId, minLpAmount)
    baseStats = await leverage.check(vaultId)

    await WETH.connect(user).transfer(pair.address, wethAmount)
    await USDC.connect(user).transfer(pair.address, usdcAmount)
    await pair.allocate(usdcAmount, wethAmount)

    const stats = await leverage.check(vaultId)
    await leverage.draw(vaultId, stats[1])
    await printBalances(tokens)
    baseStats = await leverage.check(vaultId)
    baseDaiBalance = await DAI.balanceOf(leverage.address)
    // process.exit()
  })

  after(async () => {
    await printBalances({ WETH, USDC, DAI })
    await leverage.check(vaultId)
  })

  describe('deployment', () => {
    it('1. should intialise uniswap v2 default strategy', async () => {
      expect(await pair.strategy()).to.equal(strategy.address)
    })

    it('2. should set the strategy name', async () => {
      expect(await leverage.name()).to.equal('Leverage:MakerDAO')
    })

    it('3. should set the default LP token mapping', async () => {
      const pool = await pair.pool()
      const col = await leverage.collaterals(pool)
      expect(col.ilk).to.equal(formatBytes32String('UNIV2USDCETH-A'))
    })
  })

  describe('initialize', () => {
    it('1. should open a new vaultId', async () => {
      expect(vaultId).to.be.greaterThan(0)
    })
  })

  describe('allocate', async () => {
    before(async () => {
      const liquidity = await v2Pool.balanceOf(pair.address)
      await pair.withdrawLP(user.address, liquidity)
    })

    afterEach(async () => {
      const stats = await leverage.check(vaultId)
      if (stats[0].gt(baseStats[0])) {
        await leverage.free(user.address, vaultId, stats[0].sub(baseStats[0]))
        await v2Pool.transferFrom(
          leverage.address,
          user.address,
          stats[0].sub(baseStats[0])
        )
      }
    })

    it('1. should have some LP tokens in pair vault', async () => {
      const balanceBefore = await v2Pool.balanceOf(user.address)
      const amountIn = rawEthers.utils.parseUnits('0.0002', 18)
      await v2Pool.approve(leverage.address, amountIn)
      await leverage.allocate(user.address, vaultId, amountIn)
      const balanceAfter = await v2Pool.balanceOf(user.address)

      expect(+balanceBefore).to.be.equal(+balanceAfter.add(amountIn))
    })

    it('2. reverts when allocate exceeds available liquidity (10x balance)', async () => {
      const balanceBefore = await v2Pool.balanceOf(user.address)
      await v2Pool.approve(leverage.address, balanceBefore.mul(10))
      const leverageAllocate = leverage.allocate(
        user.address,
        vaultId,
        balanceBefore.mul(10)
      )
      await expect(leverageAllocate).to.be.reverted
    })
  })

  describe('draw', async () => {
    beforeEach(async () => {
      const liquidity = await v2Pool.balanceOf(pair.address)
      await pair.withdrawLP(user.address, liquidity)
    })

    afterEach(async () => {
      let stats = await leverage.check(vaultId)
      if (stats[2].gt(baseStats[2])) {
        const daiBalance = await DAI.balanceOf(leverage.address)
        const interest = stats[2]
          .sub(baseStats[2])
          .sub(daiBalance.sub(baseDaiBalance))
        await DAI.connect(user).transfer(leverage.address, interest)
        console.log('[spec] user paid interest:', +formatUnits(interest))
        await leverage.payback(vaultId, stats[2].sub(baseStats[2]))
      }
      stats = await leverage.check(vaultId)
      if (stats[0].gt(baseStats[0])) {
        await leverage.free(user.address, vaultId, stats[0].sub(baseStats[0]))
        await v2Pool.transferFrom(
          leverage.address,
          user.address,
          stats[0].sub(baseStats[0])
        )
      }
    })

    it('1. should borrow out DAI', async () => {
      const lpBalance = await v2Pool.balanceOf(user.address)
      const amountIn = rawEthers.utils.parseUnits('0.0005', 18)
      expect(+lpBalance).to.be.greaterThan(+amountIn)

      await v2Pool.approve(leverage.address, amountIn)
      await leverage.allocate(user.address, vaultId, amountIn)

      const maxAmountOut = await leverage.getMaxAmountOut(vaultId, amountIn)
      const drawAmount = maxAmountOut.div(2)
      const daiBalanceBefore = await DAI.balanceOf(leverage.address)
      await leverage.draw(vaultId, drawAmount)

      const daiBalanceAfter = await DAI.balanceOf(leverage.address)
      console.log({ daiBalanceBefore, daiBalanceAfter })
      expect(+drawAmount).to.be.equal(+daiBalanceAfter.sub(daiBalanceBefore))
    })
  })

  describe('check', async () => {
    let amountIn

    before(async () => {
      const liquidity = await v2Pool.balanceOf(pair.address)
      await pair.withdrawLP(user.address, liquidity)

      amountIn = rawEthers.utils.parseUnits('0.0005', 18)
      await v2Pool.approve(leverage.address, amountIn)
      await leverage.allocate(user.address, vaultId, amountIn)
    })

    afterEach(async () => {
      let stats = await leverage.check(vaultId)
      if (stats[2].gt(baseStats[2])) {
        const daiBalance = await DAI.balanceOf(leverage.address)
        const interest = stats[2]
          .sub(baseStats[2])
          .sub(daiBalance.sub(baseDaiBalance))
        await DAI.connect(user).transfer(leverage.address, interest)
        console.log('[spec] user paid interest:', +formatUnits(interest))
        await leverage.payback(vaultId, stats[2].sub(baseStats[2]))
      }
      stats = await leverage.check(vaultId)
      if (stats[0].gt(baseStats[0])) {
        await leverage.free(user.address, vaultId, stats[0].sub(baseStats[0]))
        await v2Pool.transferFrom(
          leverage.address,
          user.address,
          stats[0].sub(baseStats[0])
        )
      }
    })

    it('1. has collateral, available', async () => {
      const [collateral, available, ,] = await leverage.check(vaultId)
      expect(+collateral.sub(baseStats[0])).to.be.equal(+amountIn)
      expect(+available).to.greaterThan(0)
    })

    it('2. has debt', async () => {
      let [, , debt] = await leverage.check(vaultId)

      amountIn = rawEthers.utils.parseUnits('0.0005', 18)
      await v2Pool.approve(leverage.address, amountIn)
      await leverage.allocate(user.address, vaultId, amountIn)
      await leverage.draw(vaultId, parseUnits('60001', 18))
      const vdebt = await leverage.getVaultDebt(vaultId)
      ;[, , debt] = await leverage.check(vaultId)

      expect(+debt).to.equal(+vdebt)
    })
  })

  describe('payback', () => {
    let amountIn

    before(async () => {
      const liquidity = await v2Pool.balanceOf(pair.address)
      await pair.withdrawLP(user.address, liquidity)
    })

    beforeEach(async () => {
      amountIn = rawEthers.utils.parseUnits('0.0005', 18)
      await v2Pool.approve(leverage.address, amountIn)
      await leverage.allocate(user.address, vaultId, amountIn)

      const maxAmountOut = await leverage.getMaxAmountOut(vaultId, amountIn)
      await leverage.draw(vaultId, maxAmountOut.div(2))
    })

    afterEach(async () => {
      let stats = await leverage.check(vaultId)
      if (stats[2].gt(baseStats[2])) {
        const daiBalance = await DAI.balanceOf(leverage.address)
        const interest = stats[2]
          .sub(baseStats[2])
          .sub(daiBalance.sub(baseDaiBalance))
        await DAI.connect(user).transfer(leverage.address, interest)
        console.log('[spec] user paid interest:', +formatUnits(interest))
        await leverage.payback(vaultId, stats[2].sub(baseStats[2]))
      }
      stats = await leverage.check(vaultId)
      if (stats[0].gt(baseStats[0])) {
        await leverage.free(user.address, vaultId, stats[0].sub(baseStats[0]))
        await v2Pool.transferFrom(
          leverage.address,
          user.address,
          stats[0].sub(baseStats[0])
        )
      }
    })

    it('1. should have some initial debt', async () => {
      const [, , debt] = await leverage.check(vaultId)
      expect(+debt).to.be.greaterThan(+baseStats[2])
    })

    it('2. can payback the debt in multiple transactions', async () => {
      let stats = await leverage.check(vaultId)
      const debtBefore = stats[2].sub(baseStats[2])
      const amountAny = debtBefore.div(10)
      const daiBalance = await DAI.balanceOf(leverage.address)
      const interest = stats[2]
        .sub(baseStats[2])
        .sub(daiBalance.sub(baseDaiBalance))
      await DAI.connect(user).transfer(leverage.address, interest)
      console.log('[spec:payback] user paid interest:', +formatUnits(interest))
      await leverage.payback(vaultId, amountAny)
      stats = await leverage.check(vaultId)
      const debtLeft = stats[2].sub(baseStats[2])
      await leverage.payback(vaultId, debtLeft)
      stats = await leverage.check(vaultId)
      expect(+stats[2].sub(baseStats[2])).to.be.equal(0)
    })

    it('3. can payback the debt in one transaction', async () => {
      let stats = await leverage.check(vaultId)
      const debtBefore = stats[2].sub(baseStats[2])
      const daiBalance = await DAI.balanceOf(leverage.address)
      const interest = stats[2]
        .sub(baseStats[2])
        .sub(daiBalance.sub(baseDaiBalance))
      await DAI.connect(user).transfer(leverage.address, interest)
      console.log('[spec:payback] user paid interest:', +formatUnits(interest))
      await leverage.payback(vaultId, debtBefore)
      stats = await leverage.check(vaultId)
      expect(+stats[2].sub(baseStats[2])).to.be.equal(0)
    })
  })

  describe('free', () => {
    let amountIn

    before(async () => {
      const liquidity = await v2Pool.balanceOf(pair.address)
      await pair.withdrawLP(user.address, liquidity)
    })

    beforeEach(async () => {
      amountIn = rawEthers.utils.parseUnits('0.0005', 18)
      await v2Pool.approve(leverage.address, amountIn)
      await leverage.allocate(user.address, vaultId, amountIn)

      const maxAmountOut = await leverage.getMaxAmountOut(vaultId, amountIn)
      await leverage.draw(vaultId, maxAmountOut.div(2))

      const stats = await leverage.check(vaultId)
      const daiBalance = await DAI.balanceOf(leverage.address)
      const interest = stats[2]
        .sub(baseStats[2])
        .sub(daiBalance.sub(baseDaiBalance))
      await DAI.connect(user).transfer(leverage.address, interest)
      console.log('[spec:free] user paid interest:', +formatUnits(interest))
      await leverage.payback(vaultId, stats[2].sub(baseStats[2]))
    })

    afterEach(async () => {
      let stats = await leverage.check(vaultId)
      if (stats[2].gt(baseStats[2])) {
        await leverage.payback(vaultId, stats[2].sub(baseStats[2]))
      }
      stats = await leverage.check(vaultId)
      if (stats[0].gt(baseStats[0])) {
        await leverage.free(user.address, vaultId, stats[0].sub(baseStats[0]))
        await v2Pool.transferFrom(
          leverage.address,
          user.address,
          stats[0].sub(baseStats[0])
        )
      }
      const lpBalance = await v2Pool.balanceOf(leverage.address)
      if (+lpBalance > 0) {
        await v2Pool.transferFrom(leverage.address, user.address, lpBalance)
      }
    })

    it('1. should have some collaterals in GemJoin contract', async () => {
      const col = await leverage.collaterals(poolAddr)
      const vatUrns = await vatContract.urns(col.ilk, col.urn)
      expect(+vatUrns.ink).to.be.equal(amountIn.add(baseStats[0]))
    })

    it('2. should get LP token back after free', async () => {
      let lpBalance = await v2Pool.balanceOf(leverage.address)
      expect(+lpBalance).to.be.equal(0)

      await leverage.free(user.address, vaultId, amountIn)
      const col = await leverage.collaterals(poolAddr)
      const vatUrns = await vatContract.urns(col.ilk, col.urn)
      expect(+vatUrns.ink).to.be.equal(+baseStats[0])

      lpBalance = await v2Pool.balanceOf(leverage.address)
      expect(+lpBalance).to.be.equal(+amountIn)
    })
  })

  describe('raise', async () => {
    let amountIn

    before(async () => {
      const liquidity = await v2Pool.balanceOf(pair.address)
      await pair.withdrawLP(user.address, liquidity)

      amountIn = rawEthers.utils.parseUnits('0.0005', 18)
      await v2Pool.approve(leverage.address, amountIn)
      await leverage.allocate(user.address, vaultId, amountIn)
    })

    after(async () => {
      let stats = await leverage.check(vaultId)
      if (stats[2].gt(baseStats[2])) {
        const daiBalance = await DAI.balanceOf(leverage.address)
        const interest = stats[2]
          .sub(baseStats[2])
          .sub(daiBalance.sub(baseDaiBalance))
        await DAI.connect(user).transfer(leverage.address, interest)
        console.log('[spec] user paid interest:', +formatUnits(interest))
        await leverage.payback(vaultId, stats[2].sub(baseStats[2]))
      }
      stats = await leverage.check(vaultId)
      if (stats[0].gt(baseStats[0])) {
        await leverage.free(user.address, vaultId, stats[0].sub(baseStats[0]))
        await v2Pool.transferFrom(
          leverage.address,
          user.address,
          stats[0].sub(baseStats[0])
        )
      }
      const lpBalance = await v2Pool.balanceOf(leverage.address)
      if (+lpBalance > 0) {
        await v2Pool.transferFrom(leverage.address, user.address, lpBalance)
      }
    })

    it('1. raise LP exposure towards target (2x spot value)', async () => {
      const pool = await pair.pool()
      const col = await leverage.collaterals(pool)
      const [, , spot, ,] = await vatContract.ilks(col.ilk)
      const multiplier = 2
      const leverage2xSpotValue = amountIn
        .mul(multiplier)
        .add(baseStats[0])
        .mul(spot)

      await leverage.raise(vaultId, leverage2xSpotValue)
      const [collateral, available, debt, ratio] = await leverage.check(vaultId)
      expect(+collateral).to.be.equal(+leverage2xSpotValue.div(spot))

      console.log('[spec:raise] baseStats[0]', +formatUnits(baseStats[0]))
      console.log(
        '[spec:raise] leverage2xSpotValue',
        +formatUnits(leverage2xSpotValue, 45)
      )
      console.log('[spec:raise] check:collateral', +formatUnits(collateral))
      console.log(
        '[spec:raise] check:available',
        +formatUnits(available),
        'DAI'
      )
      console.log('[spec:raise] check:debt', +formatUnits(debt), 'DAI')
      console.log('[spec:raise] check:c-ratio', +ratio / 10, '%')
    })

    it('2. should be able to decrease the raise amount (1x spot value)', async () => {
      const pool = await pair.pool()
      const col = await leverage.collaterals(pool)
      const [, , spot, ,] = await vatContract.ilks(col.ilk)
      const leverage1xSpotValue = amountIn.add(baseStats[0]).mul(spot)
      await leverage.raise(vaultId, leverage1xSpotValue)
      const [collateral, , ,] = await leverage.check(vaultId)
      expect(+collateral).to.be.equal(+leverage1xSpotValue.div(spot))

      console.log(
        '[spec:raise-down] leverage1xSpotValue',
        +formatUnits(leverage1xSpotValue, 45)
      )
    })

    xit('3. should be able to free the collateral', async () => {
      const pool = await pair.pool()
      const col = await leverage.collaterals(pool)
      const [, , spot, ,] = await vatContract.ilks(col.ilk)
      let stats = await leverage.check(vaultId)

      const leverageSpotValueAfterDebt = stats[0]
        .mul(spot)
        .sub(stats[2].sub(baseStats[2]).mul(10n ** 27n))
      await leverage.raise(vaultId, leverageSpotValueAfterDebt)
      const daiBalance = await DAI.balanceOf(leverage.address)
      if (daiBalance.gt(baseStats[2])) {
        await leverage.allocateDai(vaultId, daiBalance.sub(baseStats[2]))
      }
      stats = await leverage.check(vaultId)
      if (stats[2].gt(baseStats[2])) {
        const daiBalance = await DAI.balanceOf(leverage.address)
        const interest = stats[2]
          .sub(baseStats[2])
          .sub(daiBalance.sub(baseDaiBalance))
        await DAI.connect(user).transfer(leverage.address, interest)
        console.log('[spec:raise] user paid interest:', +formatUnits(interest))
        await leverage.payback(vaultId, stats[2].sub(baseStats[2]))
      }

      const debt = stats[2].sub(baseStats[2])
      expect(+debt).to.be.lessThan(parseInt(1n * 10n ** 18n))

      const amountLeft = stats[0].sub(baseStats[0])
      await leverage.free(user.address, vaultId, amountLeft)
      stats = await leverage.check(vaultId)
      const leverageLp = await v2Pool.balanceOf(leverage.address)
      expect(+leverageLp).to.be.equal(+amountLeft)
    })
  })

  async function setupTokens (deployer, targetList) {
    const tokenContracts = {}
    const Uniswap = Addresses.Uniswap
    const v2router = await ethers.getContractAt(
      UniswapV2Router02.abi,
      Uniswap.v2router
    )

    for (const target of targetList) {
      const symbol = target.symbol
      const token = Addresses.Tokens[target.symbol]
      tokenContracts[symbol] = await ethers.getContractAt(
        IERC20.abi,
        token.address
      )
      if (!tokenContracts[symbol].decimals) {
        tokenContracts[symbol].decimals = async () => {
          return target.decimals
        }
      }
      if (symbol === 'WETH') {
        const amount = parseUnits(target.amount.toString(), target.decimals)
        const tx = await deployer.sendTransaction({
          to: token.address,
          value: amount
        })
        await tx.wait()
      } else {
        const amountOut = parseUnits(target.amount.toString(), target.decimals)
        const path = [tokenContracts.WETH.address, token.address]
        const to = deployer.address
        const deadline = Math.trunc(Date.now() / 1000) + 5 * 60
        await uniSwapTokens(v2router, amountOut, path, to, deadline)
      }
    }
    return tokenContracts
  }

  async function setupUniswap () {
    factory = await ethers.getContractAt(
      UniswapV2Factory.abi,
      Addresses.Uniswap.v2factory
    )
    router = await ethers.getContractAt(
      UniswapV2Router02.abi,
      Addresses.Uniswap.v2router
    )

    return [factory, router]
  }

  async function uniSwapTokens (v2router, amountOut, path, to, deadline) {
    const amountIn = await v2router.getAmountsIn(amountOut, path)
    await v2router.swapETHForExactTokens(amountOut, path, to, deadline, {
      value: amountIn[0]
    })
  }

  async function setupUserBalance (sender, receiver, tokens, amount) {
    if (sender.address === receiver.address) {
      return
    }
    for (const sym in amount) {
      if (sym === 'ETH') {
        const tx = await sender.sendTransaction({
          to: receiver.address,
          value: parseEther(amount[sym].toString())
        })
        await tx.wait()
      } else {
        await tokens[sym].transfer(
          receiver.address,
          parseUnits(amount[sym].toString(), await tokens[sym].decimals())
        )
      }
    }
  }

  async function printBalances (tokens) {
    console.log('\n--- balances ---')
    const deployerBalance = await fetchBalance(deployer, tokens)
    const userBalance = await fetchBalance(user, tokens)
    const leverageBalance = await fetchBalance(leverage, tokens)
    const strategyBalance = await fetchBalance(strategy, tokens)
    const uniV2UsdcWeth = {
      address: '0xB4e16d0168e52d35CaCD2c6185b44281Ec28C9Dc',
      decimals: 18,
      name: 'UNI-V2-USDC-WETH',
      symbol: 'UNI-V2-USDC-WETH'
    }

    for (const sym in deployerBalance) {
      if (sym === 'ETH') {
        console.log(
          `[deployer balance] ${sym}:`,
          formatUnits(deployerBalance[sym], 'ether')
        )
      } else {
        console.log(
          `[deployer balance] ${sym}:`,
          formatUnits(deployerBalance[sym], await tokens[sym].decimals())
        )
      }
    }
    console.log(
      `[deployer balance] ${uniV2UsdcWeth.symbol}:`,
      formatUnits(
        await v2Pool.balanceOf(deployer.address),
        uniV2UsdcWeth.decimals
      )
    )

    for (const sym in userBalance) {
      if (sym === 'ETH') {
        console.log(
          `[user balance] ${sym}:`,
          formatUnits(userBalance[sym], 'ether')
        )
      } else {
        console.log(
          `[user balance] ${sym}:`,
          formatUnits(userBalance[sym], await tokens[sym].decimals())
        )
      }
    }
    console.log(
      `[user balance] ${uniV2UsdcWeth.symbol}:`,
      formatUnits(await v2Pool.balanceOf(user.address), uniV2UsdcWeth.decimals)
    )

    for (const sym in leverageBalance) {
      if (sym === 'ETH') {
        console.log(
          `[leverage balance] ${sym}:`,
          formatUnits(leverageBalance[sym], 'ether')
        )
      } else {
        console.log(
          `[leverage balance] ${sym}:`,
          formatUnits(leverageBalance[sym], await tokens[sym].decimals())
        )
      }
    }
    console.log(
      `[leverage balance] ${uniV2UsdcWeth.symbol}:`,
      formatUnits(
        await v2Pool.balanceOf(leverage.address),
        uniV2UsdcWeth.decimals
      )
    )

    for (const sym in strategyBalance) {
      if (sym === 'ETH') {
        console.log(
          `[strategy balance] ${sym}:`,
          formatUnits(strategyBalance[sym], 'ether')
        )
      } else {
        console.log(
          `[strategy balance] ${sym}:`,
          formatUnits(strategyBalance[sym], await tokens[sym].decimals())
        )
      }
    }
    console.log(
      `[strategy balance] ${uniV2UsdcWeth.symbol}:`,
      formatUnits(
        await v2Pool.balanceOf(strategy.address),
        uniV2UsdcWeth.decimals
      ),
      '\n'
    )
  }

  async function fetchBalance (target, tokens) {
    const balance = {}
    balance.ETH = await ethers.provider.getBalance(target.address)
    for (const name in tokens) {
      balance[name] = await tokens[name].balanceOf(target.address)
    }
    return balance
  }

  async function getVaultData (leverageAddr, deployer) {
    const CDP_MANAGER = '0x5ef30b9986345249bc32d8928B7ee64DE9435E39'
    const cdpManager = new rawEthers.Contract(
      CDP_MANAGER,
      [
        'function cdpAllow(uint256 cdp, address usr, uint256 ok)',
        'function cdpCan(address, uint256, address) view returns (uint256)',
        'function cdpi() view returns (uint256)',
        'function count(address) view returns (uint256)',
        'function enter(address src, uint256 cdp)',
        'function first(address) view returns (uint256)',
        'function flux(bytes32 ilk, uint256 cdp, address dst, uint256 wad)',
        'function flux(uint256 cdp, address dst, uint256 wad)',
        'function frob(uint256 cdp, int256 dink, int256 dart)',
        'function give(uint256 cdp, address dst)',
        'function ilks(uint256) view returns (bytes32)',
        'function last(address) view returns (uint256)',
        'function list(uint256) view returns (uint256 prev, uint256 next)',
        'function move(uint256 cdp, address dst, uint256 rad)',
        'function open(bytes32 ilk, address usr) returns (uint256)',
        'function owns(uint256) view returns (address)',
        'function quit(uint256 cdp, address dst)',
        'function shift(uint256 cdpSrc, uint256 cdpDst)',
        'function urnAllow(address usr, uint256 ok)',
        'function urnCan(address, address) view returns (uint256)',
        'function urns(uint256) view returns (address)',
        'function vat() view returns (address)'
      ],
      deployer
    )

    const vaultId = await cdpManager.last(leverageAddr)
    const ilk = await cdpManager.ilks(vaultId)
    const urn = await cdpManager.urns(vaultId)
    const count = await cdpManager.count(leverageAddr)
    const last = await cdpManager.last(leverageAddr)
    const vat = await cdpManager.vat()

    return { vaultId, ilk, urn, count, last, vat }
  }

  async function getVatData (ilk, urn, signer, amtDeposit) {
    const MCD_VAT = '0x35D1b3F3D7966A1DFe207aa4514C12a259A0492B'
    const vatContract = new rawEthers.Contract(
      MCD_VAT,
      [
        'function Line() view returns (uint256)',
        'function cage()',
        'function can(address, address) view returns (uint256)',
        'function dai(address) view returns (uint256)',
        'function debt() view returns (uint256)',
        'function deny(address usr)',
        'function file(bytes32 ilk, bytes32 what, uint256 data)',
        'function file(bytes32 what, uint256 data)',
        'function flux(bytes32 ilk, address src, address dst, uint256 wad)',
        'function fold(bytes32 i, address u, int256 rate)',
        'function fork(bytes32 ilk, address src, address dst, int256 dink, int256 dart)',
        'function frob(bytes32 i, address u, address v, address w, int256 dink, int256 dart)',
        'function gem(bytes32, address) view returns (uint256)',
        'function grab(bytes32 i, address u, address v, address w, int256 dink, int256 dart)',
        'function heal(uint256 rad)',
        'function hope(address usr)',
        'function ilks(bytes32) view returns (uint256 Art, uint256 rate, uint256 spot, uint256 line, uint256 dust)',
        'function init(bytes32 ilk)',
        'function live() view returns (uint256)',
        'function move(address src, address dst, uint256 rad)',
        'function nope(address usr)',
        'function rely(address usr)',
        'function sin(address) view returns (uint256)',
        'function slip(bytes32 ilk, address usr, int256 wad)',
        'function suck(address u, address v, uint256 rad)',
        'function urns(bytes32, address) view returns (uint256 ink, uint256 art)',
        'function vice() view returns (uint256)',
        'function wards(address) view returns (uint256)'
      ],
      signer
    )
    const gem = await vatContract.gem(ilk, urn)
    const dai = await vatContract.dai(urn)

    const vatUrns = await vatContract.urns(ilk, urn)
    const vatIlks = await vatContract.ilks(ilk)
    const vatUrn = {
      ink: BigInt(vatUrns.ink),
      art: BigInt(vatUrns.art)
    }
    const vatIlk = {
      Art: BigInt(vatIlks.Art),
      rate: BigInt(vatIlks.rate),
      spot: BigInt(vatIlks.spot),
      line: BigInt(vatIlks.line),
      dust: BigInt(vatIlks.dust)
    }

    const amtBorrowMax = rawEthers.utils.parseUnits(
      ((vatIlk.spot * BigInt(amtDeposit)) / BigInt(10 ** (27 + 18))).toString(),
      18
    )

    const dink = BigInt(amtDeposit)
    const dart = BigInt(amtBorrowMax)

    vatUrn.ink = BigInt(vatUrn.ink) + dink
    vatUrn.art = BigInt(vatUrn.art) + dart
    vatIlk.Art = BigInt(vatIlk.Art) + dart

    let debt = await vatContract.debt()
    const dtab = BigInt(vatIlk.rate) * dart
    const tab = BigInt(vatIlk.rate) * vatUrn.art
    debt = BigInt(debt) + dtab

    return {
      ilk,
      urn,
      vatUrns,
      gem,
      dai,
      vatUrn,
      vatIlk,
      dtab,
      tab,
      debt,
      dink,
      dart,
      amtBorrowMax
    }
  }
})
