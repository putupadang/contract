const fs = require('fs')
const path = require('path')
const IERC20 = require('@openzeppelin/contracts/build/contracts/IERC20.json')
const IERC20Metadata = require('@openzeppelin/contracts/build/contracts/IERC20Metadata.json')
const UniswapV2Router02 = require('@uniswap/v2-periphery/build/UniswapV2Router02.json')
const UniswapV2Factory = require('@uniswap/v2-core/build/UniswapV2Factory.json')
const { ethers: rawEthers } = require('ethers')
const { BigNumber } = rawEthers
const { parseEther, parseUnits, formatUnits, formatBytes32String, hexZeroPad } =
  rawEthers.utils

class ContractSetup {
  constructor (options = {}) {
    this.addresses = options.addresses
    this.network = options.network
  }

  async initTokens (targetList) {
    const tokenContracts = {}
    for (const target of targetList) {
      const symbol = target.symbol
      const token = this.addresses.Tokens[target.symbol]
      tokenContracts[symbol] = await ethers.getContractAt(
        IERC20Metadata.abi,
        token.address
      )
      if (!tokenContracts[symbol].decimals) {
        tokenContracts[symbol].decimals = async () => {
          return target.decimals
        }
      }
    }
    return tokenContracts
  }

  async initDeployerBalance (deployer, tokens, targetList) {
    const Uniswap = this.addresses.Uniswap
    const v2router = await ethers.getContractAt(
      UniswapV2Router02.abi,
      Uniswap.v2router
    )
    for (const target of targetList) {
      const symbol = target.symbol
      const token = tokens[target.symbol]
      if (symbol === 'WETH' && target.amount > 0) {
        const amount = parseUnits(
          target.amount.toString(),
          await token.decimals()
        )
        const tx = await deployer.sendTransaction({
          to: token.address,
          value: amount
        })
        await tx.wait()
      } else if (target.amount > 0) {
        const amountOut = parseUnits(
          target.amount.toString(),
          await token.decimals()
        )
        const path = [tokens.WETH.address, token.address]
        const to = deployer.address
        const deadline = Math.trunc(Date.now() / 1000) + 5 * 60
        await this.uniSwapTokens(v2router, amountOut, path, to, deadline)
      }
    }
  }

  async initUserBalance (sender, receiver, tokens, amount) {
    if (sender.address === receiver.address) {
      return
    }
    for (const sym in amount) {
      if (sym === 'ETH') {
        const tx = await sender.sendTransaction({
          to: receiver.address,
          value: parseEther(amount[sym].toString())
        })
        await tx.wait()
      } else {
        await tokens[sym].transfer(
          receiver.address,
          parseUnits(amount[sym].toString(), await tokens[sym].decimals())
        )
      }
    }
  }

  async uniSwapTokens (v2router, amountOut, path, to, deadline) {
    const amountIn = await v2router.getAmountsIn(amountOut, path)
    await v2router.swapETHForExactTokens(amountOut, path, to, deadline, {
      value: amountIn[0]
    })
  }

  async initDefaultStrategy (v2router) {
    const UniStrategyFactory = await ethers.getContractFactory('UniswapV2')
    let strategy

    const record = this.checkRecord('strategy')
    if (record && record.address) {
      strategy = await ethers.getContractAt(
        UniStrategyFactory.interface,
        record.address
      )
    } else {
      console.log('[deploying] default strategy')
      strategy = await UniStrategyFactory.deploy(v2router.address)
      await strategy.deployed()
      this.updateRecord('strategy', strategy.address, { key: 'address' })
    }

    if (!record || !record.setTokens) {
      await strategy.setTokens(
        [
          formatBytes32String('DAI'),
          formatBytes32String('WETH'),
          formatBytes32String('stETH'),
          formatBytes32String('USDC')
        ],
        [
          this.addresses.Tokens.DAI.address,
          this.addresses.Tokens.WETH.address,
          this.addresses.Tokens.stETH.address,
          this.addresses.Tokens.USDC.address
        ]
      )
      this.updateRecord('strategy', true, { key: 'setTokens' })
    }

    if (!record || !record.setContracts) {
      await strategy.setContracts(
        [
          formatBytes32String('MCD_PSM_USDC_A'),
          formatBytes32String('MCD_JOIN_PSM_USDC_A')
        ],
        [
          this.addresses.Maker.MCD_PSM_USDC_A,
          this.addresses.Maker.MCD_JOIN_PSM_USDC_A
        ]
      )
      this.updateRecord('strategy', true, { key: 'setContracts' })
    }

    return strategy
  }

  async initUniswap () {
    const v2factory = await ethers.getContractAt(
      UniswapV2Factory.abi,
      this.addresses.Uniswap.v2factory
    )
    const v2router = await ethers.getContractAt(
      UniswapV2Router02.abi,
      this.addresses.Uniswap.v2router
    )

    return [v2factory, v2router]
  }

  async initPairVaults (pairSymList, tokens, strategy) {
    const vaults = {}
    for (const [index, pair] of pairSymList.entries()) {
      const tokenA = tokens[pair[0]]
      const tokenB = tokens[pair[1]]

      const record = this.checkRecord('pairVaults', index)
      let pairContract
      if (record && record.address) {
        const PairVault = await ethers.getContractFactory('PairVault')
        pairContract = await ethers.getContractAt(
          PairVault.interface,
          record.address
        )
      } else {
        pairContract = await this.initPairContract(tokenA, tokenB, strategy)
        this.updateRecord('pairVaults', pairContract.address, {
          index,
          key: 'address'
        })
      }

      if (!record || !record.setStrategyMethods) {
        await this.initPairStrategyMethods(pairContract)
        this.updateRecord('pairVaults', true, {
          index,
          key: 'setStrategyMethods'
        })
      }

      if (!record || !record.setTokens) {
        await pairContract.setTokens(
          [formatBytes32String('DAI'), formatBytes32String('wstETH')],
          [
            this.addresses.Tokens.DAI.address,
            this.addresses.Tokens.wstETH.address
          ]
        )
        this.updateRecord('pairVaults', true, {
          index,
          key: 'setTokens'
        })
      }

      vaults[this.sortedKey(tokenA, tokenB)] = pairContract
      console.log('pairVaults:', pair.join('-'), pairContract.address)
    }
    return vaults
  }

  async initPairContract (tokenA, tokenB, strategy) {
    const PairVault = await ethers.getContractFactory('PairVault')
    const [token0, token1] =
      tokenA.address < tokenB.address ? [tokenA, tokenB] : [tokenB, tokenA]
    console.log('[deploying] pair contract', tokenA.address, tokenB.address)
    const pair = await PairVault.deploy(
      token0.address,
      token1.address,
      tokenB.address,
      strategy.address
    )
    await pair.deployed()

    return pair
  }

  async initPairStrategyMethods (pair) {
    await Promise.all(
      [
        'allocate(address,uint256,uint256)',
        'draw(address,uint256,uint256)',
        'payback(address,uint256,uint256)',
        'free(address,uint256,uint256)',
        'raise(address,uint256,uint256,address)',
        'fold(address,uint256,address)'
      ].map(async (method, i) => {
        return { methodSig: await pair.getMethodSig(method), value: i + 1 }
      })
    ).then(async (result) => {
      await pair.setEStrategies(
        result.map((r) => r.methodSig),
        result.map((r) => r.value)
      )
    })
  }

  async registerLeverage (pairVaults, address) {
    const LeverageFactory = await ethers.getContractFactory('Leverage')
    const addresses = {}
    ;['UNIV2USDCETH-A', 'UNIV2WBTCDAI-A', 'UNIV2DAIUSDC-A'].forEach((type) => {
      addresses[this.addresses.Maker[type].address] = {
        type,
        join: this.addresses.Maker[type].join
      }
    })
    const data = { pools: [], types: [], joins: [], pairs: [] }
    for (const key in pairVaults) {
      const pair = pairVaults[key]
      const pool = await pair.pool()
      data.pools.push(pool)
      data.types.push(addresses[pool].type)
      data.joins.push(addresses[pool].join)
      data.pairs.push(pair.address)
    }

    const leverage = await ethers.getContractAt(
      LeverageFactory.interface,
      address
    )

    console.log('leverage:', leverage.address)

    for (const key in pairVaults) {
      const pair = pairVaults[key]
      console.log('[waiting] register')
      await pair.register(0, leverage.address)
    }

    return leverage
  }

  async initLeverage (pairVaults) {
    let leverage
    const LeverageFactory = await ethers.getContractFactory('Leverage')
    const addresses = {}
    ;['UNIV2USDCETH-A', 'UNIV2WBTCDAI-A', 'UNIV2DAIUSDC-A'].forEach((type) => {
      addresses[this.addresses.Maker[type].address] = {
        type,
        join: this.addresses.Maker[type].join
      }
    })
    const data = { pools: [], types: [], joins: [], pairs: [] }
    for (const key in pairVaults) {
      const pair = pairVaults[key]
      const pool = await pair.pool()
      data.pools.push(pool)
      data.types.push(addresses[pool].type)
      data.joins.push(addresses[pool].join)
      data.pairs.push(pair.address)
    }

    const record = this.checkRecord('leverage')
    if (record && record.address) {
      leverage = await ethers.getContractAt(
        LeverageFactory.interface,
        record.address
      )
    } else {
      console.log('[deploying] leverage')
      leverage = await LeverageFactory.deploy(
        this.addresses.Maker.CDP_MANAGER,
        data.pools,
        data.types,
        data.joins,
        data.pairs
      )
      await leverage.deployed()
      this.updateRecord('leverage', leverage.address, { key: 'address' })
    }
    console.log('leverage', leverage.address)

    if (!record || !record.setTokens) {
      await leverage.setTokens(
        [formatBytes32String('DAI')],
        [this.addresses.Tokens.DAI.address]
      )
      this.updateRecord('leverage', true, {
        key: 'setTokens'
      })
    }

    if (!record || !record.setContracts) {
      await leverage.setContracts(
        [
          formatBytes32String('MCD_JOIN_DAI'),
          formatBytes32String('MCD_JUG'),
          formatBytes32String('MCD_SPOT'),
          formatBytes32String('MCD_FLASH_LEGACY')
        ],
        [
          this.addresses.Maker.MCD_JOIN_DAI,
          this.addresses.Maker.MCD_JUG,
          this.addresses.Maker.MCD_SPOT,
          this.addresses.Maker.MCD_FLASH_LEGACY
        ]
      )
      this.updateRecord('leverage', true, {
        key: 'setContracts'
      })
    }

    const DAI = this.addresses.Tokens.DAI
    for (const key in pairVaults) {
      const pair = pairVaults[key]
      const pool = await pair.pool()
      const record = this.checkRecord('leveragePairSetups', key)
      if (!record || !record.register) {
        console.log('[waiting] pair register leverage')
        await pair.register(0, leverage.address)
        this.updateRecord('leveragePairSetups', { register: true }, { key })
      }

      if (!record || !record.approveLp) {
        console.log('[waiting] approve leverage use pair LP')
        await pair.approveMax(leverage.address, pool)
        this.updateRecord('leveragePairSetups', { approveLp: true }, { key })
      }

      if (!record || !record.approveDai) {
        console.log('[waiting] approve leverage use pair DAI')
        await pair.approveMax(leverage.address, DAI.address)
        this.updateRecord('leveragePairSetups', { approveDai: true }, { key })
      }
    }

    let vaultId
    for (const [index, pool] of data.pools.entries()) {
      const record = this.checkRecord('leverageInit', index)
      if (!record || !record.done) {
        console.log('[waiting] leverage initialize')
        if (!vaultId) {
          vaultId = await leverage.callStatic.initialize(
            pool,
            rawEthers.constants.AddressZero
          )
          vaultId = +vaultId
        } else {
          vaultId += 1
        }
        console.log('vaultId:', vaultId)
        await leverage.initialize(pool, rawEthers.constants.AddressZero)
        this.updateRecord('leverageInit', true, { index, key: 'done' })
        this.updateRecord('leverageInit', vaultId, { index, key: 'vault' })
      }

      const leverageData = this.checkRecord('leverageData', index)
      if (!leverageData || !leverageData.vault) {
        console.log('[waiting] set up leverage data')
        const col = await leverage.collaterals(pool)
        const pair = Object.values(pairVaults).find(
          (item) => item.address === col.pair
        )
        await pair.setData(
          leverage.address,
          'vault',
          hexZeroPad(BigNumber.from(vaultId).toHexString(), 32)
        )

        const dustList = {
          WETH: parseUnits('0.03', 18),
          USDC: parseUnits('50', 6),
          DAI: parseUnits('50', 18),
          WBTC: parseUnits('0.001', 8)
        }
        const tokenAddr0 = await pair.token0()
        const tokenAddr1 = await pair.token1()
        for (const tokenAddr of [tokenAddr0, tokenAddr1]) {
          const token = Object.values(this.addresses.Tokens).find(
            (t) => t.address === tokenAddr
          )
          await pair.setData(
            token.address,
            'dust',
            hexZeroPad(dustList[token.symbol].toHexString(), 32)
          )
        }
        this.updateRecord('leverageData', true, { index, key: 'vault' })
      }

      if (!leverageData || !leverageData['raise-slippage']) {
        console.log('[waiting] set up leverage slippage')
        const col = await leverage.collaterals(pool)
        const pair = Object.values(pairVaults).find(
          (item) => item.address === col.pair
        )
        const slippage = this.network.name !== 'goerli' ? 0 : 0
        await pair.setData(
          leverage.address,
          'raise-slippage',
          hexZeroPad(BigNumber.from(slippage).toHexString(), 32)
        )
        this.updateRecord('leverageData', true, {
          index,
          key: 'raise-slippage'
        })
      }
    }

    return leverage
  }

  async initLeverageDeposit (
    pairVaults,
    strategy,
    leverage,
    vatContract,
    tokens
  ) {
    const [deployer] = await ethers.getSigners()
    const DAI = this.addresses.Tokens.DAI

    for (const key in pairVaults) {
      const amountIn = parseUnits('150000', 18)
      const pair = pairVaults[key]
      const pool = await pair.pool()

      const col = await leverage.collaterals(pool)
      const [, , spot, , dust] = await vatContract.ilks(col.ilk)
      const minLpAmount = dust.add(2n * 10n ** 45n).div(spot)

      await tokens.DAI.approve(strategy.address, amountIn)
      const tokenAddr0 = await pair.token0()
      const tokenAddr1 = await pair.token1()
      const token0 = Object.values(tokens).find((t) => t.address === tokenAddr0)
      const token1 = Object.values(tokens).find((t) => t.address === tokenAddr1)

      const amountIn0 = (
        await strategy.callStatic.swap(
          DAI.address,
          token0.address,
          amountIn.div(2)
        )
      ).amountOutMain
      if (token0.address !== DAI.address) {
        await strategy.swap(DAI.address, token0.address, amountIn.div(2))
      }

      const amountIn1 = (
        await strategy.callStatic.swap(
          DAI.address,
          token1.address,
          amountIn.div(2)
        )
      ).amountOutMain
      if (token1.address !== DAI.address) {
        await strategy.swap(DAI.address, token1.address, amountIn.div(2))
      }
      await token0.connect(deployer).approve(pair.address, amountIn0)
      await token1.connect(deployer).approve(pair.address, amountIn1)
      await pair.connect(deployer).deposit(amountIn0, amountIn1)

      const pairPosition = await pair.positions(deployer.address, pair.address)
      const vaultInBytes32 = await pair.data(leverage.address, 'vault')
      const iface = new rawEthers.utils.Interface([
        'function allocate(address _pool, uint256 _vault, uint256 _amount) returns (bytes32[])',
        'function draw(address _pool, uint256 _vault, uint256 _amount)'
      ])
      let method = 'allocate(address,uint256,uint256)'
      let params = [pool, +vaultInBytes32, pairPosition.amount]
      let encodedData = iface.encodeFunctionData(method, params)
      await pair.connect(deployer).invoke(leverage.address, encodedData)

      const leveragePosition = await pair.positions(
        deployer.address,
        leverage.address
      )
      const ray = rawEthers.BigNumber.from(10).pow(27)
      const precision = rawEthers.BigNumber.from(10).pow(18)
      const available = leveragePosition.amount
        .mul(spot)
        .div(ray)
        .sub(leveragePosition.debt)
      method = 'draw(address,uint256,uint256)'
      const extraAmount = parseUnits('10000', 18)
      let borrowAmount = minLpAmount.mul(spot).div(ray)
      if (available.gt(borrowAmount.add(extraAmount))) {
        borrowAmount = borrowAmount
          .add(extraAmount)
          .div(precision)
          .mul(precision)
      }
      params = [pool, +vaultInBytes32, borrowAmount]
      encodedData = iface.encodeFunctionData(method, params)
      await pair.connect(deployer).invoke(leverage.address, encodedData)
    }
  }

  initVatContract (deployer) {
    const MCD_VAT = this.addresses.Maker.MCD_VAT
    const vatContract = new rawEthers.Contract(
      MCD_VAT,
      [
        'function urns(bytes32, address) view returns (uint256 ink, uint256 art)',
        'function ilks(bytes32) view returns (uint256 Art, uint256 rate, uint256 spot, uint256 line, uint256 dust)'
      ],
      deployer
    )
    return vatContract
  }

  async initStaking (pairVaults) {
    const StakingStrategyFactory = await ethers.getContractFactory('Staking')
    const record = this.checkRecord('staking')

    let staking
    if (record && record.address) {
      staking = await ethers.getContractAt(
        StakingStrategyFactory.interface,
        record.address
      )
    } else {
      console.log('[deploying] staking')
      staking = await StakingStrategyFactory.deploy()
      await staking.deployed()
      this.updateRecord('staking', staking.address, { key: 'address' })
    }
    console.log('staking:', staking.address)

    if (!record || !record.setTokens) {
      await staking.setTokens(
        [
          formatBytes32String('DAI'),
          formatBytes32String('wstETH'),
          formatBytes32String('stETH')
        ],
        [
          this.addresses.Tokens.DAI.address,
          this.addresses.Tokens.wstETH.address,
          this.addresses.Tokens.stETH.address
        ]
      )
      this.updateRecord('staking', true, { key: 'setTokens' })
    }

    if (!record || !record.init) {
      console.log('[waiting] staking initialize')
      await staking.initialize(
        rawEthers.constants.AddressZero,
        rawEthers.constants.AddressZero
      )
      this.updateRecord('staking', true, { key: 'init' })
    }

    for (const key in pairVaults) {
      const pair = pairVaults[key]
      const record = this.checkRecord('stakingPairSetups', key)
      if (!record || !record.register) {
        console.log('[waiting] staking register')
        await pair.register(1, staking.address)
        this.updateRecord('stakingPairSetups', { register: true }, { key })
      }
      if (!record || !record.approveWstEth) {
        console.log('[waiting] approve staking use pair wstETH')
        await pair.approveMax(
          staking.address,
          this.addresses.Tokens.wstETH.address
        )
        this.updateRecord('stakingPairSetups', { approveWstEth: true }, { key })
      }
    }
    return staking
  }

  sortedKey (token0, token1) {
    const [tokenA, tokenB] =
      token0.address < token1.address ? [token0, token1] : [token1, token0]
    return `${tokenA.address}-${tokenB.address}`
  }

  checkRecord (target, key) {
    if (this.network.name !== 'goerli') {
      return false
    }

    const filePath = path.resolve(__dirname, '../static/setup.json')
    const data = JSON.parse(fs.readFileSync(filePath))
    if (key !== undefined) {
      return data[target] && data[target][key]
    } else {
      return data[target]
    }
  }

  updateRecord (target, value, options = {}) {
    if (this.network.name !== 'goerli') {
      return false
    }

    const filePath = path.resolve(__dirname, '../static/setup.json')
    const data = JSON.parse(fs.readFileSync(filePath))
    if (options.index !== undefined) {
      if (!data[target]) {
        data[target] = []
      }
      if (options.key !== undefined) {
        if (!data[target][options.index]) {
          data[target][options.index] = {}
        }
        data[target][options.index][options.key] = value
      } else {
        data[target][options.index] = value
      }
    } else if (options.key !== undefined) {
      if (!data[target]) {
        data[target] = {}
      }
      if (typeof value === 'object' && value !== null) {
        if (!data[target][options.key]) {
          data[target][options.key] = {}
        }
        Object.assign(data[target][options.key], value)
      } else {
        data[target][options.key] = value
      }
    } else {
      data[target] = value
    }
    fs.writeFileSync(filePath, JSON.stringify(data), {
      flag: 'w'
    })
  }

  async fetchBalance (target, tokens, pairVaults, leverage) {
    const balance = {}
    balance.ETH = await ethers.provider.getBalance(target.address)
    for (const name in tokens) {
      balance[name] = await tokens[name].balanceOf(target.address)
    }
    for (const key in pairVaults) {
      const pair = pairVaults[key]
      const pool = await pair.pool()
      const v2Pool = await ethers.getContractAt(IERC20.abi, pool)
      const col = await leverage.collaterals(pool)
      balance[col.ilk] = await v2Pool.balanceOf(target.address)
    }
    return balance
  }

  async printBalance (balance, name, tokens) {
    for (const sym in balance) {
      if (sym === 'ETH') {
        console.log(`[${name}] ${sym}:`, formatUnits(balance[sym], 'ether'))
      } else if (sym.length === 66) {
        console.log(
          `[${name}] ${rawEthers.utils.parseBytes32String(sym)}:`,
          formatUnits(balance[sym], 18)
        )
      } else {
        const decimals = await tokens[sym].decimals()
        console.log(`[${name}] ${sym}:`, formatUnits(balance[sym], decimals))
      }
    }
  }

  async printBalances (targets, tokens, pairVaults, leverage) {
    console.log('\n--- balances ---')
    for (const key in targets) {
      const balance = await this.fetchBalance(
        targets[key],
        tokens,
        pairVaults,
        leverage
      )
      await this.printBalance(balance, key, tokens)
    }
  }
}

module.exports = ContractSetup
