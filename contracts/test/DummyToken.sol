// SPDX-License-Identifier: MIT
pragma solidity ^0.8.13;
import 'hardhat/console.sol';

import '@openzeppelin/contracts/token/ERC20/ERC20.sol';

// Example class - a mock class using delivering from ERC20
// Allows custom name, symbol, decimals to test env
// Mock out a real token for DAI, WETH, USDC, WBTC
contract DummyToken is ERC20 {
  uint8 private customdecimals = 18;

  constructor(
    uint256 _supply,
    string memory _symbol,
    uint8 _decimals
  ) ERC20('Dummy', _symbol) {
    if (_decimals != customdecimals) {
      customdecimals = _decimals;
    }

    _mint(msg.sender, _supply);
  }

  function decimals() public view virtual override returns (uint8) {
    return customdecimals;
  }

  // function _beforeTokenTransfer(
  //   address from,
  //   address to,
  //   uint256 amount
  // ) internal virtual override {
  //   console.log(from, to, amount);
  // }

  receive() external payable {
    console.log('[DummyToken:receive]', msg.value);
  }

  fallback() external payable {
    console.logBytes(msg.data);
  }
}
