// SPDX-License-Identifier: MIT
// Copyright (c) 2022 taylor@decouple.co

pragma solidity ^0.8.13;

import '@uniswap/v2-core/contracts/interfaces/IUniswapV2Factory.sol';
import '@uniswap/v2-periphery/contracts/interfaces/IUniswapV2Router02.sol';
import '@uniswap/v2-core/contracts/interfaces/IUniswapV2ERC20.sol';
import 'hardhat/console.sol';

import './interfaces/IERC20.sol';
import './interfaces/IStrategy.sol';

// Local require for solidity 0.8+ compatibility
import './libraries/UniswapV2Library.sol';

interface IPairVault {
  struct Position {
    // deposit of the position owner token0/token1
    // the amount of liquidity LP token owned by this position
    uint112 amount;
    // debt
    uint112 debt;
    // block timestamp in second
    uint32 blocktime;
    // second slot, fee earned, available balance for withdraw
    uint128 available;
    bytes16 info;
  }

  function token0() external returns (address);

  function token1() external returns (address);

  function pool() external returns (address);

  function getReserves()
    external
    returns (
      uint112 reserve0,
      uint112 reserve1,
      uint256 totalSupply
    );

  function positions(address owner, address strategy)
    external
    returns (Position calldata);
}

interface UniswapInterface {
  function router() external returns (address);
}

interface VatLike {
  function urns(bytes32, address) external view returns (uint256, uint256);
}

interface IDssPsm {
  function vat() external view returns (address);

  function ilk() external view returns (bytes32);

  function tin() external view returns (uint256);

  function tout() external view returns (uint256);

  function sellGem(address usr, uint256 gemAmt) external;

  function buyGem(address usr, uint256 gemAmt) external;
}

// Default LP strategy
contract UniswapV2 is IStrategy, UniswapInterface {
  string public override name = 'UniswapV2';
  bytes4 public override selector =
    bytes4(keccak256(bytes('UniswapV2(address,address)')));

  address public factory;
  address public router;

  mapping(bytes32 => address) internal tokens;
  mapping(bytes32 => address) internal contracts;

  // pool address shall be store on pair vault,
  // strategy shall only include constant state that does not change between
  // different token / asset pairs.
  constructor(address _router) {
    router = _router;
    factory = IUniswapV2Router02(_router).factory();
  }

  function initialize(address _token0, address _token1)
    external
    view
    returns (bytes32)
  {
    address pool = UniswapV2Library.pairFor(factory, _token0, _token1);
    return bytes32(bytes20(pool));
  }

  function setTokens(
    bytes32[] calldata _nameList,
    address[] calldata _addressList
  ) external {
    require(_nameList.length == _addressList.length, 'invalid-token-inputs');
    for (uint8 i = 0; i < _nameList.length; i++) {
      tokens[_nameList[i]] = _addressList[i];
    }
  }

  function setContracts(
    bytes32[] calldata _nameList,
    address[] calldata _addressList
  ) external {
    require(_nameList.length == _addressList.length, 'invalid-contract-inputs');
    for (uint8 i = 0; i < _nameList.length; i++) {
      contracts[_nameList[i]] = _addressList[i];
    }
  }

  function allocate(
    address _pool,
    uint256 _amount0,
    uint256 _amount1
  ) external returns (bytes[] memory) {
    console.log(
      '[UniswapV2:allocate] _pool, _amount0-1:',
      _pool,
      _amount0,
      _amount1
    );

    address token0 = IUniswapV2Pair(_pool).token0();
    address token1 = IUniswapV2Pair(_pool).token1();

    IERC20(token0).approve(router, _amount0);
    IERC20(token1).approve(router, _amount1);
    // TODO: check current liquidity for this user
    (uint256 amountUsed0, uint256 amountUsed1, uint256 newLiquidity) = IUniswapV2Router02(
      router
    ).addLiquidity(
        token0,
        token1, // token A, token B
        _amount0,
        _amount1, // amount A, amount B
        0,
        0, // min amount A, min amount B
        msg.sender, // transfer to
        block.timestamp + 5 minutes // expery
      );

    if (_amount0 > amountUsed0) {
      IERC20(token0).approve(msg.sender, _amount0 - amountUsed0);
    }
    if (_amount1 > amountUsed1) {
      IERC20(token1).approve(msg.sender, _amount1 - amountUsed1);
    }

    emit Allocate(_pool, _amount0, _amount1);

    bytes[] memory result = new bytes[](1);
    result[0] = abi.encode(address(this), newLiquidity, 0, 0, 0);
    return result;
  }

  function free(
    address pool,
    uint256 _amount0,
    uint256 _amount1
  ) external returns (bytes[] memory) {
    address token0 = IUniswapV2Pair(pool).token0();
    address token1 = IUniswapV2Pair(pool).token1();

    uint256 liquidity;
    uint256 amount0;
    uint256 amount1;

    {
      (uint112 reserve0, uint112 reserve1, uint256 totalSupply) = IPairVault(
        msg.sender
      ).getReserves();

      // see UniswapV2Pair#_burn
      // uint _totalSupply = totalSupply; // gas savings, must be defined here since totalSupply can update in _mintFee
      // amount0 = liquidity.mul(balance0) / _totalSupply; // using balances ensures pro-rata distribution
      // amount1 = liquidity.mul(balance1) / _totalSupply; // using balances ensures pro-rata distribution

      // FIXME: + minLiquidity hack can be removed once we setup pools with initial liquidity
      // PairVault contract shouldn't be first to setup the pool or it would mess up the math
      // _balance0 = (pos.amount * reserve0) / (totalSupply);
      // _balance1 = (pos.amount * reserve1) / (totalSupply);

      // Decrease liquidity
      // existing liquidity x (amount0 / balance0)

      // Note: using _amount1 would not need a hack, related to USDC with lower decimals 6
      liquidity = (_amount0 * totalSupply) / reserve0;

      uint256 liquidity1 = (_amount1 * totalSupply) / reserve1;
      if (liquidity1 > liquidity) {
        liquidity = liquidity1;
      }
      // liquidity = _amount1 * totalSupply / reserve1;
      // liquidity = pos.amount * _amount1 / _balance1;
      // liquidity = pos.amount * _amount0 / _balance0;

      console.log('[UniswapV2:free] liquidity', liquidity);
    }
    // require(_balance0 >= _amount0, "insufficient-balance0");
    // require(_balance1 >= _amount1, "insufficient-balance1");
    // console.log("balance 0-1", _balance0, _balance1);

    // Transfer Liquidity token from PairVault to UniswapV2 strategy
    IERC20(pool).transferFrom(msg.sender, address(this), liquidity);

    // IERC20(pool).approve(address(this), pos.amount);
    IERC20(pool).approve(router, liquidity);

    (amount0, amount1) = IUniswapV2Router02(router).removeLiquidity(
      token0,
      token1, // token A, token B
      liquidity, // liquidity
      (_amount0 * 97) / 100, // 3% slippage, 0.5 ETH, 1500 USDC
      (_amount1 * 97) / 100, // need to fix calculation of liquidity
      msg.sender, // transfer to
      block.timestamp + 5 minutes // expery
    );

    console.log('[UniswapV2:free] amount0-1', amount0, amount1);

    bytes[] memory result = new bytes[](1);
    result[0] = abi.encode(amount0, amount1, liquidity);
    return result;
  }

  // What is useful here for Uniswap Pool (reserve0, reserve1, totalSupply)
  function check(uint256) external view returns (uint256[] memory) {
    console.log('dummy strategy: check status, balance');
    return new uint256[](0);
  }

  function fold(
    address,
    uint256,
    address
  ) external view returns (bytes[] memory) {
    console.log('dummy strategy: fold/close the position completely');
    return new bytes[](0);
  }

  function swapTokenToEth(address _token0, uint256 _amountIn)
    public
    returns (uint256 amountOut)
  {
    IERC20(_token0).transferFrom(msg.sender, address(this), _amountIn);
    amountOut = _swapTokenToEth(_token0, _amountIn);

    (bool success, ) = address(msg.sender).call{ value: amountOut }(
      new bytes(0)
    );
    require(success, 'univ2-swap-to-eth-failed');
  }

  function _swapTokenToEth(address _token0, uint256 _amountIn)
    internal
    returns (uint256 amountOut)
  {
    IERC20(_token0).approve(router, _amountIn);
    address[] memory path = new address[](2);
    path[0] = _token0;
    path[1] = tokens['WETH'];
    uint256[] memory amounts = IUniswapV2Router02(router).swapExactTokensForETH(
        _amountIn,
        0,
        path,
        address(this), // transfer to
        block.timestamp + 5 minutes // expery
      );
    amountOut = amounts[amounts.length - 1];
  }

  // Swap token0, token1, _amountIn
  // Swap LPToken, DAI, _amountIn
  // Swap DAI, LPToken, _amountIn
  function swap(
    address _token0,
    address _token1,
    uint256 _amountIn
  )
    public
    returns (
      uint256 amountOut0,
      uint256 amountOut1,
      uint256 amountOutMain
    )
  {
    console.log('[UniswapV2:swap]', _token0, _token1, _amountIn);
    require(_token0 != address(0), 'invalid-token0');
    require(_token1 != address(0), 'invalid-token1');
    require(_amountIn > 0, 'invalid-amountIn');

    if (
      // check if token0 is UNI LP Token
      keccak256(abi.encode(IUniswapV2ERC20(_token0).symbol())) ==
      keccak256(abi.encode('UNI-V2')) &&
      _token1 == tokens['DAI']
    ) {
      amountOutMain = _swapLpToDai(_token0, _token1, _amountIn);
    } else if (
      // check if token1 is UNI LP Token
      _token0 == tokens['DAI'] &&
      keccak256(abi.encode(IUniswapV2ERC20(_token1).symbol())) ==
      keccak256(abi.encode('UNI-V2'))
    ) {
      (amountOut0, amountOut1, amountOutMain) = _swapDaiToLp(
        _token0,
        _token1,
        _amountIn
      );
    } else if (_token0 == tokens['stETH'] && _token1 == tokens['DAI']) {
      amountOutMain = _swapStEthToDai(_token0, _token1, _amountIn);
      IERC20(_token1).approve(msg.sender, amountOutMain);
    } else {
      IERC20(_token0).transferFrom(msg.sender, address(this), _amountIn);
      amountOutMain = _swapTokens(_token0, _token1, _amountIn, msg.sender);
    }

    require(amountOutMain > 0, 'invalid-amountOut');
  }

  function quoteDaiToStEth(uint256 _amountIn) external view returns (uint256) {
    address[] memory path = new address[](3);
    path[0] = tokens['DAI'];
    path[1] = tokens['WETH'];
    path[2] = tokens['stETH'];
    uint256[] memory result = _getAmountsOut(_amountIn, path);
    return result[result.length - 1];
  }

  function _swapStEthToDai(
    address _token0,
    address _token1,
    uint256 _amountIn
  ) internal returns (uint256 amountOut) {
    console.log('[UniswapV2:_swapStEthToDai]', _token0, _token1, _amountIn);
    IERC20(_token0).transferFrom(msg.sender, address(this), _amountIn);
    address[] memory path = new address[](3);
    path[0] = _token0;
    path[1] = tokens['WETH'];
    path[2] = _token1;
    IERC20(_token0).approve(router, _amountIn);
    uint256 balanceBefore = IERC20(_token1).balanceOf(address(this));

    IUniswapV2Router02(router)
      .swapExactTokensForTokensSupportingFeeOnTransferTokens(
        _amountIn,
        0,
        path,
        address(this),
        block.timestamp + 5 minutes
      );
    uint256 balanceAfter = IERC20(_token1).balanceOf(address(this));
    require(balanceAfter > balanceBefore, 'invalid-swap');

    amountOut = balanceAfter - balanceBefore;
  }

  function raise(
    address,
    uint256,
    uint256,
    address
  ) external view returns (bytes[] memory) {
    console.log('dummy strategy: draw liquidity and increase leverage');
    return new bytes[](0);
  }

  function payback(
    address,
    uint256,
    uint256
  ) external view returns (bytes[] memory) {
    console.log('dummy strategy: repay debt');
    return new bytes[](0);
  }

  function draw(
    address,
    uint256,
    uint256
  ) external view returns (bytes[] memory) {
    console.log('dummy strategy: draw liquidity / borrow');
    return new bytes[](0);
  }

  // --- Helpers ---

  function _swapDaiToLp(
    address _tokenDAI,
    address _tokenLP,
    uint256 _amountIn
  )
    internal
    returns (
      uint256 amountLeft0,
      uint256 amountLeft1,
      uint256 liquidityOut
    )
  {
    address token0 = IUniswapV2Pair(_tokenLP).token0();
    address token1 = IUniswapV2Pair(_tokenLP).token1();
    require(token0 < token1, 'invalid-tokens');

    // 1. swap DAI to token0 & token1
    uint256 amount0 = _swapTokens(
      _tokenDAI,
      token0,
      _amountIn / 2,
      address(this)
    );
    uint256 amount1 = _swapTokens(
      _tokenDAI,
      token1,
      _amountIn / 2,
      address(this)
    );

    // 2. add token0 & token1 into v2 pool
    uint256 amountUsed0;
    uint256 amountUsed1;
    (amountUsed0, amountUsed1, liquidityOut) = _addLiquidity(
      token0,
      token1,
      amount0,
      amount1
    );
    amountLeft0 = amount0 - amountUsed0;
    amountLeft1 = amount1 - amountUsed1;
    if (amountLeft0 > 0) {
      IERC20(token0).transfer(msg.sender, amountLeft0);
    }
    if (amountLeft1 > 0) {
      IERC20(token1).transfer(msg.sender, amountLeft1);
    }
    require(liquidityOut > 0, 'invalid-liquidity-out');
  }

  function _swapLpToDai(
    address _tokenLP,
    address _tokenDAI,
    uint256 _amountIn
  ) internal returns (uint256 amountOut) {
    console.log('[UniswapV2:_swapLpToDai]', _tokenLP, _tokenDAI, _amountIn);

    // 1. remove LP liquidity to get token0 & token1
    address token0 = IUniswapV2Pair(_tokenLP).token0();
    address token1 = IUniswapV2Pair(_tokenLP).token1();
    require(token0 < token1, 'invalid-tokens');
    (uint256 amountOut0, uint256 amountOut1) = _removeLiquidity(
      _tokenLP,
      _amountIn,
      token0,
      token1
    );
    require(amountOut0 > 0, 'invalid-amountOut0');
    require(amountOut1 > 0, 'invalid-amountOut1');

    // 2. swap token0 & token1 to DAI
    require(_tokenDAI == tokens['DAI'], 'invalid-DAI-address');
    amountOut = _swapTokens(token0, _tokenDAI, amountOut0, address(this));
    amountOut += _swapTokens(token1, _tokenDAI, amountOut1, address(this));
    IERC20(_tokenDAI).transfer(msg.sender, amountOut);
  }

  function _swapTokens(
    address _token0,
    address _token1,
    uint256 _amountIn,
    address _to
  ) internal returns (uint256 amountOut) {
    console.log('[UniswapV2:_swapTokens]', _token0, _token1, _amountIn);
    if (_token0 == _token1) {
      return _amountIn;
    }

    if (_token0 == tokens['DAI'] && _token1 == tokens['USDC']) {
      (uint256 ink, , uint256 tout) = _checkPsm();
      if (tout == 0 && ink >= _amountIn) {
        return _swapDaiToUsdcViaPsm(_amountIn, _to);
      }
    } else if (_token0 == tokens['USDC'] && _token1 == tokens['DAI']) {
      (, uint256 tin, ) = _checkPsm();
      if (tin == 0) {
        return _swapUsdcToDaiViaPsm(_amountIn, _to);
      }
    }
    amountOut = _swapTokensViaUni(_token0, _token1, _amountIn, _to);
  }

  function _swapTokensViaUni(
    address _token0,
    address _token1,
    uint256 _amountIn,
    address _to
  ) internal returns (uint256 amountOut) {
    console.log('[UniswapV2:_swapTokensViaUni]', _token0, _token1, _amountIn);
    address[] memory path = new address[](2);
    path[0] = _token0;
    path[1] = _token1;

    IERC20(_token0).approve(router, _amountIn);
    uint256[] memory result = IUniswapV2Router02(router)
      .swapExactTokensForTokens(
        _amountIn,
        0,
        path,
        _to,
        block.timestamp + 5 minutes
      );
    amountOut = result[result.length - 1];
  }

  function _swapDaiToUsdcViaPsm(uint256 _amountIn, address _owner)
    internal
    returns (uint256 amountOut)
  {
    console.log('[UniswapV2:_swapDaiToUsdcViaPsm] _amountIn', _amountIn);

    IDssPsm psmUsdc = IDssPsm(contracts['MCD_PSM_USDC_A']);
    IERC20(tokens['DAI']).approve(address(psmUsdc), _amountIn);
    amountOut = (_amountIn * 10**6) / 10**18;
    psmUsdc.buyGem(_owner, amountOut);
  }

  function _swapUsdcToDaiViaPsm(uint256 _amountIn, address _to)
    internal
    returns (uint256 amountOut)
  {
    console.log('[UniswapV2:_swapUsdcToDaiViaPsm] _amountIn', _amountIn);

    IDssPsm psmUsdc = IDssPsm(contracts['MCD_PSM_USDC_A']);
    IERC20(tokens['USDC']).approve(contracts['MCD_JOIN_PSM_USDC_A'], _amountIn);
    psmUsdc.sellGem(_to, _amountIn);
    amountOut = (_amountIn * 10**18) / 10**6;
  }

  function _addLiquidity(
    address _token0,
    address _token1,
    uint256 _amountIn0,
    uint256 _amountIn1
  )
    internal
    returns (
      uint256 amountUsed0,
      uint256 amountUsed1,
      uint256 liquidityOut
    )
  {
    IERC20(_token0).approve(router, _amountIn0);
    IERC20(_token1).approve(router, _amountIn1);
    (amountUsed0, amountUsed1, liquidityOut) = IUniswapV2Router02(router)
      .addLiquidity(
        _token0,
        _token1,
        _amountIn0,
        _amountIn1,
        0,
        0,
        // (_amountIn0 * 95) / 100, // min amount 0, 5% slippage
        // (_amountIn1 * 95) / 100, // min amount 1, 5% slippage
        msg.sender, // transfer to
        block.timestamp + 5 minutes // expery
      );
  }

  function _removeLiquidity(
    address _tokenLP,
    uint256 _amountIn,
    address _token0,
    address _token1
  ) internal returns (uint256 amountOut0, uint256 amountOut1) {
    IUniswapV2ERC20(_tokenLP).approve(router, _amountIn);
    (amountOut0, amountOut1) = IUniswapV2Router02(router).removeLiquidity(
      _token0,
      _token1,
      _amountIn,
      0, // min amount 0
      0, // min amount 1
      address(this), // transfer to
      block.timestamp + 5 minutes // expery
    );
  }

  function _getAmountsOut(uint256 amountIn, address[] memory path)
    internal
    view
    returns (uint256[] memory amounts)
  {
    return IUniswapV2Router02(router).getAmountsOut(amountIn, path);
  }

  function _checkPsm()
    internal
    view
    returns (
      uint256 ink,
      uint256 tin,
      uint256 tout
    )
  {
    IDssPsm psmUsdc = IDssPsm(contracts['MCD_PSM_USDC_A']);
    VatLike vat = VatLike(psmUsdc.vat());
    (ink, ) = vat.urns(psmUsdc.ilk(), address(psmUsdc));
    tin = psmUsdc.tin();
    tout = psmUsdc.tout();

    console.log('[UniswapV2:_checkPsm] ink, tin, tout', ink, tin, tout);
  }

  receive() external payable {}

  fallback() external payable {}
}
