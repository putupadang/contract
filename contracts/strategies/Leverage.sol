// SPDX-License-Identifier: MIT
// Copyright (c) 2022 taylor@decouple.co

pragma solidity ^0.8.13;

import 'hardhat/console.sol';

import '@openzeppelin/contracts/token/ERC20/extensions/IERC20Metadata.sol';
import '../interfaces/IStrategy.sol';
import '../interfaces/MakerInterfaces.sol';
import '../interfaces/LeverageInterfaces.sol';
import '../interfaces/IERC3156FlashBorrower.sol';
import '../interfaces/IERC3156FlashLender.sol';

contract Leverage is IStrategy {
  string public override name = 'Leverage:MakerDAO';
  bytes4 public override selector =
    bytes4(keccak256(bytes('Leverage(address,address)')));

  struct Collateral {
    bytes32 ilk;
    address join;
    address urn;
    address pair;
  }

  struct Maker {
    IManager manager;
    IDaiJoin daiJoin;
    IJug mcdJug;
    ISpot spot;
    IVat vat;
    IERC3156FlashLender flashLender;
  }
  Maker public maker;

  mapping(bytes32 => address) public tokens;
  mapping(address => Collateral) public collaterals;
  mapping(uint256 => address) public vaultToCollateral;

  constructor(
    address _manager,
    address[] memory _pools,
    string[] memory _types,
    address[] memory _joins,
    address[] memory _pairs
  ) {
    maker.manager = IManager(_manager);
    maker.vat = IVat(maker.manager.vat());
    for (uint256 i = 0; i < _pools.length; i++) {
      collaterals[_pools[i]] = Collateral({
        ilk: bytes32(bytes(_types[i])),
        urn: address(0),
        join: _joins[i],
        pair: _pairs[i]
      });
    }
  }

  /*
    Uniswap V2 Pair Address to MakerDAO colalteral type mapping
      - (USDC, ETH) > UNI-V2-USDCETH
      - (DAI, WBTC) > UNI-V2-WBTCDAI
  */
  function initialize(address _pool, address _vaultId)
    external
    returns (bytes32)
  {
    Collateral storage col = collaterals[_pool];
    uint256 vault = uint256(uint160(_vaultId));

    // TODO:
    // 1. open new vault
    // 2. set self as maker manager
    // 3. set pair as owner
    if (vault == 0) {
      vault = maker.manager.open(col.ilk, address(this));
    }

    col.urn = maker.manager.urns(vault);
    vaultToCollateral[vault] = _pool;

    return bytes32(vault);
  }

  function setTokens(
    bytes32[] calldata _nameList,
    address[] calldata _addressList
  ) external {
    require(_nameList.length == _addressList.length, 'invalid-token-inputs');
    for (uint8 i = 0; i < _nameList.length; i++) {
      tokens[_nameList[i]] = _addressList[i];
    }
  }

  function setContracts(
    bytes32[] calldata _nameList,
    address[] calldata _addressList
  ) external {
    require(_nameList.length == _addressList.length, 'invalid-token-inputs');
    for (uint8 i = 0; i < _nameList.length; i++) {
      if (_nameList[i] == 'MCD_JOIN_DAI') {
        maker.daiJoin = IDaiJoin(_addressList[i]);
      } else if (_nameList[i] == 'MCD_JUG') {
        maker.mcdJug = IJug(_addressList[i]);
      } else if (_nameList[i] == 'MCD_SPOT') {
        maker.spot = ISpot(_addressList[i]);
      } else if (_nameList[i] == 'MCD_FLASH_LEGACY') {
        maker.flashLender = IERC3156FlashLender(_addressList[i]);
      }
    }
  }

  // --- External Methods ---

  function payback(
    address _pool,
    uint256 _vault,
    uint256 _amount
  ) external returns (bytes[] memory result) {
    require(_amount > 0, 'invalid-payback-amount');
    maker.daiJoin.dai().transferFrom(msg.sender, address(this), _amount);
    _payback(_pool, _vault, _amount);

    if (msg.sender == collaterals[_pool].pair) {
      result = new bytes[](2);
      bytes16 info = _calcInfo(_pool, 0, _amount);
      result[0] = abi.encode(address(this), 0, 0, -int256(_amount), info);
      result[1] = abi.encode(msg.sender, 0, 0, 0, 0);
    } else {
      result = new bytes[](1);
      bytes16 info = _calcInfo(_pool, 0, _amount);
      result[0] = abi.encode(address(this), 0, 0, -int256(_amount), info);
    }
  }

  function fold(
    address _pool,
    uint256 _vault,
    address _owner
  ) external returns (bytes[] memory) {
    (uint256 pairAmount, , , uint256 pairAvailable, ) = IPairVault(
      collaterals[_pool].pair
    ).positions(_owner, collaterals[_pool].pair);
    (uint256 amount, uint256 debt, , , ) = IPairVault(collaterals[_pool].pair)
      .positions(_owner, address(this));
    uint256[] memory statsBefore = check(_vault);

    if (debt > 0) {
      _payDebt(_pool, _vault, debt);
    }

    uint256[] memory statsAfter = check(_vault);
    uint256 maxAmountOut = (amount - (statsBefore[0] - statsAfter[0]));
    free(_pool, _vault, maxAmountOut);

    bytes[] memory result = new bytes[](2);
    result[0] = abi.encode(address(this), 0, 0, 0, 0);
    if (maxAmountOut > pairAmount - pairAvailable) {
      result[1] = abi.encode(
        msg.sender,
        maxAmountOut - (pairAmount - pairAvailable),
        maxAmountOut + pairAvailable,
        0,
        0
      );
    } else {
      result[1] = abi.encode(
        msg.sender,
        -int256(pairAmount - pairAvailable - maxAmountOut),
        maxAmountOut + pairAvailable,
        0,
        0
      );
    }

    IPairVault(collaterals[_pool].pair).popStack(_owner);
    return result;
  }

  function foldAll(address _pool, uint256 _vault) external {
    uint256[] memory statsBefore = check(_vault);

    if (statsBefore[2] > 0) {
      maker.daiJoin.dai().transferFrom(
        msg.sender,
        address(this),
        statsBefore[2]
      );
      _payback(_pool, _vault, statsBefore[2]);
    }

    uint256[] memory statsAfter = check(_vault);
    free(_pool, _vault, statsAfter[0]);
  }

  // --- Public Methods ---

  // Raise to colalteral value target
  function raise(
    address _pool,
    uint256 _vault,
    uint256 _targetValue,
    address _owner
  ) public returns (bytes[] memory) {
    uint256[] memory stats0 = check(_vault);
    (, , , uint256 pairAvailable, ) = IPairVault(collaterals[_pool].pair)
      .positions(_owner, address(collaterals[_pool].pair));
    (uint256 amount, , , , ) = IPairVault(collaterals[_pool].pair).positions(
      _owner,
      address(this)
    );
    uint256 target = stats0[0] -
      amount +
      (_targetValue * 10**27) /
      getPrice(_vault);

    if (target > stats0[0]) {
      _raiseUp(_pool, _vault, target, _owner);
    } else if (target < stats0[0]) {
      _decreaseCollateral(_pool, _vault, target);
    }

    uint256[] memory stats1 = check(_vault);
    bytes[] memory result;
    int256 changedAmount;
    int256 changedDebt;

    if (stats1[0] < stats0[0]) {
      changedAmount = -int256(stats0[0] - stats1[0]);
    } else {
      changedAmount = int256(stats1[0] - stats0[0]);
    }

    bytes16 info;
    if (stats0[2] > stats1[2]) {
      changedDebt = -int256(stats0[2] - stats1[2]);
      info = _calcInfo(_pool, 0, stats0[2] - stats1[2]);
    } else {
      changedDebt = int256(stats1[2] - stats0[2]);
      info = _calcInfo(_pool, stats1[2] - stats0[2], 0);
    }

    checkCRatio(_pool, _vault, _owner, changedAmount, changedDebt);

    if (pairAvailable > 0) {
      result = new bytes[](2);
      result[1] = abi.encode(msg.sender, 0, -int256(pairAvailable), 0, 0);
    } else {
      result = new bytes[](1);
    }
    result[0] = abi.encode(address(this), changedAmount, 0, changedDebt, info);
    return result;
  }

  function allocate(
    address _pool,
    uint256 _vault,
    uint256 _amount
  ) public returns (bytes[] memory result) {
    require(_amount > 0, 'invalid-allocate-amount');
    require(
      IERC20(_pool).balanceOf(msg.sender) >= _amount,
      'insufficient-balance'
    );

    IUniswapV2ERC20(_pool).transferFrom(msg.sender, address(this), _amount);
    _allocate(_pool, _vault, _amount);
    emit Allocate(_pool, _vault, _amount);

    if (msg.sender == collaterals[_pool].pair) {
      result = new bytes[](2);
      result[0] = abi.encode(address(this), _amount, 0, 0, 0);
      result[1] = abi.encode(msg.sender, 0, -int256(_amount), 0, 0);
    } else {
      result = new bytes[](1);
      result[0] = abi.encode(address(this), _amount, 0, 0, 0);
    }
  }

  function draw(
    address _pool,
    uint256 _vault,
    uint256 _amount
  ) public returns (bytes[] memory result) {
    uint256 drawAmount = _draw(_pool, _vault, _amount);
    IERC20(tokens['DAI']).transfer(msg.sender, drawAmount);

    if (msg.sender == collaterals[_pool].pair) {
      result = new bytes[](2);
      bytes16 info = _calcInfo(_pool, _amount, 0);
      result[0] = abi.encode(address(this), 0, 0, _amount, info);
      result[1] = abi.encode(collaterals[_pool].pair, 0, 0, 0, 0);
    } else {
      result = new bytes[](1);
      bytes16 info = _calcInfo(_pool, _amount, 0);
      result[0] = abi.encode(address(this), 0, 0, _amount, info);
    }
  }

  function free(
    address _pool,
    uint256 _vault,
    uint256 _amount
  ) public returns (bytes[] memory result) {
    uint256 freeAmount = _free(_pool, _vault, _amount);
    IUniswapV2ERC20(_pool).transfer(msg.sender, freeAmount);

    if (msg.sender == collaterals[_pool].pair) {
      result = new bytes[](2);
      result[0] = abi.encode(address(this), -int256(freeAmount), 0, 0, 0);
      result[1] = abi.encode(msg.sender, 0, freeAmount, 0, 0);
    } else {
      result = new bytes[](1);
      result[0] = abi.encode(address(this), -int256(freeAmount), 0, 0, 0);
    }
  }

  // --- Public Helpers ---

  // input vault id, output (collateral, available, debt, c-ratio)
  function check(uint256 _vault) public view returns (uint256[] memory stats) {
    stats = new uint256[](4);
    Collateral memory col = collaterals[vaultToCollateral[_vault]];

    uint256 dai = maker.vat.dai(col.urn);
    (uint256 ink, uint256 art) = maker.vat.urns(col.ilk, col.urn);
    (, uint256 rate, uint256 spot, , ) = maker.vat.ilks(col.ilk);

    // Collateral amount
    stats[0] = ink;

    // Debt
    uint256 ray = 10**27;
    uint256 wad;
    if (art > 0) {
      uint256 rad = art * rate - dai;
      wad = rad / ray;
      wad = wad * ray < rad ? wad + 1 : wad;
    } else {
      wad = 0;
    }

    // Available in DAI
    stats[1] = (spot * ink) / 10**27 >= wad
      ? ((spot * ink) - wad * 10**27) / 10**27
      : 0;

    // Debt
    stats[2] = wad;

    // C-Ratio in basis points
    if (wad > 0) {
      (, uint256 mat) = maker.spot.ilks(col.ilk);
      uint256 par = maker.spot.par();
      uint256 price = (spot * mat) / par;
      stats[3] = (((price * ink) / ray) * 1000) / wad;
    }

    console.log('[Leverage:check] collateral', stats[0]);
    console.log('[Leverage:check] available', stats[1]);
    console.log('[Leverage:check] debt', stats[2]);
    console.log('[Leverage:check] ratio', stats[3]);
    return stats;
  }

  // amount, available, debt, c-ratio
  function checkUser(
    address _pool,
    uint256 _vault,
    address _owner
  ) public view returns (uint256[] memory stats) {
    (uint256 amount, uint256 debt, , , ) = IPairVault(collaterals[_pool].pair)
      .positions(_owner, address(this));
    (, , uint256 spot, , ) = maker.vat.ilks(
      collaterals[vaultToCollateral[_vault]].ilk
    );

    stats = new uint256[](4);
    stats[0] = amount;
    stats[1] = (amount * spot) / 10**27 - debt;
    stats[2] = debt;
    if (debt > 0) {
      stats[3] = (((amount * getPrice(_vault)) / 10**27) * 1000) / debt;
    }
  }

  // Collateral price without the safety margin
  function getPrice(uint256 _vault) public view returns (uint256 price) {
    Collateral memory col = collaterals[vaultToCollateral[_vault]];
    (, , uint256 spot, , ) = maker.vat.ilks(col.ilk);
    (, uint256 mat) = maker.spot.ilks(col.ilk);
    uint256 par = maker.spot.par();
    price = (spot * mat) / par;
  }

  function getSpot(uint256 _vault) public view returns (uint256 spot) {
    (, , spot, , ) = maker.vat.ilks(collaterals[vaultToCollateral[_vault]].ilk);
  }

  // --- Internal Helpers ---

  function checkCRatio(
    address _pool,
    uint256 _vault,
    address _owner,
    int256 _changedAmount,
    int256 _changedDebt
  ) internal view {
    (uint256 amount0, uint256 debt0, , , ) = IPairVault(collaterals[_pool].pair)
      .positions(_owner, address(this));

    uint256 amount1;
    uint256 debt1;
    if (_changedAmount >= 0) {
      amount1 = amount0 + uint256(_changedAmount);
      debt1 = debt0 + uint256(_changedDebt);
    } else {
      amount1 = amount0 - uint256(-_changedAmount);
      debt1 = debt0 < uint256(-_changedDebt)
        ? 0
        : debt0 - uint256(-_changedDebt);
    }

    uint256 price = getPrice(_vault);
    if (debt1 > 0) {
      uint256 userCRatio = (((amount1 * price) / 10**27) * 1000) / debt1;
      (, uint256 mat) = maker.spot.ilks(collaterals[_pool].ilk);
      uint256 vaultCRatio = (mat * 1000) / maker.spot.par();
      require(userCRatio >= vaultCRatio, 'c-ratio-check-failed');
    }
  }

  function _allocate(
    address _pool,
    uint256,
    uint256 _amount
  ) internal {
    Collateral memory col = collaterals[_pool];
    ITokenJoin tokenJoin = ITokenJoin(col.join);
    IToken token = tokenJoin.gem();

    if (_amount == type(uint256).max) {
      _amount = token.balanceOf(msg.sender);
    }

    IERC20(_pool).approve(col.join, _amount);
    token.approve(col.urn, _amount);
    tokenJoin.join(address(this), _amount);
    maker.vat.frob(
      col.ilk,
      col.urn,
      address(this),
      address(this),
      int256(_amount),
      0
    );
  }

  function _payback(
    address _pool,
    uint256 _vault,
    uint256 _amount
  ) internal {
    Collateral memory col = collaterals[_pool];
    uint256 maxDebt = _getVaultDebt(_vault);
    _amount = _amount == type(uint256).max ? maxDebt : _amount;

    require(maxDebt >= _amount, 'paying-excess-debt');
    require(
      maker.daiJoin.dai().balanceOf(address(this)) >= _amount,
      'insufficient-payback-balance'
    );

    maker.daiJoin.dai().approve(address(maker.daiJoin), _amount);
    maker.daiJoin.join(col.urn, _amount);
    int256 dart = _getWipeAmt(maker.vat.dai(col.urn), col.urn, col.ilk);
    maker.manager.frob(_vault, 0, dart);
  }

  function _free(
    address _pool,
    uint256 _vault,
    uint256 _amount
  ) internal returns (uint256) {
    Collateral memory col = collaterals[_pool];
    ITokenJoin tokenJoin = ITokenJoin(col.join);

    uint256 _amt18;
    if (_amount == type(uint256).max) {
      (_amt18, ) = maker.vat.urns(col.ilk, col.urn);
      _amount = _amt18;
    } else {
      _amt18 = _amount;
    }

    maker.manager.frob(_vault, -_toInt(_amt18), 0);
    maker.manager.flux(_vault, address(this), _amt18);
    tokenJoin.exit(address(this), _amount);

    return _amount;
  }

  function _payDebt(
    address _pool,
    uint256 _vault,
    uint256 _debt
  ) internal {
    uint256[] memory stats = check(_vault);
    uint256 price = getPrice(_vault);
    (, uint256 rate, , , ) = maker.vat.ilks(collaterals[_pool].ilk);
    uint256 targetAmount = (stats[0] * price - _debt * rate) / price;
    _decreaseCollateral(_pool, _vault, targetAmount);
  }

  function _draw(
    address _pool,
    uint256 _vault,
    uint256 _amount
  ) internal returns (uint256) {
    int256 dart = _getBorrowAmt(
      collaterals[_pool].urn,
      collaterals[_pool].ilk,
      _amount
    );

    maker.manager.frob(_vault, 0, dart);
    maker.manager.move(_vault, address(this), _amount * 10**27);
    if (maker.vat.can(address(this), address(maker.daiJoin)) == 0) {
      maker.vat.hope(address(maker.daiJoin));
    }
    maker.daiJoin.exit(address(this), _amount);
    return _amount;
  }

  // 1. calculate total required DAI
  // 2. borrow from PairVault / flashLender
  // 3. swap DAI into LP
  // 4. handle addLiquidity remaining tokens
  // 5. allocate LP
  // 6. draw DAI to payback PairVault / flashLender
  function _raiseUp(
    address _pool,
    uint256 _vault,
    uint256 _target,
    address _owner
  ) internal {
    console.log('\n[Leverage:raise-up] _vault', _vault, '_target', _target);

    IPairVault pair = IPairVault(collaterals[_pool].pair);
    (, , , uint256 pairAvailable, ) = IPairVault(collaterals[_pool].pair)
      .positions(_owner, address(pair));

    // 1. calculate total required DAI
    uint256 borrowAmount = ((_target - check(_vault)[0] - pairAvailable) *
      getPrice(_vault)) / 10**27;

    if (IERC20(tokens['DAI']).balanceOf(address(pair)) < borrowAmount) {
      bytes memory data = abi.encode(
        _pool,
        _vault,
        _owner,
        borrowAmount,
        pairAvailable,
        address(maker.flashLender)
      );
      _flashBorrow(borrowAmount, data);
    } else {
      bytes memory data = abi.encode(
        _pool,
        _vault,
        _owner,
        borrowAmount,
        pairAvailable,
        address(pair)
      );
      _performRaiseUp(data);
    }
  }

  function _performRaiseUp(bytes memory _data) internal {
    (
      address pool,
      uint256 vault,
      address owner,
      uint256 borrowAmount,
      uint256 pairAvailable,
      address lender
    ) = abi.decode(
        _data,
        (address, uint256, address, uint256, uint256, address)
      );

    IPairVault pair = IPairVault(collaterals[pool].pair);

    // 2. borrow from PairVault / flashLender
    if (lender != address(maker.flashLender)) {
      IERC20(tokens['DAI']).transferFrom(lender, pair.strategy(), borrowAmount);
    } else {
      IERC20(tokens['DAI']).transfer(pair.strategy(), borrowAmount);
    }

    // 3. swap DAI into LP
    (
      uint256 amountLeft0,
      uint256 amountLeft1,
      uint256 liquidity
    ) = UniswapV2Strategy(pair.strategy()).swap(
        tokens['DAI'],
        pool,
        borrowAmount
      );

    // 4. handle addLiquidity remaining tokens
    _handleTokenLeft(pool, vault, owner, pair.token0(), amountLeft0);
    _handleTokenLeft(pool, vault, owner, pair.token1(), amountLeft1);

    // 5. allocate LP
    if (pairAvailable > 0) {
      IUniswapV2ERC20(pool).transferFrom(
        address(pair),
        address(this),
        pairAvailable
      );
    }
    _allocate(pool, vault, liquidity + pairAvailable);

    // 6. draw DAI to payback PairVault / flashLender
    _draw(pool, vault, borrowAmount);
    if (lender != address(maker.flashLender)) {
      IERC20(tokens['DAI']).transfer(lender, borrowAmount);
    }
  }

  function _handleTokenLeft(
    address _pool,
    uint256 _vault,
    address _owner,
    address _token,
    uint256 _amountLeft
  ) internal {
    if (_amountLeft == 0) {
      return;
    }

    IPairVault pair = IPairVault(collaterals[_pool].pair);
    UniswapV2Strategy strategy = UniswapV2Strategy(pair.strategy());
    uint256 dust = uint256(pair.data(_token, 'dust'));

    if (_amountLeft > dust) {
      IERC20(_token).approve(address(strategy), _amountLeft);
      (, , uint256 amountOut) = strategy.swap(
        _token,
        tokens['DAI'],
        _amountLeft
      );
      _payback(_pool, _vault, amountOut);
    } else if (_amountLeft > 0) {
      IERC20(_token).transfer(_owner, _amountLeft);
    }
  }

  function _decreaseCollateral(
    address _pool,
    uint256 _vault,
    uint256 _target
  ) internal {
    require(_target >= 0, 'invalid-target');

    console.log('\n[Leverage:raise-down] _vault', _vault, '_target', _target);
    uint256[] memory stats = check(_vault);
    UniswapV2Strategy strategy = UniswapV2Strategy(
      IPairVault(collaterals[_pool].pair).strategy()
    );

    uint256 freeAmount;
    uint256 daiAmount;
    while (stats[0] > _target) {
      console.log('\n[Leverage:raise-down] --- loop starts ---');

      // 1. Draw required collateral
      freeAmount = _drawCollateral(_pool, _vault, _target);

      // 2. Remove liquidity from UniV2 & Swap token0 & token1 to DAI
      IUniswapV2ERC20(_pool).transfer(address(strategy), freeAmount);
      (, , daiAmount) = strategy.swap(_pool, tokens['DAI'], freeAmount);

      // 3. Payback DAI
      _payback(_pool, _vault, daiAmount);
      stats = check(_vault);
    }
    console.log('[Leverage:raise-down] --- loop ends ---');
  }

  function _drawCollateral(
    address _pool,
    uint256 _vault,
    uint256 _target
  ) internal returns (uint256 freeAmount) {
    Collateral memory col = collaterals[_pool];
    uint256[] memory stats = check(_vault);
    uint256 collateral = stats[0];
    (, uint256 art) = maker.vat.urns(col.ilk, col.urn);
    (, uint256 rate, uint256 spot, , ) = maker.vat.ilks(col.ilk);
    freeAmount = (collateral * spot - (rate * art)) / spot;
    if (collateral - freeAmount < _target) {
      freeAmount = collateral - _target;
    }
    freeAmount = _free(_pool, _vault, freeAmount);
  }

  function _getBorrowAmt(
    address _urn,
    bytes32 _ilk,
    uint256 _amt
  ) internal returns (int256 dart) {
    uint256 ray = 10**27;
    uint256 rate = maker.mcdJug.drip(_ilk);
    uint256 dai = maker.vat.dai(_urn);
    if (dai < _amt * ray) {
      dart = _toInt((_amt * ray - dai) / rate);
      dart = (uint256(dart) * rate) < (_amt * ray) ? dart + 1 : dart;
    }
  }

  function _toInt(uint256 x) internal pure returns (int256 y) {
    y = int256(x);
    require(y >= 0, 'int-overflow');
  }

  function _getWipeAmt(
    uint256 _amt,
    address _urn,
    bytes32 _ilk
  ) internal view returns (int256 dart) {
    (, uint256 rate, , , ) = maker.vat.ilks(_ilk);
    (, uint256 art) = maker.vat.urns(_ilk, _urn);
    dart = _toInt(_amt / rate);
    dart = uint256(dart) <= art ? -dart : -_toInt(art);
  }

  function _getVaultDebt(uint256 _vault) internal view returns (uint256 wad) {
    Collateral memory col = collaterals[vaultToCollateral[_vault]];
    (, uint256 rate, , , ) = maker.vat.ilks(col.ilk);
    (, uint256 art) = maker.vat.urns(col.ilk, col.urn);
    uint256 dai = maker.vat.dai(col.urn);
    uint256 ray = 10**27;
    uint256 rad = art * rate - dai;
    wad = rad / ray;
    wad = wad * ray < rad ? wad + 1 : wad;
  }

  function _calcInfo(
    address _pool,
    uint256 _amount0,
    uint256 _amount1
  ) internal view returns (bytes16 result) {
    uint64 info0;
    uint64 info1;
    if (_amount0 > 0) {
      info0 = uint64(
        _amount0 / 10**(IERC20Metadata(tokens['DAI']).decimals() - 6)
      );
    }
    if (_amount1 > 0) {
      info1 = uint64(
        _amount1 / 10**(IERC20Metadata(tokens['DAI']).decimals() - 6)
      );
    }
    result = IPairVault(collaterals[_pool].pair).encodeInfo(info0, info1);
  }

  // --- Maker flashLender Methods ---
  function _flashBorrow(uint256 _amount, bytes memory _data) internal {
    address DAI = tokens['DAI'];
    require(maker.flashLender.flashFee(DAI, _amount) == 0, 'non-zero-fee');
    IERC20(DAI).approve(address(maker.flashLender), _amount);
    maker.flashLender.flashLoan(
      IERC3156FlashBorrower(address(this)),
      DAI,
      _amount,
      _data
    );
  }

  function onFlashLoan(
    address _initiator,
    address _token,
    uint256 _amount,
    uint256, // _fee
    bytes calldata _data
  ) external returns (bytes32) {
    require(msg.sender == address(maker.flashLender), 'untrusted-lender');
    require(_initiator == address(this), 'untrusted-initiator');
    require(
      IERC20(_token).balanceOf(address(this)) >= _amount,
      'invalid-balance'
    );
    _performRaiseUp(_data);
    return keccak256('ERC3156FlashBorrower.onFlashLoan');
  }
}
