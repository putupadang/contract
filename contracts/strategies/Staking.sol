// SPDX-License-Identifier: MIT
// Copyright (c) 2022 taylor@decouple.co

pragma solidity ^0.8.13;

import 'hardhat/console.sol';

import '@openzeppelin/contracts/token/ERC20/extensions/IERC20Metadata.sol';
import '../interfaces/IStrategy.sol';

interface IPairVault {
  function pool() external view returns (address);

  function strategy() external view returns (address);

  function data(address, string calldata) external view returns (bytes32);

  function positions(address, address)
    external
    view
    returns (
      uint256 amount,
      uint256 debt,
      uint32 blocktime,
      uint256 available,
      bytes16 info
    );

  function popStack(address) external;

  function encodeInfo(uint64, uint64) external pure returns (bytes16 result);

  function decodeInfo(bytes16) external pure returns (uint64, uint64);
}

interface ILeverage {
  function draw(
    address,
    uint256,
    uint256
  ) external returns (bytes[] memory);

  function payback(
    address,
    uint256,
    uint256
  ) external returns (bytes[] memory);
}

interface IUniswapV2Strategy {
  function swapTokenToEth(address, uint256) external returns (uint256);

  function quoteDaiToStEth(uint256) external view returns (uint256);

  function swap(
    address,
    address,
    uint256
  )
    external
    returns (
      uint256,
      uint256,
      uint256
    );
}

interface IWstETH {
  function unwrap(uint256 _wstETHAmount) external returns (uint256);

  function getStETHByWstETH(uint256 _wstETHAmount)
    external
    view
    returns (uint256);
}

contract Staking is IStrategy {
  string public override name = 'Staking:ETH2.0';
  bytes4 public override selector =
    bytes4(keccak256(bytes('Staking(address,address)')));

  mapping(bytes32 => address) internal tokens;

  struct StrategyStats {
    address strategy;
    int256 amount;
    int256 available;
    int256 debt;
    bytes16 info;
  }

  constructor() {}

  function initialize(address _pool, address _null)
    external
    returns (bytes32)
  {}

  function setTokens(
    bytes32[] calldata _nameList,
    address[] calldata _addressList
  ) external {
    require(_nameList.length == _addressList.length, 'invalid-token-inputs');
    for (uint8 i = 0; i < _nameList.length; i++) {
      tokens[_nameList[i]] = _addressList[i];
    }
  }

  // --- External Methods ---

  function allocate(
    address _leverage,
    uint256 _amount,
    uint256
  ) public returns (bytes[] memory) {
    console.log('[Staking:allocate] _amount', _amount);
    // 1. Draw DAI from Leverage
    address pool = IPairVault(msg.sender).pool();
    uint256 vault = uint256(IPairVault(msg.sender).data(_leverage, 'vault'));
    bytes[] memory leverageResult = ILeverage(_leverage).draw(
      pool,
      vault,
      _amount
    );

    // 2. Swap DAI to ETH
    IERC20(tokens['DAI']).approve(IPairVault(msg.sender).strategy(), _amount);
    uint256 ethAmount = IUniswapV2Strategy(IPairVault(msg.sender).strategy())
      .swapTokenToEth(tokens['DAI'], _amount);

    // 3. Send ETH to wstETH contract
    (bool success, ) = tokens['wstETH'].call{ value: ethAmount }(new bytes(0));
    require(success, 'staking-transfer-eth-failed');
    uint256 wstEthBalance = IERC20(tokens['wstETH']).balanceOf(address(this));
    require(wstEthBalance > 0, 'staking-wstETH-failed');
    console.log('[Staking:allocate] ETH -> wstETH', ethAmount, wstEthBalance);

    // 4. Send wstETH to PairVault
    IERC20(tokens['wstETH']).transfer(msg.sender, wstEthBalance);

    // Position updates
    bytes[] memory result = new bytes[](2);
    bytes16 info = _calcInfo(wstEthBalance, 0);
    result[0] = abi.encode(address(this), wstEthBalance, 0, _amount, info);
    result[1] = leverageResult[0];
    return result;
  }

  function free(
    address _leverage,
    uint256 _amount,
    uint256
  ) public returns (bytes[] memory result) {
    console.log('[Staking:free] _leverage, _amount', _leverage, _amount);

    // 1. Unwrap wstETH to stETH
    IERC20(tokens['wstETH']).transferFrom(msg.sender, address(this), _amount);
    uint256 stEthAmount = IWstETH(tokens['wstETH']).unwrap(_amount);
    require(stEthAmount > 0, 'invalid-stETH-amount');
    console.log('[Staking:free] stEthAmount', stEthAmount);

    // 2. Swap stETH to DAI
    address uniV2Strategy = IPairVault(msg.sender).strategy();
    IERC20(tokens['stETH']).approve(uniV2Strategy, stEthAmount);
    (, , uint256 daiAmount) = IUniswapV2Strategy(uniV2Strategy).swap(
      tokens['stETH'],
      tokens['DAI'],
      stEthAmount
    );
    IERC20(tokens['DAI']).transferFrom(uniV2Strategy, address(this), daiAmount);
    console.log('[Staking:free] daiAmount', daiAmount);

    // 3. Payback DAI to Leverage
    address pool = IPairVault(msg.sender).pool();
    uint256 vault = uint256(IPairVault(msg.sender).data(_leverage, 'vault'));
    IERC20(tokens['DAI']).approve(_leverage, daiAmount);
    bytes[] memory leverageResult = ILeverage(_leverage).payback(
      pool,
      vault,
      daiAmount
    );

    // Position updates
    result = new bytes[](2);
    bytes16 info = _calcInfo(0, stEthAmount);
    result[0] = abi.encode(
      address(this),
      -int256(_amount),
      0,
      -int256(daiAmount),
      info
    );
    result[1] = leverageResult[0];
  }

  function fold(
    address _leverage,
    uint256,
    address _owner
  ) external returns (bytes[] memory result) {
    console.log('[Staking:fold] _leverage, _owner', _leverage, _owner);
    (uint256 amount, uint256 debt, , , ) = IPairVault(msg.sender).positions(
      _owner,
      address(this)
    );

    // 1. free all wstETH & payback to Leverage
    if (amount > 0) {
      result = free(_leverage, amount, 0);
      result[0] = abi.encode(address(this), 0, 0, 0, 0);
    } else if (debt > 0) {
      result = new bytes[](1);
      result[0] = abi.encode(address(this), 0, 0, 0, 0);
    }

    // 2. popStack
    IPairVault(msg.sender).popStack(_owner);

    return result;
  }

  function raise(
    address _leverage,
    uint256 _amount,
    uint256,
    address _owner
  ) external returns (bytes[] memory result) {
    (uint256 amount, uint256 debt0, , , ) = IPairVault(msg.sender).positions(
      _owner,
      address(this)
    );
    console.log(
      '[Staking:raise] _leverage, _amount, _owner',
      _leverage,
      _amount,
      _owner
    );
    console.log('[Staking:raise] amount', amount);
    console.log('[Staking:raise] debt0', debt0);

    if (_amount > debt0) {
      // raise up
      result = allocate(_leverage, _amount - debt0, 0);
    } else if (_amount < debt0) {
      // raise down
      address uniV2Strategy = IPairVault(msg.sender).strategy();
      uint256 amountIn = IUniswapV2Strategy(uniV2Strategy).quoteDaiToStEth(
        debt0 - _amount
      );

      result = free(_leverage, amountIn, 0);
      (, , , int256 debt1, ) = abi.decode(
        result[0],
        (address, int256, int256, int256, int256)
      );

      if (debt1 < 0 && debt0 < _amount + uint256(-debt1)) {
        bytes[] memory result1 = allocate(
          _leverage,
          _amount + uint256(-debt1) - debt0,
          0
        );

        result = _mergeBytes(result, result1);
      }
    }
  }

  function _mergeBytes(bytes[] memory result0, bytes[] memory result1)
    internal
    view
    returns (bytes[] memory)
  {
    for (uint8 i = 0; i < result0.length; i++) {
      StrategyStats memory stats0;
      StrategyStats memory stats1;
      (
        stats0.strategy,
        stats0.amount,
        stats0.available,
        stats0.debt,
        stats0.info
      ) = abi.decode(result0[i], (address, int256, int256, int256, bytes16));
      (
        stats1.strategy,
        stats1.amount,
        stats1.available,
        stats1.debt,
        stats1.info
      ) = abi.decode(result1[i], (address, int256, int256, int256, bytes16));

      (uint64 infoA0, uint64 infoA1) = IPairVault(msg.sender).decodeInfo(
        stats0.info
      );
      (uint64 infoB0, uint64 infoB1) = IPairVault(msg.sender).decodeInfo(
        stats1.info
      );
      bytes16 info = IPairVault(msg.sender).encodeInfo(
        infoA0 + infoB0,
        infoA1 + infoB1
      );

      result0[i] = abi.encode(
        stats0.strategy,
        stats0.amount + stats1.amount,
        stats0.available + stats1.available,
        stats0.debt + stats1.debt,
        info
      );
    }
    return result0;
  }

  function payback(
    address _pool,
    uint256 _vault,
    uint256 _amount
  ) external returns (bytes[] memory) {}

  function draw(
    address _pool,
    uint256 _vault,
    uint256 _amount
  ) external returns (bytes[] memory) {}

  function check(uint256 _vault)
    public
    view
    returns (uint256[] memory status)
  {}

  // --- Public Methods ---

  // --- Public Helpers ---

  // --- Internal Helpers ---

  function _calcInfo(uint256 _amount0, uint256 _amount1)
    internal
    view
    returns (bytes16 result)
  {
    uint64 info0;
    uint64 info1;
    if (_amount0 > 0) {
      uint256 stEthAmount = IWstETH(tokens['wstETH']).getStETHByWstETH(
        _amount0
      );
      info0 = uint64(
        stEthAmount / 10**(IERC20Metadata(tokens['stETH']).decimals() - 6)
      );
    }
    if (_amount1 > 0) {
      info1 = uint64(
        _amount1 / 10**(IERC20Metadata(tokens['stETH']).decimals() - 6)
      );
    }
    result = IPairVault(msg.sender).encodeInfo(info0, info1);
  }

  receive() external payable {}

  fallback() external payable {}
}
