// SPDX-License-Identifier: MIT
// Copyright (c) 2022 taylor@decouple.co

pragma solidity ^0.8.13;

import '@openzeppelin/contracts/token/ERC20/extensions/IERC20Metadata.sol';

// all states, positions, deposit, withdraws
// daily compound, weekly settlement.
contract BaseVault {
  address public token0;
  address public token1;
  address public tokenQuote;

  // All user positoins

  // EMV storage: 256bits, 32 bytes per slot
  // positions[owner][default] => position (amount, debt, available, blockTime)
  // positions[owner][leverage] => position (amount, debt, available, blockTime)
  // positions[owner][anchor] => position (amount, debt, available, blockTime)

  // Position info stored for each user's position max 2 slots
  struct Position {
    // deposit of the position owner token0/token1
    // the amount of liquidity LP token owned by this position
    uint112 amount;
    // debt
    uint112 debt;
    // block timestamp in second
    uint32 blocktime;
    // second slot, fee earned, available balance for withdraw
    uint128 available;
    bytes16 info;
  }

  event Deposit(
    address _owner,
    uint256 _amount0,
    uint256 _amount1,
    bytes _result
  );
  event Withdraw(
    address _owner,
    uint256 _amount0,
    uint256 _amount1,
    bytes _result
  );
  event Invoke(address _owner, address _strategy, bytes _data, bytes _result);
  event PositionUpdate(address _owner, address _strategy, Position _position);

  // @positions
  mapping(address => mapping(address => Position)) public positions;

  // function positoins(_owner,_strategy) external view returns (tuple of struct) is generated automatically

  // strategy metadata
}
