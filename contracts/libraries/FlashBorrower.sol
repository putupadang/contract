// SPDX-License-Identifier: MIT
pragma solidity ^0.8.13;

import '../interfaces/IERC20.sol';
import '../interfaces/IERC3156FlashBorrower.sol';
import '../interfaces/IERC3156FlashLender.sol';

contract FlashBorrower is IERC3156FlashBorrower {
  enum Action {
    NORMAL,
    OTHER
  }

  IERC3156FlashLender public lender;

  constructor(IERC3156FlashLender _lender) {
    lender = _lender;
  }

  /// @dev ERC-3156 Flash loan callback
  function onFlashLoan(
    address _initiator,
    address _token,
    uint256 _amount,
    uint256 _fee,
    bytes calldata _data
  ) external override returns (bytes32) {
    require(msg.sender == address(lender), 'FlashBorrower/untrusted-lender');
    require(_initiator == address(this), 'FlashBorrower/untrusted-initiator');
    Action action = abi.decode(_data, (Action));
    if (action == Action.NORMAL) {
      require(IERC20(_token).balanceOf(address(this)) >= _amount);
      // make a profitable trade here
      IERC20(_token).transfer(_initiator, _amount + _fee);
    } else if (action == Action.OTHER) {
      // do another
    }
    return keccak256('ERC3156FlashBorrower.onFlashLoan');
  }

  /// @dev Initiate a flash loan
  function flashBorrow(address _token, uint256 _amount) public {
    bytes memory data = abi.encode(Action.NORMAL);
    uint256 _allowance = IERC20(_token).allowance(
      address(this),
      address(lender)
    );
    uint256 _fee = lender.flashFee(_token, _amount);
    uint256 _repayment = _amount + _fee;
    IERC20(_token).approve(address(lender), _allowance + _repayment);
    lender.flashLoan(this, _token, _amount, data);
  }
}
