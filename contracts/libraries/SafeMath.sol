// SPDX-License-Identifier: UNLICENSED

// DO NOT USE: Solidity version 0.8+ has safe math overflow as default
// it allows solidity compiler compatibility when include UniswapV2Library
// see https://hackmd.io/@fvictorio/a-hardhat-faq#The-project-cannot-be-compiled-HH606

pragma solidity >=0.6.6;

// a library for performing overflow-safe math, courtesy of DappHub (https://github.com/dapphub/ds-math)

library SafeMath {
    function add(uint x, uint y) internal pure returns (uint z) {
        require((z = x + y) >= x, 'ds-math-add-overflow');
    }

    function sub(uint x, uint y) internal pure returns (uint z) {
        require((z = x - y) <= x, 'ds-math-sub-underflow');
    }

    function mul(uint x, uint y) internal pure returns (uint z) {
        require(y == 0 || (z = x * y) / y == x, 'ds-math-mul-overflow');
    }
}
