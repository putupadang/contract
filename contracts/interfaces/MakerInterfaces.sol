// SPDX-License-Identifier: MIT
pragma solidity ^0.8.13;

interface IManager {
  function urns(uint256) external view returns (address);

  function vat() external view returns (address);

  function open(bytes32, address) external returns (uint256);

  function frob(
    uint256,
    int256,
    int256
  ) external;

  function flux(
    uint256,
    address,
    uint256
  ) external;

  function move(
    uint256,
    address,
    uint256
  ) external;
}

interface IToken {
  function approve(address, uint256) external;

  function transfer(address, uint256) external;

  function transferFrom(
    address,
    address,
    uint256
  ) external;

  function deposit() external payable;

  function withdraw(uint256) external;

  function balanceOf(address) external view returns (uint256);

  function decimals() external view returns (uint256);

  function totalSupply() external view returns (uint256);
}

interface ITokenJoin {
  function dec() external returns (uint256);

  function gem() external returns (IToken);

  function join(address, uint256) external payable;

  function exit(address, uint256) external;
}

interface IVat {
  function can(address, address) external view returns (uint256);

  function ilks(bytes32)
    external
    view
    returns (
      uint256,
      uint256,
      uint256,
      uint256,
      uint256
    );

  function dai(address) external view returns (uint256);

  function urns(bytes32, address) external view returns (uint256, uint256);

  function frob(
    bytes32,
    address,
    address,
    address,
    int256,
    int256
  ) external;

  function hope(address) external;

  function move(
    address,
    address,
    uint256
  ) external;

  function gem(bytes32, address) external view returns (uint256);
}

interface IJug {
  function drip(bytes32) external returns (uint256);
}

interface IDaiJoin {
  function vat() external returns (IVat);

  function dai() external returns (IToken);

  function join(address, uint256) external payable;

  function exit(address, uint256) external;
}

interface ISpot {
  function par() external view returns (uint256);

  function ilks(bytes32) external view returns (address pip, uint256 mat);
}
