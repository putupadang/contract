// SPDX-License-Identifier: MIT
pragma solidity ^0.8.13;

interface IStrategy {
  event Allocate(address owner, uint256 _amount0, uint256 _amount1);

  function selector() external view returns (bytes4);

  function name() external view returns (string memory);

  function initialize(address, address) external returns (bytes32);

  function allocate(
    address,
    uint256,
    uint256
  ) external returns (bytes[] memory);

  function free(
    address,
    uint256,
    uint256
  ) external returns (bytes[] memory);

  function draw(
    address,
    uint256,
    uint256
  ) external returns (bytes[] memory);

  function payback(
    address,
    uint256,
    uint256
  ) external returns (bytes[] memory);

  function raise(
    address,
    uint256,
    uint256,
    address
  ) external returns (bytes[] memory);

  function fold(
    address,
    uint256,
    address
  ) external returns (bytes[] memory);

  function check(uint256) external view returns (uint256[] memory);
}
