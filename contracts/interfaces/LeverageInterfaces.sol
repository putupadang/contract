// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

interface IUniswapV2ERC20 {
  function approve(address spender, uint256 value) external returns (bool);

  function transfer(address to, uint256 value) external returns (bool);

  function transferFrom(
    address from,
    address to,
    uint256 value
  ) external returns (bool);
}

interface IPairVault {
  function token0() external view returns (address);

  function token1() external view returns (address);

  function strategy() external view returns (address);

  function positions(address, address)
    external
    view
    returns (
      uint256 amount,
      uint256 debt,
      uint32 blocktime,
      uint256 available,
      bytes16 info
    );

  function data(address, string calldata) external returns (bytes32);

  function popStack(address) external;

  function encodeInfo(uint64, uint64) external pure returns (bytes16 result);

  function decodeInfo(bytes16) external pure returns (uint64, uint64);
}

interface UniswapV2Strategy {
  function swap(
    address,
    address,
    uint256
  )
    external
    returns (
      uint256,
      uint256,
      uint256
    );
}
