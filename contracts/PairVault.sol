// SPDX-License-Identifier: MIT
// Copyright (c) 2022 taylor@decouple.co

pragma solidity ^0.8.13;

import '@uniswap/v2-core/contracts/interfaces/IUniswapV2Factory.sol';
import '@uniswap/v2-periphery/contracts/interfaces/IUniswapV2Router02.sol';
import '@uniswap/v2-core/contracts/interfaces/IUniswapV2ERC20.sol';

import './BaseVault.sol';
import './interfaces/IStrategy.sol';

// Local require for solidity 0.8+ compatibility
import './libraries/UniswapV2Library.sol';
import 'hardhat/console.sol';

// TODO:
// - add contract owner & modifiers

interface ILeverage {
  function raise(
    address _pool,
    uint256 _vault,
    uint256 _targetValue,
    address _owner
  ) external returns (bytes[] memory);

  function foldAll(address _pool, uint256 _vault) external;
}

// Becareful with state variables Storage slot ordering with inheritance
// might need for upgradable proxy
contract PairVault is BaseVault {
  IStrategy public strategy; // Uniswap V2 LP
  address public router;

  // LP pool (UniswapV2Pair, UniswapV3Pool, CurveLP, SLP)
  address public pool;

  struct Strategy {
    bytes4 selector;
    address strategy;
  }

  struct StrategyStats {
    address strategy;
    int256 amount;
    int256 available;
    int256 debt;
    bytes16 info;
  }

  // max 255 strategies
  mapping(uint8 => Strategy) public addons;
  mapping(address => mapping(string => bytes32)) public data;

  mapping(bytes32 => address) public tokens;

  enum EStrategy {
    None,
    Allocate,
    Draw,
    Payback,
    Free,
    Raise,
    Fold
  }
  mapping(bytes4 => EStrategy) public methods;

  Position public position;

  address public owner;

  constructor(
    address _token0,
    address _token1,
    address _tokenQuote,
    address _strategy
  ) {
    require(_token0 < _token1, 'invalid-token0-greater');
    owner = msg.sender;

    token0 = _token0;
    token1 = _token1;
    tokenQuote = _tokenQuote;
    // Default already deployed on chain (UniswapV2)
    strategy = IStrategy(_strategy);

    pool = address(bytes20(strategy.initialize(_token0, _token1)));
  }

  function setTokens(
    bytes32[] calldata _nameList,
    address[] calldata _addressList
  ) external onlyOwner {
    require(_nameList.length == _addressList.length, 'invalid-token-inputs');
    for (uint8 i = 0; i < _nameList.length; i++) {
      tokens[_nameList[i]] = _addressList[i];
    }
  }

  modifier onlyOwner() {
    require(msg.sender == owner, 'not-authorized');
    _;
  }

  function setEStrategies(
    bytes4[] calldata methodSigs,
    EStrategy[] calldata values
  ) external onlyOwner {
    for (uint8 i = 0; i < methodSigs.length; i++) {
      bytes4 methodSig = methodSigs[i];
      methods[methodSig] = values[i];
    }
  }

  function getMethodSig(string memory _input)
    public
    pure
    returns (bytes4 methodSig)
  {
    methodSig = bytes4(keccak256(bytes(_input)));
  }

  // vault position deposit withdraw features
  function deposit(uint256 _amount0, uint256 _amount1) external {
    console.log('[PairVault:deposit] _amount0-1', _amount0, _amount1);

    uint256 before0 = IERC20(token0).balanceOf(address(this));
    uint256 before1 = IERC20(token1).balanceOf(address(this));

    // ERC20#_transfer has from balance check already

    IERC20(token0).transferFrom(msg.sender, address(this), _amount0);
    IERC20(token1).transferFrom(msg.sender, address(this), _amount1);

    uint256 after0 = IERC20(token0).balanceOf(address(this));
    uint256 after1 = IERC20(token1).balanceOf(address(this));

    require((after0 + _amount0) >= before0, 'invalid-after-amount0');
    require((after1 + _amount1) >= before1, 'invalid-after-amount1');

    // default strategy allocate
    bytes memory result = allocate(_amount0, _amount1);
    emit Deposit(msg.sender, _amount0, _amount1, result);
  }

  function withdraw(uint256 _amount0, uint256 _amount1) external {
    console.log('[PairVault:withdraw] _amount0-1', _amount0, _amount1);
    bytes memory result = free(_amount0, _amount1);
    emit Withdraw(msg.sender, _amount0, _amount1, result);
  }

  // returns amount of liquidity token
  // Add liquidity: https://gist.github.com/QuantSoldier/8e0e148c0024df47bccc006560b3f615
  function allocate(uint256 _amount0, uint256 _amount1)
    public
    returns (bytes memory result)
  {
    console.log('[PairVault:allocate] _amount0-1', _amount0, _amount1);
    Position storage pos = positions[msg.sender][address(this)];

    // Increase liquidity
    // Default UniswapV2 Strategy allocate
    IERC20(token0).transfer(address(strategy), _amount0);
    IERC20(token1).transfer(address(strategy), _amount1);

    bytes[] memory resultList = strategy.allocate(pool, _amount0, _amount1);
    (, uint256 liquidity, , , ) = abi.decode(
      resultList[0],
      (address, uint256, uint256, uint256, uint256)
    );

    uint256 amountLeft0 = IERC20(token0).allowance(
      address(strategy),
      address(this)
    );
    uint256 amountLeft1 = IERC20(token1).allowance(
      address(strategy),
      address(this)
    );
    if (amountLeft0 > 0) {
      IERC20(token0).transferFrom(
        address(strategy),
        address(this),
        amountLeft0
      );
      IERC20(token0).transfer(msg.sender, amountLeft0);
    }
    if (amountLeft1 > 0) {
      IERC20(token1).transferFrom(
        address(strategy),
        address(this),
        amountLeft1
      );
      IERC20(token1).transfer(msg.sender, amountLeft1);
    }
    console.log('[PairVault:allocate] amountLeft0-1', amountLeft0, amountLeft1);

    // Update existing position
    pos.amount += uint112(liquidity);
    pos.available += uint128(liquidity);
    pos.blocktime = uint32(block.timestamp);
    pos.info = _calcInfo(pos.info, liquidity, 0);

    emit PositionUpdate(msg.sender, address(this), pos);

    position.amount += uint112(liquidity);
    position.blocktime = uint32(block.timestamp);

    result = abi.encode(address(this), liquidity, liquidity, 0, pos.info);
  }

  function _calcInfo(
    bytes16 _info,
    uint256 _amount0,
    uint256 _amount1
  ) internal view returns (bytes16 result) {
    // console.log('[PairVault:_calcInfo] _amount0, _amount1', _amount0, _amount1);
    (uint64 info0, uint64 info1) = decodeInfo(_info);
    (uint112 reserve0, uint112 reserve1, uint256 totalSupply) = getReserves();
    uint256 value;
    if (_amount0 > 0) {
      value =
        (_amount0 * (tokenQuote == token0 ? reserve0 : reserve1) * 2) /
        totalSupply;
      info0 += uint64(value / 10**(IERC20Metadata(tokenQuote).decimals() - 6));
    }
    if (_amount1 > 0) {
      value =
        (_amount1 * (tokenQuote == token0 ? reserve0 : reserve1) * 2) /
        totalSupply;
      info1 += uint64(value / 10**(IERC20Metadata(tokenQuote).decimals() - 6));
    }
    result = encodeInfo(info0, info1);
  }

  function encodeInfo(uint64 _amount0, uint64 _amount1)
    public
    pure
    returns (bytes16 result)
  {
    result =
      (bytes16(uint128(_amount0)) << (16 * 4)) |
      bytes16(uint128(_amount1));
  }

  function decodeInfo(bytes16 _info)
    public
    pure
    returns (uint64 amount0, uint64 amount1)
  {
    amount0 = uint64(bytes8(_info));
    amount1 = uint64(bytes8(_info << (16 * 4)));
  }

  function free(uint256 _amount0, uint256 _amount1)
    public
    returns (bytes memory result)
  {
    Position storage pos = positions[msg.sender][address(this)];
    require(pos.amount > 0, 'invalid-pos-amount');

    uint256 balanceBefore0 = IERC20(token0).balanceOf(msg.sender);
    uint256 balanceBefore1 = IERC20(token1).balanceOf(msg.sender);

    // UniswapV2Pair, UNI-V2
    IERC20(pool).approve(address(strategy), pos.amount);
    bytes[] memory resultList = strategy.free(pool, _amount0, _amount1);
    (uint256 amountOut0, uint256 amountOut1, uint256 liquidity) = abi.decode(
      resultList[0],
      (uint256, uint256, uint256)
    );

    if (amountOut0 > 0) {
      IERC20(token0).transfer(msg.sender, amountOut0);
    }
    if (amountOut1 > 0) {
      IERC20(token1).transfer(msg.sender, amountOut1);
    }

    uint256 balanceAfter0 = IERC20(token0).balanceOf(msg.sender);
    uint256 balanceAfter1 = IERC20(token1).balanceOf(msg.sender);

    require(
      (balanceBefore0 + amountOut0) >= balanceAfter0,
      'invalid-balance-after-amount0'
    );
    require(
      (balanceBefore1 + amountOut1) >= balanceAfter1,
      'invalid-balance-after-amount1'
    );

    pos.amount -= uint112(liquidity);
    pos.available -= uint128(liquidity);
    pos.blocktime = uint32(block.timestamp);
    pos.info = _calcInfo(pos.info, 0, liquidity);

    if (position.amount >= liquidity) {
      position.amount -= uint112(liquidity);
    } else {
      position.amount = 0;
    }
    position.blocktime = uint32(block.timestamp);

    result = abi.encode(
      address(this),
      -int256(liquidity),
      -int256(liquidity),
      0,
      pos.info
    );
  }

  function getReserves()
    public
    view
    returns (
      uint112 reserve0,
      uint112 reserve1,
      uint256 totalSupply
    )
  {
    totalSupply = IUniswapV2Pair(pool).totalSupply();
    (reserve0, reserve1, ) = IUniswapV2Pair(pool).getReserves();
  }

  /*
      Addon related
  */

  // User only methods

  // user invoke strategy method
  // PairVault.invoke(address strategy, bytes4 method, bytes calldata data)
  //
  // stack = stacks[msg.sender]
  // stack contains up to 8 strategy selectors in bytes4, read/write is always 1 storage slot

  // user strategy stack
  //
  // Strategy selector is generated using Strategy(param,param) where params are what initialize method requires
  //
  //  UniswapV2(address,address) > UniswapV2.initialize(address token0, address token1)
  //  Leverage(address,address) > UniswapV2.initialize(address _pool,address _null)
  //  Staking(address,address) > Staking.initialize(address, address)
  //
  // pairVault.setStack([
  //   '0xf1ce09b1', '0x7bca2456', '0x4243da47', '0x00000000', '0x00000000', '0x00000000', '0x00000000', '0x00000000'
  // ])
  //
  // admin could set stack on behave of a user
  // pairVault.setStack(userAddress, [
  //   '0xf1ce09b1', '0x7bca2456', '0x4243da47', '0x00000000', '0x00000000', '0x00000000', '0x00000000', '0x00000000'
  // ])

  // addons = [address(Leverage), address(Staking)]
  // addons[selector] = address(Leverage)
  // addons[selector] = address(Staking)

  mapping(address => bytes32) public stacks;

  function getStack() public view returns (bytes4[] memory selectors) {
    return _getStack(msg.sender);
  }

  function setStack(bytes4[] memory selectors) external view onlyOwner {
    require(selectors.length <= 8, 'invalid-strategy-stack');
  }

  // push 0xd23322a4, 0xd23322a400000000000000000000000000000000000000000000000000000000
  // push 0x46f035e6, 0xd23322a446f035e6000000000000000000000000000000000000000000000000
  // push 0x6b84fcb1, 0xd23322a446f035e66b84fcb10000000000000000000000000000000000000000
  // push 0x6b84fcb1, 0xd23322a446f035e66b84fcb10000000000000000000000000000000000000000

  function use(bytes4 selector) internal {
    bytes4[] memory selectors = new bytes4[](8);
    bytes32 stack = stacks[msg.sender];

    selectors = _getStack(msg.sender);

    console.log('[PairVault:use] new selector:');
    console.logBytes4(selector);
    console.log('[PairVault:use] current selectors length', selectors.length);
    console.log('[PairVault:use] stack before:');
    console.logBytes32(stacks[msg.sender]);

    bool exist = false;
    for (uint8 i = 0; i < selectors.length; i++) {
      if (selectors[i] == selector) {
        exist = true;
      }
    }
    require(!exist, 'duplicated-selector');

    // append 4 bytes pack with new selector
    stacks[msg.sender] = (((bytes32(selector) >> (32 * selectors.length))) |
      stack);

    console.log('[PairVault:use] stack after:');
    console.logBytes32(stacks[msg.sender]);
  }

  function invoke(address _strategy, bytes calldata _data)
    external
    returns (bytes memory result)
  {
    require(_strategy != address(0), 'invalid-address');

    result = _invoke(_strategy, _data);
  }

  function invokeMulti(address _strategy, bytes[] calldata _dataList)
    external
    returns (bytes[] memory results)
  {
    require(_strategy != address(0), 'invalid-address');

    results = new bytes[](_dataList.length);
    for (uint8 i = 0; i < _dataList.length; i++) {
      results[i] = _invoke(_strategy, _dataList[i]);
    }
  }

  function _invoke(address _strategy, bytes calldata _data)
    internal
    returns (bytes memory)
  {
    console.log('[PairVault:invoke] _strategy', _strategy);
    console.log('[PairVault:invoke] _data');
    console.logBytes(_data);

    bytes4 methodSig = bytes4(_data);
    require(methods[methodSig] != EStrategy.None, 'invalid-invoke-method');

    _invokeBeforeHandler(_strategy, methodSig, _data);
    bytes memory result;
    if (
      methods[methodSig] == EStrategy.Raise &&
      IStrategy(_strategy).selector() ==
      bytes4(keccak256(bytes('Leverage(address,address)')))
    ) {
      (
        address _pool,
        uint256 _vault,
        uint256 _targetValue,
        address _owner
      ) = abi.decode(_data[4:], (address, uint256, uint256, address));
      bytes[] memory resultList = ILeverage(_strategy).raise(
        _pool,
        _vault,
        _targetValue,
        _owner
      );
      result = abi.encode(resultList);
    } else {
      bool success;
      (success, result) = _strategy.call(_data);
      require(success, 'invoke-failed');
    }
    _invokeAfterHandler(_strategy, methodSig, result);

    emit Invoke(msg.sender, _strategy, _data, result);

    return result;
  }

  function _addSelector(address _strategy) internal {
    bytes4 selector = IStrategy(_strategy).selector();
    if (!_selectorExisted(selector)) {
      use(selector);
    }
  }

  function _invokeBeforeHandler(
    address _strategy,
    bytes4 _methodSig,
    bytes calldata _data
  ) internal {
    if (methods[_methodSig] == EStrategy.Allocate) {
      console.log('[PairVault:_invokeBeforeHandler] allocate');
      _addSelector(_strategy);
    } else if (methods[_methodSig] == EStrategy.Payback) {
      (, , uint256 amount) = abi.decode(_data[4:], (address, uint256, uint256));
      IERC20(tokens['DAI']).transferFrom(msg.sender, address(this), amount);
    } else if (methods[_methodSig] == EStrategy.Raise) {
      uint8 start = 4 + 32 * 3 + (32 - 20);
      require(address(bytes20(_data[start:])) == msg.sender, 'invalid-owner');
      _addSelector(_strategy);
    } else if (methods[_methodSig] == EStrategy.Fold) {
      uint8 start = 4 + 32 * 2 + (32 - 20);
      require(address(bytes20(_data[start:])) == msg.sender, 'invalid-owner');
    }
  }

  function _invokeAfterHandler(
    address _strategy,
    bytes4 _methodSig,
    bytes memory _result
  ) internal {
    bytes[] memory resultList = abi.decode(_result, (bytes[]));
    StrategyStats memory stats;
    for (uint8 i = 0; i < resultList.length; i++) {
      (
        stats.strategy,
        stats.amount,
        stats.available,
        stats.debt,
        stats.info
      ) = abi.decode(resultList[i], (address, int256, int256, int256, bytes16));
      require(stats.strategy != address(0), 'invalid-address');
      Position storage pos = positions[msg.sender][stats.strategy];

      if (
        stats.strategy == _strategy && methods[_methodSig] == EStrategy.Fold
      ) {
        pos.amount = 0;
        pos.available = 0;
        pos.debt = 0;
        pos.info = 0;
        pos.blocktime = uint32(block.timestamp);

        emit PositionUpdate(msg.sender, stats.strategy, pos);

        continue;
      } else if (
        stats.strategy == _strategy && methods[_methodSig] == EStrategy.Draw
      ) {
        IERC20(tokens['DAI']).transfer(msg.sender, uint256(stats.debt));
      }

      bool hasChanges = false;

      if (stats.amount > 0) {
        pos.amount += uint112(uint256(stats.amount));
        hasChanges = true;
      } else if (stats.amount < 0) {
        pos.amount -= uint112(uint256(-stats.amount));
        hasChanges = true;
      }

      if (stats.available > 0) {
        pos.available += uint128(uint256(stats.available));
        hasChanges = true;
      } else if (stats.available < 0) {
        pos.available -= uint128(uint256(-stats.available));
        hasChanges = true;
      }

      if (stats.debt > 0) {
        pos.debt += uint112(uint256(stats.debt));
        hasChanges = true;
      } else if (stats.debt < 0) {
        if (pos.debt >= uint112(uint256(-stats.debt))) {
          pos.debt -= uint112(uint256(-stats.debt));
        } else {
          pos.debt = 0;
        }
        hasChanges = true;
      }

      (uint64 info0, uint64 info1) = decodeInfo(stats.info);
      if (info0 > 0 || info1 > 0) {
        (uint64 posInfo0, uint64 posInfo1) = decodeInfo(pos.info);
        posInfo0 += info0;
        posInfo1 += info1;
        pos.info = encodeInfo(posInfo0, posInfo1);
        hasChanges = true;
      }

      if (hasChanges) {
        pos.blocktime = uint32(block.timestamp);
        emit PositionUpdate(msg.sender, stats.strategy, pos);
      }
    }
  }

  function _selectorExisted(bytes4 _selector)
    internal
    view
    returns (bool exist)
  {
    bytes4[] memory selectors = _getStack(msg.sender);
    exist = false;
    for (uint8 i = 0; i < selectors.length; i++) {
      if (selectors[i] == _selector) {
        exist = true;
        break;
      }
    }
  }

  // Admin only method
  function register(uint8 index, address _strategy) external onlyOwner {
    Strategy storage addon = addons[index];
    bool isEmpty = addon.strategy == address(0);

    require(
      isEmpty || (!isEmpty && addon.strategy != _strategy),
      'invalid-override-existing-strategy'
    );

    console.log('[PairVault:register] name', IStrategy(_strategy).name());
    console.logBytes4(IStrategy(_strategy).selector());

    addon.strategy = _strategy;
    addon.selector = IStrategy(_strategy).selector();
  }

  // Admin only method
  function approveMax(address _strategy, address _token) external onlyOwner {
    if (
      keccak256(abi.encode(IUniswapV2ERC20(_token).symbol())) ==
      keccak256(abi.encode('UNI-V2'))
    ) {
      IUniswapV2ERC20(_token).approve(_strategy, type(uint256).max);
    } else {
      IERC20(_token).approve(_strategy, type(uint256).max);
    }
  }

  function remove(address _strategy) external {
    bool found;

    for (uint8 i = 0; i < 255; i++) {
      if (addons[i].strategy == _strategy) {
        found = true;
        addons[i] = Strategy(0, address(0));
      }
    }
    require(found, 'invalid-strategy-not-exist');
  }

  function check() public view returns (uint256[] memory stats) {
    stats = new uint256[](4);
    stats[0] = uint256(position.amount);
    stats[1] = IUniswapV2ERC20(pool).balanceOf(address(this));
  }

  function fold(
    address _pool,
    uint256 _vault,
    address _owner
  ) external {}

  // Not implemented
  function raise(
    address,
    uint256,
    uint256
  ) external {}

  function payback() external {}

  function draw(uint256, uint256) external {}

  function popStack(address _owner) external {
    _popStack(_owner);
  }

  function _popStack(address _owner) internal {
    console.log('[PairVault:_popStack]', _owner);
    bytes4[] memory selectors = _getStack(_owner);
    require(selectors.length > 0, 'invalid-selectors');
    uint16 bits = uint16((8 - selectors.length + 1) * 4 * 8);
    stacks[_owner] = (stacks[_owner] >> bits) << bits;
  }

  function _getStack(address _owner) internal view returns (bytes4[] memory) {
    bytes32 stack = stacks[_owner];
    uint8 counter = 0;

    if (uint256(stack) == 0) {
      return new bytes4[](0);
    }

    for (uint8 i = 0; i < 32 / 4; i++) {
      if (bytes4(stack << (4 * 8 * i)) > 0) {
        counter++;
      } else {
        break;
      }
    }

    bytes4[] memory selectors = new bytes4[](counter);
    for (uint8 i = 0; i < counter; i++) {
      selectors[i] = bytes4(stack << (4 * 8 * i));
    }
    return selectors;
  }

  function setData(
    address _strategy,
    string calldata _name,
    bytes32 _value
  ) external onlyOwner {
    data[_strategy][_name] = _value;
  }

  receive() external payable {
    console.log('[PairVault:receive]', msg.value);
  }

  fallback() external payable {
    console.logBytes(msg.data);
  }

  // NOTE: --- Admin test only ---
  function foldAll(
    address _strategy,
    address _pool,
    uint256 _vault
  ) external onlyOwner {
    console.log('[foldAll]', _strategy, _pool, _vault);
    ILeverage(_strategy).foldAll(_pool, _vault);
    IERC20(tokens['DAI']).transfer(
      msg.sender,
      IERC20(tokens['DAI']).balanceOf(address(this))
    );
    IUniswapV2ERC20(_pool).transfer(
      msg.sender,
      IUniswapV2ERC20(_pool).balanceOf(address(this))
    );
    IERC20(tokens['wstETH']).transfer(
      msg.sender,
      IERC20(tokens['wstETH']).balanceOf(address(this))
    );
  }

  function copyPosition(
    address _owner,
    address _from,
    address _to
  ) external onlyOwner {
    Position memory fromPos = positions[_owner][_from];
    Position storage toPos = positions[_owner][_to];

    toPos.amount = fromPos.amount;
    toPos.debt = fromPos.debt;
    toPos.blocktime = fromPos.blocktime;
    toPos.available = fromPos.available;
    toPos.info = fromPos.info;
  }
}
