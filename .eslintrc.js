module.exports = {
  root: true,
  env: {
    browser: false,
    es2021: true,
    mocha: true,
    node: true
  },
  extends: ['eslint:recommended'],
  parserOptions: {
    sourceType: 'module'
  },
  overrides: [
    {
      files: ['hardhat.config.js']
    }
  ],
  globals: { task: true, network: true, ethers: true, hre: true },
  rules: {
    'space-before-function-paren': [2, 'always'],
    'prefer-const': 2,
    'no-prototype-builtins': 0,
    curly: [2, 'all'],
    eqeqeq: [2, 'always']
  }
}
