# Initial Setup

1. run 'npm install'
2. setup .env file based on .env.example

# Start local node with mainnet fork

```sh
# step1. Fork mainnet into local hardhat node
npx hardhat node --fork https://eth-mainnet.alchemyapi.io/v2/<key> --fork-block-number <blocknumber>

# step2. Setup local contracts, balances, deploy, user wallets
npx hardhat setup --network localhost
```

# Useful hardhat cmds

```sh
# node: start a local node
> npx hardhat node

# script: run a script on a specific network
> npx hardhat run scripts/script.js --network localhost|goerli

# task: run a task on a specific network
> npx hardhat task-name --network localhost|goerli

# compile all contracts
> npx hardhat compile

# hardhat console
> npx hardhat console --network localhost|goerli
```

# Useful links

- [Add hardhat local env to Metamask](https://hardhat.org/tutorial/hackathon-boilerplate-project.html#how-to-use-it)
- [Metamask register method names](https://github.com/MetaMask/metamask-docs/blob/1896f19c9abd0343960379b1e71bf1af2c466453/docs/guide/registering-function-names.md)
- [hardhat: impersonate an address](https://mixbytes.io/blog/how-fork-mainnet-testing)
- [Oasis.app](https://oasis.app/)
- [ethersjs-encodeFunctionData](https://ethereum.stackexchange.com/questions/111343/etherjs-equivalent-of-abi-encodewithsignature)
- [ethersjs-encode](https://github.com/ethers-io/ethers.js/issues/211)
- [Why Do We Need Transaction Data](https://blog.mycrypto.com/why-do-we-need-transaction-data-)

# lido Staking links

- [WstETH.sol](https://github.com/lidofinance/lido-dao/blob/master/contracts/0.6.12/WstETH.sol)
- [lido Deployed Contracts](https://docs.lido.fi/deployed-contracts)

# Slither installation

- [slither](https://github.com/crytic/slither)

```sh
pip3 install slither-analyzer
pip3 install solc-select
solc-select install 0.8.13
solc-select use 0.8.13
slither file-path
```

# Maker

## maker formulas

```js
collateral * price / ray = (available + debt) * price / spot
collateral * spot / ray = available + debt
```

## Gas Usage

### allocate + raise 2x multi-call

1. original { gas: 2,272,872 }
2. move approve out of loop { gas: 2,249,916 }
3. use transfer instead of approve & transferFrom in swap { gas: 2,101,817 }
4. increase token dust limit to around $50 for each token,
   to avoid extra swap & payback { gas: 1,874,923 }
5. Borrow DAI from PairVault { gas: 1,150,000 }
   1. transfer DAI from pair vault to leverage: 44,013
   2. swap DAI to LP: 590,000
   3. handle dust: 221,823
   4. allocate LP: 111,376
   5. draw DAI: 138,587
   6. transfer DAI back to pair vault: 4,314

- borrow DAI from PairVault: { gas: 1,156,951 }
- borrow DAI from Maker#flashLender: { gas: 1,376,817 }

### Summary

- allocate { gas: 370,000 }
- raise 2x { gas: 1,500,000 }

### Details

- hardhat console.log { gas: 900 }
- Leverage check { gas: 12,255 }
- Leverage draw { gas: 187,129 -> 107,380 }
- uni swap DAI to LP { gas: 433,768 -> 253,524 }
- Leverage handle dust { gas: 197,661 -> 13,000 }
- Leverage allocate { gas: 89,233 }

- PSM swap DAI to USDC { gas: 144,808 -> 231,508 }
- Uni swap DAI to USDC { gas: 57,740 -> 117,456 }

### Leverage.raise-up

1. draw available debt in DAI
2. swap DAI into LP
3. convert all remaining tokens into DAI and payback
4. allocate LP

- 1st loop: 187000 + 433000 + 197000 + 90000 = 907,000
- 2nd loop: 107000 + 253000 + 13000 + 89000 = 462,000

## Maker Links

- [MakerDAO Deployment Registry](https://chainlog.makerdao.com/)
- [Developer Guides and Tutorials](https://github.com/makerdao/developerguides)
- [Docs](https://docs.makerdao.com/)
- [CDP Manager Guide](https://github.com/makerdao/developerguides/blob/master/vault/cdp-manager-guide/cdp-manager-guide.md)
- [Vault lifecycle with CDP Manager](https://github.com/makerdao/developerguides/blob/master/mcd/mcd-seth/mcd-seth.md)
- [dss-cdp-manager](https://github.com/makerdao/dss-cdp-manager)
- [instadapp](https://github.com/Instadapp/dsa-connectors/blob/main/contracts/mainnet/connectors/makerdao/main.sol)
- [maker addresses](https://chainlog.makerdao.com/api/mainnet/active.json)
- [maker glossary](https://github.com/makerdao/dss/wiki/Glossary#general)
