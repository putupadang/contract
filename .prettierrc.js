module.exports = {
  printWidth: 80,
  tabWidth: 2,
  useTabs: false,
  semi: false,
  singleQuote: true,
  quoteProps: 'as-needed',
  trailingComma: 'none',
  bracketSpacing: true,
  bracketSameLine: false,
  overrides: [
    {
      files: 'contracts/MakerStrategy.sol',
      options: {
        compiler: '0.8.13'
      }
    }
  ]
}
