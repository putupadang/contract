const { ethers: rawEthers } = require('ethers')
const fs = require('fs')
const path = require('path')
const ContractSetup = require('../lib/contract-setup')

task('setup', 'Set up initial contracts & balances').setAction(() => {
  return main()
})

async function main () {
  await hre.run('compile')

  console.log('current network:', network.name)

  let Addresses
  if (network.name === 'goerli') {
    Addresses = require('../static/addresses.json').goerli
  } else {
    Addresses = require('../static/addresses.json').mainnet
  }
  const setup = new ContractSetup({ addresses: Addresses, network })

  const user = new rawEthers.Wallet(process.env.WALLET_PRIVATE_KEY)
  const [deployer] = await ethers.getSigners()
  console.log('deployer:', deployer.address)
  console.log('user:', user.address)

  const tokens = await setup.initTokens([
    { symbol: 'wstETH', decimals: 18 },
    { symbol: 'WETH', decimals: 18 },
    { symbol: 'WBTC', decimals: 8 },
    { symbol: 'USDC', decimals: 6 },
    { symbol: 'DAI', decimals: 18 }
  ])

  const [v2factory, v2router] = await setup.initUniswap()
  console.log('v2factory:', v2factory.address)
  console.log('v2router:', v2router.address)

  const strategy = await setup.initDefaultStrategy(v2router)
  console.log('strategy:', strategy.address)

  const pairSymList = [
    ['WETH', 'USDC'],
    ['WBTC', 'DAI'],
    ['USDC', 'DAI']
  ]

  const pairVaults = await setup.initPairVaults(pairSymList, tokens, strategy)
  const leverage = await setup.initLeverage(pairVaults)
  const staking = await setup.initStaking(pairVaults)

  if (network.name !== 'goerli') {
    console.log('[waiting] deployer balances setups')
    await setup.initDeployerBalance(deployer, tokens, [
      { symbol: 'WETH', amount: 25 },
      { symbol: 'WBTC', amount: 4 },
      { symbol: 'USDC', amount: 10_000 },
      { symbol: 'DAI', amount: 360_000 },
      { symbol: 'wstETH', amount: 0 }
    ])
    console.log('[waiting] user balances setups')
    await setup.initUserBalance(deployer, user, tokens, {
      ETH: 1,
      WETH: 5,
      WBTC: 0.1,
      DAI: 10_000,
      USDC: 10_000
    })
    const vatContract = setup.initVatContract(deployer)
    console.log('[waiting] leverage strategy initial deposit setups')
    await setup.initLeverageDeposit(
      pairVaults,
      strategy,
      leverage,
      vatContract,
      tokens
    )

    await setup.printBalances({ deployer, user }, tokens, pairVaults, leverage)
  }

  console.log('\n--- GlobalData for frontend ---')
  const GlobalData = {
    Addresses: {},
    Tokens: {},
    PairList: []
  }
  const pairs = {}
  for (const key in pairVaults) {
    pairs[key] = {}
    pairs[key]['vault'] = pairVaults[key].address
    pairs[key]['pool'] = await pairVaults[key].pool()
    pairs[key]['token0'] = await pairVaults[key].token0()
    pairs[key]['token1'] = await pairVaults[key].token1()
    pairs[key]['tokenQuote'] = await pairVaults[key].tokenQuote()
  }
  GlobalData.Addresses = {
    v2factory: v2factory.address,
    v2router: v2router.address,
    pairs
  }
  for (const sym in tokens) {
    const token = tokens[sym]
    GlobalData.Tokens[token.address] = {
      address: token.address,
      symbol: sym,
      decimals: await token.decimals()
    }
  }

  GlobalData.PairList.push(
    {
      id: setup.sortedKey(tokens.WETH, tokens.USDC),
      maxVolume: 5_000_000,
      addressA: tokens.WETH.address,
      addressB: tokens.USDC.address,
      info: 'Uniswap V2 LP',
      compounds: ['v2-lp', 'leverage', 'staking']
    },
    {
      id: setup.sortedKey(tokens.WBTC, tokens.DAI),
      maxVolume: 5_000_000,
      addressA: tokens.WBTC.address,
      addressB: tokens.DAI.address,
      info: 'Uniswap V2 LP',
      compounds: ['v2-lp', 'leverage', 'staking']
    },
    {
      id: setup.sortedKey(tokens.USDC, tokens.DAI),
      maxVolume: 5_000_000,
      addressA: tokens.USDC.address,
      addressB: tokens.DAI.address,
      info: 'Uniswap V2 LP',
      compounds: ['v2-lp', 'leverage', 'staking']
    }
  )
  const filePath = path.resolve(__dirname, '../../data.json')
  fs.writeFileSync(filePath, JSON.stringify(GlobalData), {
    flag: 'w'
  })
  console.dir(GlobalData, { depth: null })
  console.log('Updated data.json:', filePath)

  const setupFile = path.resolve(__dirname, '../static/setup.json')
  fs.writeFileSync(setupFile, JSON.stringify({}), {
    flag: 'w'
  })

  const addrList = {
    deployer: deployer.address,
    user: user.address,
    v2factory: v2factory.address,
    v2router: v2router.address,
    strategy: strategy.address,
    pairVaults: {},
    leverage: leverage.address,
    staking: staking.address
  }

  for (const key in pairVaults) {
    const pair = pairVaults[key]
    const token0Addr = await pair.token0()
    const token1Addr = await pair.token1()
    const tokenQuoteAddr = await pair.tokenQuote()
    const token0 = Object.values(tokens).find((t) => t.address === token0Addr)
    const token1 = Object.values(tokens).find((t) => t.address === token1Addr)
    const [tokenA, tokenB] =
      token1Addr === tokenQuoteAddr ? [token0, token1] : [token1, token0]

    addrList.pairVaults[`${tokenA.address}/${tokenB.address}`] = pair.address
  }
  console.log({ addrList })
}
